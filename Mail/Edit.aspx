﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Mail_Edit, App_Web_mqdw5et2" %>

<%@ Register Src="Nav.ascx" TagName="Nav" TagPrefix="MojoCube" %>

<%@ Register Src="~/Controls/CKeditor.ascx" TagName="CKeditor" TagPrefix="MojoCube" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

    <script type="text/javascript">

        function ddlChange() {
            var ddl = document.getElementById("ctl00_cphMain_ddlAccount");
            if (ddl.value == "0") {
                $("#ctl00_cphMain_txtMailTo").attr("onfocus", "this.blur()");
                document.getElementById("ctl00_cphMain_txtMailTo").value = "";
                document.getElementById("ctl00_cphMain_txtMailToID").value = "";
            }
            else {
                $("#ctl00_cphMain_txtMailTo").removeAttr("onfocus");
                document.getElementById("ctl00_cphMain_txtMailTo").value = "";
                document.getElementById("ctl00_cphMain_txtMailToID").value = "";
            }
        }

        function showMore() {
            document.getElementById("MoreDiv").style.display = "";
        }

    </script>

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            邮件管理
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">邮件管理</li>
          </ol>
        </section>

        <section class="content">
            <div class="row">
        
                <MojoCube:Nav id="Nav" runat="server" />
    
                <div class="col-md-9">
                
                  <div id="AlertDiv" runat="server"></div>

                  <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">写信</h3>
                      <div class="pull-right">
                          <asp:HyperLink ID="hlMore" runat="server" NavigateUrl="javascript:showMore();"><span class="label label-back"><i class="fa fa-chevron-down"></i> 展开更多</span></asp:HyperLink>
                      </div>
                    </div>
                    <div class="box-body">
                      <div id="MoreDiv" style="display:none;">
                          <div class="form-group">
                            <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control select2"></asp:DropDownList>
                          </div>
                          <div class="form-group">
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control select2"></asp:DropDownList>
                          </div>
                          <div class="form-group">
                            <asp:DropDownList ID="ddlAccount" runat="server" CssClass="form-control select2" onchange="ddlChange();"></asp:DropDownList>
                          </div>
                      </div>
                      <div class="form-group" style="position:relative;">
                        <asp:TextBox ID="txtMailTo" runat="server" CssClass="form-control" placeholder="收件人：" onfocus="this.blur()"></asp:TextBox>
                        <asp:TextBox ID="txtMailToID" runat="server" style="display:none;"></asp:TextBox>
                        <div style="position:absolute; top:5px; right:5px;">
                            <asp:HyperLink ID="hlAddress" runat="server" CssClass="fancybox fancybox.iframe"><span class="label label-success"><i class="fa fa-plus"></i> 通讯录</span></asp:HyperLink>
                        </div>
                      </div>
                      <div class="form-group">
                        <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control" placeholder="主题："></asp:TextBox>
                      </div>
                      <div class="form-group">
                        <MojoCube:CKeditor id="txtContent" runat="server" Height="400" />
                      </div>
                      <div class="form-group">
                        <div class="btn btn-default btn-file">
                          <i class="fa fa-paperclip"></i> 增加附件
                          <asp:FileUpload ID="fuAttachment" runat="server" onchange="ChkUpload(this);"></asp:FileUpload>
                          <span id="filepath"></span>
                        </div>
                        <p class="help-block">5MB以内</p>
                      </div>
                    </div>
                    <div class="box-footer">
                      <div class="pull-right">
                        <asp:LinkButton ID="lnbDraft" runat="server" CssClass="btn btn-default" onclick="lnbDraft_Click"><i class="fa fa-pencil"></i> 存草稿</asp:LinkButton>
                        <asp:LinkButton ID="lnbSend" runat="server" CssClass="btn btn-primary" onclick="lnbSend_Click"><i class="fa fa-envelope-o"></i> 发送</asp:LinkButton>
                      </div>
                      <asp:LinkButton ID="lnbDiscard" runat="server" CssClass="btn btn-default" onclick="lnbDiscard_Click"><i class="fa fa-times"></i> 放弃</asp:LinkButton>
                    </div>
                  </div>
                </div>

            </div>
        </section>

      </div>

</asp:Content>


