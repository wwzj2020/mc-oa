USE [OA]
GO
/****** Object:  Table [dbo].[User_Position]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Position](
	[pk_Position] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NULL,
	[ParentID] [int] NULL,
	[LevelID] [int] NULL,
	[SortID] [int] NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_User_Position] PRIMARY KEY CLUSTERED 
(
	[pk_Position] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[User_Position] ON
INSERT [dbo].[User_Position] ([pk_Position], [Title], [ParentID], [LevelID], [SortID], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (1, N'CEO', 0, 1, 0, 0, 1, CAST(0x0000A51F017BE840 AS DateTime), 1, CAST(0x0000A51F017C28B4 AS DateTime))
INSERT [dbo].[User_Position] ([pk_Position], [Title], [ParentID], [LevelID], [SortID], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (2, N'总经理', 0, 2, 0, 0, 1, CAST(0x0000A51F017C3B74 AS DateTime), 0, CAST(0x0000A51F017C3B74 AS DateTime))
INSERT [dbo].[User_Position] ([pk_Position], [Title], [ParentID], [LevelID], [SortID], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (3, N'副总经理', 0, 3, 0, 0, 1, CAST(0x0000A51F017C4984 AS DateTime), 0, CAST(0x0000A51F017C4984 AS DateTime))
INSERT [dbo].[User_Position] ([pk_Position], [Title], [ParentID], [LevelID], [SortID], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (4, N'总监', 0, 4, 0, 0, 1, CAST(0x0000A51F017C5D70 AS DateTime), 0, CAST(0x0000A51F017C5D70 AS DateTime))
INSERT [dbo].[User_Position] ([pk_Position], [Title], [ParentID], [LevelID], [SortID], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (5, N'部门经理', 0, 5, 0, 0, 1, CAST(0x0000A51F017C81C4 AS DateTime), 0, CAST(0x0000A51F017C81C4 AS DateTime))
INSERT [dbo].[User_Position] ([pk_Position], [Title], [ParentID], [LevelID], [SortID], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (6, N'办公室主任', 0, 6, 0, 0, 1, CAST(0x0000A51F017C9CB8 AS DateTime), 0, CAST(0x0000A51F017C9CB8 AS DateTime))
INSERT [dbo].[User_Position] ([pk_Position], [Title], [ParentID], [LevelID], [SortID], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (7, N'助理', 0, 7, 0, 0, 1, CAST(0x0000A51F017D0AE0 AS DateTime), 1, CAST(0x0000A51F017D237C AS DateTime))
INSERT [dbo].[User_Position] ([pk_Position], [Title], [ParentID], [LevelID], [SortID], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (8, N'文员', 0, 8, 0, 0, 1, CAST(0x0000A51F017D1698 AS DateTime), 0, CAST(0x0000A51F017D1698 AS DateTime))
SET IDENTITY_INSERT [dbo].[User_Position] OFF
/****** Object:  Table [dbo].[User_Online]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Online](
	[pk_Online] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[SessionID] [nvarchar](50) NULL,
	[IPAddress] [nvarchar](50) NULL,
	[Browser] [nvarchar](50) NULL,
	[TypeID] [int] NULL,
	[LoginTime] [datetime] NULL,
	[fk_Company] [int] NULL,
 CONSTRAINT [PK_User_Online] PRIMARY KEY CLUSTERED 
(
	[pk_Online] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[User_Online] ON
INSERT [dbo].[User_Online] ([pk_Online], [fk_User], [SessionID], [IPAddress], [Browser], [TypeID], [LoginTime], [fk_Company]) VALUES (33, 1, N'zia5jdd3sqnlo1x4245rzwh4', N'::1', N'Chrome 72.0', 0, CAST(0x0000AA0B00A13D8F AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[User_Online] OFF
/****** Object:  Table [dbo].[User_Log]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Log](
	[pk_Log] [int] IDENTITY(1,1) NOT NULL,
	[IPAddress] [nvarchar](50) NULL,
	[Url] [nvarchar](200) NULL,
	[fk_User] [int] NULL,
	[Browser] [nvarchar](50) NULL,
	[TypeID] [int] NULL,
	[LogTime] [datetime] NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL,
	[SessionID] [nvarchar](50) NULL,
	[fk_Company] [int] NULL,
 CONSTRAINT [PK_User_Log] PRIMARY KEY CLUSTERED 
(
	[pk_Log] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User_List](
	[pk_User] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](100) NULL,
	[Password] [nvarchar](100) NULL,
	[TypeID] [int] NULL,
	[fk_Department] [int] NULL,
	[RoleValue] [int] NULL,
	[RoleList] [nvarchar](200) NULL,
	[Position] [int] NULL,
	[Number] [nvarchar](50) NULL,
	[Skin] [nvarchar](50) NULL,
	[Language] [varchar](10) NULL,
	[IsLock] [bit] NULL,
	[LastLoginIP] [nvarchar](50) NULL,
	[LastLoginTime] [datetime] NULL,
	[NickName] [nvarchar](50) NULL,
	[FullName] [nvarchar](50) NULL,
	[FirstName] [nvarchar](50) NULL,
	[MiddleName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Phone1] [nvarchar](50) NULL,
	[Phone2] [nvarchar](50) NULL,
	[Email1] [nvarchar](100) NULL,
	[Email2] [nvarchar](100) NULL,
	[Fax] [nvarchar](50) NULL,
	[Line] [nvarchar](50) NULL,
	[Wechat] [nvarchar](50) NULL,
	[QQ] [nvarchar](50) NULL,
	[Facebook] [nvarchar](50) NULL,
	[Twitter] [nvarchar](50) NULL,
	[Linkedin] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](50) NULL,
	[Place] [nvarchar](50) NULL,
	[Address1] [nvarchar](500) NULL,
	[Address2] [nvarchar](500) NULL,
	[Province] [int] NULL,
	[City] [int] NULL,
	[County] [int] NULL,
	[Zone] [int] NULL,
	[Sex] [int] NULL,
	[Height] [int] NULL,
	[Weight] [int] NULL,
	[Birthday] [datetime] NULL,
	[Education] [nvarchar](50) NULL,
	[School] [nvarchar](50) NULL,
	[ImagePath1] [nvarchar](200) NULL,
	[ImagePath2] [nvarchar](200) NULL,
	[IDCardPath] [nvarchar](200) NULL,
	[ResumePath] [nvarchar](200) NULL,
	[Wages] [decimal](18, 3) NULL,
	[BankAccount] [nvarchar](100) NULL,
	[IDNumber] [nvarchar](50) NULL,
	[Source] [nvarchar](100) NULL,
	[Note] [nvarchar](500) NULL,
	[Remark] [nvarchar](500) NULL,
	[EntryTime] [datetime] NULL,
	[IsQuit] [bit] NULL,
	[QuitTime] [datetime] NULL,
	[ShowHistory] [int] NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_User_List] PRIMARY KEY CLUSTERED 
(
	[pk_User] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[User_List] ON
INSERT [dbo].[User_List] ([pk_User], [UserName], [Password], [TypeID], [fk_Department], [RoleValue], [RoleList], [Position], [Number], [Skin], [Language], [IsLock], [LastLoginIP], [LastLoginTime], [NickName], [FullName], [FirstName], [MiddleName], [LastName], [Phone1], [Phone2], [Email1], [Email2], [Fax], [Line], [Wechat], [QQ], [Facebook], [Twitter], [Linkedin], [ZipCode], [Place], [Address1], [Address2], [Province], [City], [County], [Zone], [Sex], [Height], [Weight], [Birthday], [Education], [School], [ImagePath1], [ImagePath2], [IDCardPath], [ResumePath], [Wages], [BankAccount], [IDNumber], [Source], [Note], [Remark], [EntryTime], [IsQuit], [QuitTime], [ShowHistory], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (1, N'admin', N'21232f297a57a5a743894a0e4a801fc3', 0, 1, 1, N'1', 1, N'', N'blue', N'CHS', 0, N'::1', CAST(0x0000AA0B00A13D8E AS DateTime), N'Administrator', N'盖茨', N'茨', N'', N'盖', N'13800138000', N'18600186000', N'gates@mojocube.com', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'广东省广州市天河区', N'', 0, 0, 0, 0, 0, 0, 0, CAST(0x0000770C00000000 AS DateTime), N'本科', N'哈佛大学', N'User/1/2015102321215926568945.jpg', N'', N'', N'', CAST(3000.000 AS Decimal(18, 3)), N'（工行）88088800008020', N'441891198801082910', N'', N'天生我材必有用', N'<i class="fa fa-circle text-success"></i> 在线', CAST(0x0000A4B400000000 AS DateTime), 0, CAST(0x0000A4B40160B174 AS DateTime), 10, 0, 1, CAST(0x0000A4B40160B174 AS DateTime), 1, CAST(0x0000AA0B00A13D8F AS DateTime))
INSERT [dbo].[User_List] ([pk_User], [UserName], [Password], [TypeID], [fk_Department], [RoleValue], [RoleList], [Position], [Number], [Skin], [Language], [IsLock], [LastLoginIP], [LastLoginTime], [NickName], [FullName], [FirstName], [MiddleName], [LastName], [Phone1], [Phone2], [Email1], [Email2], [Fax], [Line], [Wechat], [QQ], [Facebook], [Twitter], [Linkedin], [ZipCode], [Place], [Address1], [Address2], [Province], [City], [County], [Zone], [Sex], [Height], [Weight], [Birthday], [Education], [School], [ImagePath1], [ImagePath2], [IDCardPath], [ResumePath], [Wages], [BankAccount], [IDNumber], [Source], [Note], [Remark], [EntryTime], [IsQuit], [QuitTime], [ShowHistory], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (2, N'romeo', N'e10adc3949ba59abbe56e057f20f883e', 0, 2, 2, N'2', 2, N'', N'green', N'CHS', 0, N'::1', CAST(0x0000A8CA00F364AB AS DateTime), N'Romeo', N'罗密欧', N'密欧', N'', N'罗', N'18600186000', N'', N'romeo@mojocube.com', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'广东省广州市天河区', N'', 0, 0, 0, 0, 0, 0, 0, CAST(0x0000818F00000000 AS DateTime), N'本科', N'北京大学', N'User/2/2015102322505278126894.jpg', N'', N'', N'', CAST(8000.000 AS Decimal(18, 3)), N'（工行）', N'441891198801082911', N'', N'', N'<i class="fa fa-circle text-success"></i> 在线', CAST(0x0000A53A00000000 AS DateTime), 0, CAST(0x0000A53A016F3ED8 AS DateTime), 10, 0, 1, CAST(0x0000A53A016F3ED8 AS DateTime), 1, CAST(0x0000A8CA00F364AD AS DateTime))
INSERT [dbo].[User_List] ([pk_User], [UserName], [Password], [TypeID], [fk_Department], [RoleValue], [RoleList], [Position], [Number], [Skin], [Language], [IsLock], [LastLoginIP], [LastLoginTime], [NickName], [FullName], [FirstName], [MiddleName], [LastName], [Phone1], [Phone2], [Email1], [Email2], [Fax], [Line], [Wechat], [QQ], [Facebook], [Twitter], [Linkedin], [ZipCode], [Place], [Address1], [Address2], [Province], [City], [County], [Zone], [Sex], [Height], [Weight], [Birthday], [Education], [School], [ImagePath1], [ImagePath2], [IDCardPath], [ResumePath], [Wages], [BankAccount], [IDNumber], [Source], [Note], [Remark], [EntryTime], [IsQuit], [QuitTime], [ShowHistory], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (3, N'juliet', N'e10adc3949ba59abbe56e057f20f883e', 0, 3, 4, N'4', 6, N'', N'purple', N'CHS', 0, N'127.0.0.1', CAST(0x0000A55B017E8D98 AS DateTime), N'Juliet', N'朱丽叶', N'丽叶', N'', N'朱', N'15868886888', N'', N'Juliet@mojocube.com', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'广东省广州市天河区', N'', 0, 0, 0, 0, 1, 0, 0, CAST(0x0000841700000000 AS DateTime), N'本科', N'中央财经大学', N'User/3/2015102322511228129536.jpg', N'', N'', N'', CAST(6000.000 AS Decimal(18, 3)), N'（工行）', N'441891198801082912', N'', N'', N'', CAST(0x0000A53A00000000 AS DateTime), 0, CAST(0x0000A53A017085CC AS DateTime), 10, 0, 1, CAST(0x0000A53A017085CC AS DateTime), 1, CAST(0x0000A587016A682C AS DateTime))
SET IDENTITY_INSERT [dbo].[User_List] OFF
/****** Object:  Table [dbo].[User_Department]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Department](
	[pk_Department] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentName] [nvarchar](50) NULL,
	[Phone1] [nvarchar](50) NULL,
	[Phone2] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[Email] [nvarchar](100) NULL,
	[Address] [nvarchar](500) NULL,
	[ParentID] [int] NULL,
	[LevelID] [int] NULL,
	[SortID] [int] NULL,
	[TypeID] [int] NULL,
	[Province] [int] NULL,
	[City] [int] NULL,
	[County] [int] NULL,
	[Zone] [int] NULL,
	[Manager] [int] NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[Monday] [nvarchar](50) NULL,
	[Tuesday] [nvarchar](50) NULL,
	[Wednesday] [nvarchar](50) NULL,
	[Thursday] [nvarchar](50) NULL,
	[Friday] [nvarchar](50) NULL,
	[Saturday] [nvarchar](50) NULL,
	[Sunday] [nvarchar](50) NULL,
 CONSTRAINT [PK_User_Branch] PRIMARY KEY CLUSTERED 
(
	[pk_Department] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[User_Department] ON
INSERT [dbo].[User_Department] ([pk_Department], [DepartmentName], [Phone1], [Phone2], [Fax], [Email], [Address], [ParentID], [LevelID], [SortID], [TypeID], [Province], [City], [County], [Zone], [Manager], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate], [Monday], [Tuesday], [Wednesday], [Thursday], [Friday], [Saturday], [Sunday]) VALUES (1, N'总公司', N'88888888', N'', N'88888888', N'hq@mojocube.com', N'中国广东', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, CAST(0x0000A51F00B9E254 AS DateTime), 1, CAST(0x0000A5270186387C AS DateTime), N'08:00-12:00|14:00-18:00', N'08:00-12:00|14:00-18:00', N'08:00-12:00|14:00-18:00', N'08:00-12:00|14:00-18:00', N'08:00-12:00|14:00-18:00', N'', N'')
INSERT [dbo].[User_Department] ([pk_Department], [DepartmentName], [Phone1], [Phone2], [Fax], [Email], [Address], [ParentID], [LevelID], [SortID], [TypeID], [Province], [City], [County], [Zone], [Manager], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate], [Monday], [Tuesday], [Wednesday], [Thursday], [Friday], [Saturday], [Sunday]) VALUES (2, N'总经办', N'88888881', N'', N'88888881', N'office@mojocube.com', N'中国广东', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, CAST(0x0000A51F00BA8538 AS DateTime), 1, CAST(0x0000A52701864560 AS DateTime), N'08:00-12:00|14:00-18:00', N'08:00-12:00|14:00-18:00', N'08:00-12:00|14:00-18:00', N'08:00-12:00|14:00-18:00', N'08:00-12:00|14:00-18:00', N'', N'')
INSERT [dbo].[User_Department] ([pk_Department], [DepartmentName], [Phone1], [Phone2], [Fax], [Email], [Address], [ParentID], [LevelID], [SortID], [TypeID], [Province], [City], [County], [Zone], [Manager], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate], [Monday], [Tuesday], [Wednesday], [Thursday], [Friday], [Saturday], [Sunday]) VALUES (3, N'财务科', N'88888882', N'', N'88888882', N'finance@mojocube.com', N'中国广东', 1, 1, 2, 0, 0, 0, 0, 0, 0, 0, 1, CAST(0x0000A51F00BAF5B8 AS DateTime), 1, CAST(0x0000A52701864A10 AS DateTime), N'08:00-12:00|14:00-18:00', N'08:00-12:00|14:00-18:00', N'08:00-12:00|14:00-18:00', N'08:00-12:00|14:00-18:00', N'08:00-12:00|14:00-18:00', N'', N'')
SET IDENTITY_INSERT [dbo].[User_Department] OFF
/****** Object:  Table [dbo].[Task_Receiver]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Task_Receiver](
	[pk_Receiver] [int] IDENTITY(1,1) NOT NULL,
	[fk_Task] [int] NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[IsRead] [bit] NULL,
	[ReadDate] [datetime] NULL,
	[IsReceive] [bit] NULL,
	[ReceiveDate] [datetime] NULL,
	[StatusID] [int] NULL,
 CONSTRAINT [PK_Task_Receiver] PRIMARY KEY CLUSTERED 
(
	[pk_Receiver] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Task_Receiver] ON
INSERT [dbo].[Task_Receiver] ([pk_Receiver], [fk_Task], [fk_User], [fk_Department], [IsRead], [ReadDate], [IsReceive], [ReceiveDate], [StatusID]) VALUES (1, 1, 3, 3, 1, CAST(0x0000A55A017FCEB0 AS DateTime), 1, CAST(0x0000A55A01812E40 AS DateTime), 1)
INSERT [dbo].[Task_Receiver] ([pk_Receiver], [fk_Task], [fk_User], [fk_Department], [IsRead], [ReadDate], [IsReceive], [ReceiveDate], [StatusID]) VALUES (2, 1, 2, 2, 0, CAST(0x0000A7CA00E023B0 AS DateTime), 0, CAST(0x0000A7CA00E023B0 AS DateTime), 0)
INSERT [dbo].[Task_Receiver] ([pk_Receiver], [fk_Task], [fk_User], [fk_Department], [IsRead], [ReadDate], [IsReceive], [ReceiveDate], [StatusID]) VALUES (3, 1, 1, 1, 0, CAST(0x0000A7CA00E023B0 AS DateTime), 0, CAST(0x0000A7CA00E023B0 AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[Task_Receiver] OFF
/****** Object:  Table [dbo].[Task_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Task_List](
	[pk_Task] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL,
	[Note] [nvarchar](1000) NULL,
	[Feedback] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](200) NULL,
	[FilePath] [nvarchar](200) NULL,
	[UserList] [nvarchar](1000) NULL,
	[DepartmentList] [nvarchar](1000) NULL,
	[RoleList] [nvarchar](1000) NULL,
	[Url] [nvarchar](200) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[IsComplete] [bit] NULL,
	[CompleteDate] [datetime] NULL,
	[IsCancel] [bit] NULL,
	[CancelDate] [datetime] NULL,
	[IsTop] [bit] NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Task_List] PRIMARY KEY CLUSTERED 
(
	[pk_Task] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Task_List] ON
INSERT [dbo].[Task_List] ([pk_Task], [fk_User], [fk_Department], [TypeID], [StatusID], [Title], [Description], [Note], [Feedback], [ImagePath], [FilePath], [UserList], [DepartmentList], [RoleList], [Url], [StartDate], [EndDate], [IsComplete], [CompleteDate], [IsCancel], [CancelDate], [IsTop], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (1, 1, 1, 0, 3, N'财务科工资报表制作', N'1. 列出更部门员工具体工资
2. 打印员工具体考勤情况
3. 打印银行流水账，盖上公章', N'【盖茨】【盖茨】', N'朱丽叶<span style="font-size:8pt; color:#999; margin-left:3px;">[2015-11-24 23:22]</span>：将状态改为【已接收】
朱丽叶<span style="font-size:8pt; color:#999; margin-left:3px;">[2015-11-25 23:13]</span>：请问离职的员工也需要做进报表吗？
盖茨<span style="font-size:8pt; color:#999; margin-left:3px;">[2015-11-25 23:14]</span>：是的，离职员工也做进报表，谢谢！
', N'', N'', N'3|2|1', N'', N'', N'', CAST(0x0000A55A00000000 AS DateTime), CAST(0x0000A57800000000 AS DateTime), 0, CAST(0x0000A55A017FBE48 AS DateTime), 0, CAST(0x0000A55A017FBE48 AS DateTime), 0, 0, 1, CAST(0x0000A55A017FBE48 AS DateTime), 1, CAST(0x0000A7CA00E039F4 AS DateTime))
SET IDENTITY_INSERT [dbo].[Task_List] OFF
/****** Object:  Table [dbo].[Sys_TypeID]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_TypeID](
	[pk_TypeID] [int] IDENTITY(1,1) NOT NULL,
	[TypeName_CHS] [nvarchar](20) NULL,
	[TypeName_CHT] [nvarchar](20) NULL,
	[TypeName_EN] [nvarchar](20) NULL,
	[ID] [int] NULL,
	[Visual] [varchar](10) NULL,
	[TableName] [varchar](20) NULL,
	[Description_CHS] [nvarchar](1000) NULL,
	[Description_CHT] [nvarchar](1000) NULL,
	[Description_EN] [nvarchar](1000) NULL,
 CONSTRAINT [PK_Sys_TypeID] PRIMARY KEY CLUSTERED 
(
	[pk_TypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_TypeID] ON
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (1, N'菜单', N'', N'', 0, N'#008000', N'Sys_Menu', NULL, NULL, NULL)
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (2, N'页面', NULL, NULL, 1, N'#999999', N'Sys_Menu', NULL, NULL, NULL)
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (3, N'上班', N'', N'', 0, N'', N'Attendance_List', NULL, NULL, NULL)
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (4, N'下班', N'', N'', 1, N'', N'Attendance_List', NULL, NULL, NULL)
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (5, N'外出', N'', N'', 2, N'', N'Attendance_List', NULL, NULL, NULL)
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (6, N'请假', N'', N'', 3, N'', N'Attendance_List', NULL, NULL, NULL)
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (7, N'加班', N'', N'', 4, N'', N'Attendance_List', NULL, NULL, NULL)
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (8, N'请假', N'', N'', 0, N'', N'Process_List', N'请假时间：
请假原因：', N'2|3', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (9, N'加班', N'', N'', 1, N'', N'Process_List', N'加班时间：
加班原因：', N'2|3', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (10, N'报销', N'', N'', 2, N'', N'Process_List', N'商品描述：
商品价格：
商品数量：
购买原因：', N'2|3', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (11, N'出差', N'', N'', 3, N'', N'Process_List', N'出差时间：
出差原因：', N'2|3', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (12, N'公告', N'', N'', 0, N'', N'Notice_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (13, N'通知', N'', N'', 1, N'', N'Notice_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (14, N'投票', N'', N'', 2, N'', N'Notice_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (15, N'系统邮件', N'', N'', 0, N'', N'Mail_Account', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (16, N'公司邮件', N'', N'', 1, N'', N'Mail_Account', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (17, N'邮件', N'', N'', 0, N'', N'Mail_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (18, N'通知', N'', N'', 1, N'', N'Mail_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (19, N'公告', N'', N'', 2, N'red', N'Mail_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (20, N'公事', N'', N'', 0, N'', N'Task_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (21, N'私事', N'', N'', 1, N'', N'Task_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (22, N'日程提醒', N'', N'', 0, N'', N'Calendar_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (23, N'假日安排', N'', N'', 1, N'', N'Calendar_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (24, N'日计划', N'', N'', 0, N'', N'Plan_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (25, N'周计划', N'', N'', 1, N'', N'Plan_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (26, N'月计划', N'', N'', 2, N'', N'Plan_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (27, N'我的文件', N'', N'', 0, N'', N'Document_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (28, N'公司文件', N'', N'', 1, N'', N'Document_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (29, N'共享文件', N'', N'', 2, N'', N'Document_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (30, N'我的笔记', N'', N'', 0, N'', N'Note_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (31, N'公司笔记', N'', N'', 1, N'', N'Note_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (32, N'共享笔记', N'', N'', 2, N'', N'Note_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (33, N'我的通讯录', N'', N'', 0, N'', N'Address_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (34, N'公司通讯录', N'', N'', 1, N'', N'Address_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (35, N'共享通讯录', N'', N'', 2, N'', N'Address_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (36, N'讨论', N'', N'', 0, N'', N'Forum_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (37, N'公告', N'', N'', 1, N'', N'Forum_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (38, N'投票', N'', N'', 2, N'', N'Forum_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (39, N'活动', N'', N'', 3, N'', N'Forum_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (40, N'业务', N'', N'', 4, N'', N'Forum_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (41, N'下载', N'', N'', 5, N'', N'Forum_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (42, N'手动', N'', N'', 0, N'', N'User_Online', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (43, N'自动', N'', N'', 1, N'', N'User_Online', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (44, N'办事', N'', N'', 0, N'', N'Car_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (45, N'出差', N'', N'', 1, N'', N'Car_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (46, N'接待', N'', N'', 2, N'', N'Car_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (47, N'小车', N'', N'', 0, N'', N'Car_Set', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (48, N'商务车', N'', N'', 1, N'', N'Car_Set', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (49, N'银联', N'', N'', 0, N'', N'Finance_Account', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (50, N'微信', N'', N'', 1, N'', N'Finance_Account', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (51, N'支付宝', N'', N'', 2, N'', N'Finance_Account', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (52, N'收入', N'', N'', 0, N'', N'Finance_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (53, N'支出', N'', N'', 1, N'', N'Finance_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (54, N'一般', N'', N'', 0, N'#00A65A', N'Workflow_Receiver', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (55, N'紧急', N'', N'', 1, N'#F39C12', N'Workflow_Receiver', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (56, N'严重', N'', N'', 2, N'#DD4B39', N'Workflow_Receiver', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (57, N'公章', N'', N'', 0, N'', N'Workflow_Signature', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (58, N'私章', N'', N'', 1, N'', N'Workflow_Signature', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (59, N'私有', N'', N'', 0, N'', N'Customer_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (60, N'公开', N'', N'', 1, N'', N'Customer_List', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (61, N'电话', N'', N'', 0, N'', N'Customer_Follow', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (62, N'邮件', N'', N'', 1, N'', N'Customer_Follow', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (63, N'信息', N'', N'', 2, N'', N'Customer_Follow', N'', N'', N'')
INSERT [dbo].[Sys_TypeID] ([pk_TypeID], [TypeName_CHS], [TypeName_CHT], [TypeName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (64, N'拜访', N'', N'', 3, N'', N'Customer_Follow', N'', N'', N'')
SET IDENTITY_INSERT [dbo].[Sys_TypeID] OFF
/****** Object:  Table [dbo].[Sys_StatusID]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_StatusID](
	[pk_StatusID] [int] IDENTITY(1,1) NOT NULL,
	[StatusName_CHS] [nvarchar](20) NULL,
	[StatusName_CHT] [nvarchar](20) NULL,
	[StatusName_EN] [nvarchar](20) NULL,
	[ID] [int] NULL,
	[Visual] [varchar](10) NULL,
	[TableName] [varchar](20) NULL,
	[Description_CHS] [nvarchar](1000) NULL,
	[Description_CHT] [nvarchar](1000) NULL,
	[Description_EN] [nvarchar](1000) NULL,
 CONSTRAINT [PK_Sys_StatusID] PRIMARY KEY CLUSTERED 
(
	[pk_StatusID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_StatusID] ON
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (1, N'正常', N'', N'', 0, N'#00C0EF', N'Attendance_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (2, N'迟到', N'', N'', 1, N'#DD4B39', N'Attendance_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (3, N'早退', N'', N'', 2, N'#DD4B39', N'Attendance_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (4, N'旷工', N'', N'', 3, N'#DD4B39', N'Attendance_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (5, N'处理中', N'', N'', 0, N'#F39C12', N'Process_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (6, N'已批准', N'', N'', 1, N'#00A65A', N'Process_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (7, N'一般', N'', N'', 0, N'#00C0EF', N'Notice_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (8, N'重要', N'', N'', 1, N'#F39C12', N'Notice_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (9, N'紧急', N'', N'', 2, N'#DD4B39', N'Notice_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (10, N'有效', N'', N'', 0, N'#00A65A', N'Mail_Account', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (11, N'无效', N'', N'', 1, N'#D2D6DE', N'Mail_Account', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (12, N'一般', N'', N'', 0, N'#00C0EF', N'Mail_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (13, N'重要', N'', N'', 1, N'#F39C12', N'Mail_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (14, N'紧急', N'', N'', 2, N'#DD4B39', N'Mail_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (15, N'新任务', N'', N'', 0, N'#F39C12', N'Task_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (16, N'已接收', N'', N'', 1, N'#00C0EF', N'Task_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (17, N'进行中', N'', N'', 2, N'#3C8DBC', N'Task_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (18, N'已完成', N'', N'', 3, N'#00A65A', N'Task_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (19, N'一般', N'', N'', 0, N'#00C0EF', N'Calendar_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (20, N'重要', N'', N'', 1, N'#F39C12', N'Calendar_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (21, N'紧急', N'', N'', 2, N'#DD4B39', N'Calendar_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (22, N'未完成', N'', N'', 0, N'#00C0EF', N'Plan_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (23, N'已完成', N'', N'', 1, N'#00A65A', N'Plan_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (24, N'已取消', N'', N'', 2, N'#999999', N'Plan_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (25, N'一般', N'', N'', 0, N'#00C0EF', N'Document_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (26, N'重要', N'', N'', 1, N'#F39C12', N'Document_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (27, N'一般', N'', N'', 0, N'#00C0EF', N'Note_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (28, N'重要', N'', N'', 1, N'#F39C12', N'Note_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (29, N'一般', N'', N'', 0, N'#00C0EF', N'Address_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (30, N'重要', N'', N'', 1, N'#F39C12', N'Address_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (31, N'一般', N'', N'', 0, N'#00C0EF', N'Forum_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (32, N'重要', N'', N'', 1, N'#F39C12', N'Forum_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (33, N'紧急', N'', N'', 2, N'#DD4B39', N'Forum_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (34, N'未批准', N'', N'', 2, N'#999999', N'Process_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (35, N'申请中', N'', N'', 0, N'#F39C12', N'Car_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (36, N'已批准', N'', N'', 1, N'#00A65A', N'Car_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (37, N'未批准', N'', N'', 2, N'#999999', N'Car_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (38, N'可使用', N'', N'', 0, N'#00A65A', N'Car_Set', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (39, N'使用中', N'', N'', 1, N'#F39C12', N'Car_Set', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (40, N'保养中', N'', N'', 2, N'#999999', N'Car_Set', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (41, N'已报废', N'', N'', 3, N'#DD4B39', N'Car_Set', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (42, N'有效', N'', N'', 0, N'#00A65A', N'Finance_Account', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (43, N'无效', N'', N'', 1, N'#D2D6DE', N'Finance_Account', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (44, N'待定', N'', N'', 0, N'#999999', N'Finance_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (45, N'有效', N'', N'', 1, N'#00A65A', N'Finance_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (46, N'无效', N'', N'', 2, N'#D2D6DE', N'Finance_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (47, N'新工作', N'', N'', 0, N'#F39C12', N'Workflow_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (48, N'进行中', N'', N'', 1, N'#00C0EF', N'Workflow_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (49, N'已通过', N'', N'', 2, N'#00A65A', N'Workflow_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (50, N'未通过', N'', N'', 3, N'#DD4B39', N'Workflow_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (51, N'已取消', N'', N'', 4, N'#999999', N'Workflow_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (52, N'待审批', N'', N'', 0, N'#F39C12', N'Workflow_Receiver', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (53, N'已同意', N'', N'', 1, N'#00A65A', N'Workflow_Receiver', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (54, N'不同意', N'', N'', 2, N'#DD4B39', N'Workflow_Receiver', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (55, N'已撤回', N'', N'', 3, N'#999999', N'Workflow_Receiver', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (56, N'已退回', N'', N'', 4, N'#999999', N'Workflow_Receiver', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (57, N'有效', N'', N'', 0, N'#00A65A', N'Customer_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (58, N'无效', N'', N'', 1, N'#D2D6DE', N'Customer_List', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (59, N'新跟进', N'', N'', 0, N'#F39C12', N'Customer_Follow', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (60, N'进行中', N'', N'', 1, N'#00C0EF', N'Customer_Follow', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (61, N'已完成', N'', N'', 2, N'#00A65A', N'Customer_Follow', N'', N'', N'')
INSERT [dbo].[Sys_StatusID] ([pk_StatusID], [StatusName_CHS], [StatusName_CHT], [StatusName_EN], [ID], [Visual], [TableName], [Description_CHS], [Description_CHT], [Description_EN]) VALUES (62, N'已取消', N'', N'', 3, N'#999999', N'Customer_Follow', N'', N'', N'')
SET IDENTITY_INSERT [dbo].[Sys_StatusID] OFF
/****** Object:  Table [dbo].[Sys_Province]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_Province](
	[pk_Province] [int] IDENTITY(1,1) NOT NULL,
	[fk_Country] [int] NULL,
	[FullName] [nvarchar](100) NULL,
	[ShortName] [nvarchar](10) NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_Sys_Province] PRIMARY KEY CLUSTERED 
(
	[pk_Province] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Sys_Province] ON
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (1, 16, N'北京市', N'北京', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (2, 16, N'天津市', N'天津', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (3, 16, N'上海市', N'上海', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (4, 16, N'重庆市', N'重庆', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (5, 16, N'广东省', N'广东', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (6, 16, N'湖南省', N'湖南', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (7, 16, N'湖北省', N'湖北', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (8, 16, N'浙江省', N'浙江', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (9, 16, N'江西省', N'江西', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (10, 16, N'江苏省', N'江苏', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (11, 16, N'福建省', N'福建', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (12, 16, N'甘肃省', N'甘肃', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (13, 16, N'贵州省', N'贵州', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (14, 16, N'云南省', N'云南', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (15, 16, N'海南省', N'海南', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (16, 16, N'四川省', N'四川', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (17, 16, N'河北省', N'河北', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (18, 16, N'河南省', N'河南', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (19, 16, N'山东省', N'山东', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (20, 16, N'山西省', N'山西', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (21, 16, N'陕西省', N'陕西', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (22, 16, N'青海省', N'青海', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (23, 16, N'辽宁省', N'辽宁', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (24, 16, N'吉林省', N'吉林', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (25, 16, N'黑龙江省', N'黑龙江', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (26, 16, N'广西壮族自治区', N'广西', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (27, 16, N'宁夏回族自治区', N'宁夏', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (28, 16, N'内蒙古自治区', N'内蒙古', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (29, 16, N'西藏自治区', N'西藏', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (30, 16, N'新疆维吾尔自治区', N'新疆', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (31, 16, N'香港特别行政区', N'香港', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (32, 16, N'澳门特别行政区', N'澳门', 1)
INSERT [dbo].[Sys_Province] ([pk_Province], [fk_Country], [FullName], [ShortName], [Visible]) VALUES (33, 16, N'台湾省', N'台湾', 1)
SET IDENTITY_INSERT [dbo].[Sys_Province] OFF
/****** Object:  Table [dbo].[Sys_Menu]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_Menu](
	[pk_Menu] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[Name_CHS] [nvarchar](50) NULL,
	[Name_CHT] [nvarchar](50) NULL,
	[Name_EN] [nvarchar](50) NULL,
	[Url] [nvarchar](200) NULL,
	[Icon] [nvarchar](50) NULL,
	[SortID] [int] NULL,
	[LevelID] [int] NULL,
	[TypeID] [int] NULL,
	[Visible] [bit] NULL,
	[Tag_CHS] [nvarchar](200) NULL,
	[Tag_CHT] [nvarchar](200) NULL,
	[Tag_EN] [nvarchar](200) NULL,
 CONSTRAINT [PK_Sys_Menu] PRIMARY KEY CLUSTERED 
(
	[pk_Menu] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Sys_Menu] ON
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (1, 0, N'系统管理', N'', N'', N'#', N'fa-desktop', 1, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (2, 1, N'菜单管理', N'', N'', N'System/Menu.aspx', N'fa-circle-o', 1, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (3, 0, N'角色管理', N'', N'', N'#', N'fa-mortar-board', 2, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (4, 1, N'类型管理', N'', N'', N'System/Type.aspx', N'fa-circle-o', 4, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (5, 1, N'菜单编辑', N'', N'', N'System/MenuEdit.aspx', N'fa-circle-o', 2, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (6, 1, N'类型编辑', N'', N'', N'System/TypeEdit.aspx', N'fa-circle-o', 5, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (7, 3, N'角色列表', N'', N'', N'Role/Name.aspx', N'fa-circle-o', 1, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (8, 3, N'角色编辑', N'', N'', N'Role/NameEdit.aspx', N'fa-circle-o', 2, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (9, 3, N'角色设定', N'', N'', N'Role/List.aspx', N'fa-circle-o', 3, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (10, 0, N'系统权限', N'', N'', N'#', N'fa-circle-o', 0, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (11, 10, N'控制面板', N'', N'', N'Dashboard/Default.aspx', N'fa-circle-o', 1, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (12, 0, N'用户管理', N'', N'', N'#', N'fa-users', 3, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (13, 12, N'部门管理', N'', N'', N'User/Department.aspx', N'fa-circle-o', 1, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (14, 12, N'职位管理', N'', N'', N'User/Position.aspx', N'fa-circle-o', 3, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (15, 12, N'用户管理', N'', N'', N'User/List.aspx', N'fa-circle-o', 5, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (16, 1, N'菜单图标', N'', N'', N'Skins/Icons.aspx', N'fa-circle-o', 3, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (17, 12, N'部门编辑', N'', N'', N'User/DepartmentEdit.aspx', N'fa-circle-o', 2, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (18, 12, N'职位编辑', N'', N'', N'User/PositionEdit.aspx', N'fa-circle-o', 4, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (19, 12, N'用户编辑', N'', N'', N'User/Edit.aspx', N'fa-circle-o', 6, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (20, 12, N'用户面板', N'', N'', N'User/Profile.aspx', N'fa-circle-o', 7, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (21, 1, N'状态管理', N'', N'', N'System/Status.aspx', N'fa-circle-o', 6, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (22, 1, N'状态编辑', N'', N'', N'System/StatusEdit.aspx', N'fa-circle-o', 7, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (23, 0, N'考勤管理', N'', N'', N'#', N'fa-clock-o', 4, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (24, 23, N'考勤管理', N'', N'', N'Attendance/List.aspx', N'fa-circle-o', 1, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (25, 23, N'考勤编辑', N'', N'', N'Attendance/Edit.aspx', N'fa-circle-o', 2, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (26, 23, N'考勤周报表', N'', N'', N'Attendance/Report_W.aspx', N'fa-circle-o', 3, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (27, 23, N'考勤月报表', N'', N'', N'Attendance/Report_M.aspx', N'fa-circle-o', 4, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (28, 23, N'考勤审核', N'', N'', N'Attendance/Check.aspx', N'fa-circle-o', 5, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (29, 0, N'流程管理', N'', N'', N'#', N'fa-hourglass-half', 5, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (30, 29, N'流程管理', N'', N'', N'Process/List.aspx', N'fa-circle-o', 1, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (31, 29, N'流程编辑', N'', N'', N'Process/Edit.aspx', N'fa-circle-o', 2, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (32, 29, N'流程审核', N'', N'', N'Process/CheckList.aspx', N'fa-circle-o', 3, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (33, 29, N'流程审核', N'', N'', N'Process/Check.aspx', N'fa-circle-o', 4, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (34, 0, N'公告通知', N'', N'', N'#', N'fa-bell-o', 6, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (35, 34, N'通知管理', N'', N'', N'Notice/List.aspx', N'fa-circle-o', 1, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (36, 34, N'通知编辑', N'', N'', N'Notice/Edit.aspx', N'fa-circle-o', 2, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (37, 34, N'通知列表', N'', N'', N'Notice/ViewList.aspx', N'fa-circle-o', 3, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (38, 34, N'通知查看', N'', N'', N'Notice/View.aspx', N'fa-circle-o', 4, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (39, 0, N'邮件管理', N'', N'', N'#', N'fa-envelope-o', 7, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (40, 39, N'账号管理', N'', N'', N'Mail/Account.aspx', N'fa-circle-o', 1, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (41, 39, N'账号编辑', N'', N'', N'Mail/AccountEdit.aspx', N'fa-circle-o', 2, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (42, 39, N'邮件管理', N'', N'', N'Mail/List.aspx', N'fa-circle-o', 3, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (43, 39, N'邮件编辑', N'', N'', N'Mail/Edit.aspx', N'fa-circle-o', 4, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (44, 39, N'邮件查看', N'', N'', N'Mail/View.aspx', N'fa-circle-o', 5, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (45, 39, N'通讯录', N'', N'', N'Mail/Address.aspx', N'fa-circle-o', 6, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (46, 0, N'任务管理', N'', N'', N'#', N'fa-flag-o', 8, 0, 0, 1, N'我的任务', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (47, 46, N'任务管理', N'', N'', N'Task/List.aspx', N'fa-circle-o', 1, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (48, 46, N'任务编辑', N'', N'', N'Task/Edit.aspx', N'fa-circle-o', 2, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (49, 46, N'任务接收', N'', N'', N'Task/Receiver.aspx', N'fa-circle-o', 3, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (50, 46, N'我的任务', N'', N'', N'Task/ViewList.aspx', N'fa-circle-o', 4, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (51, 46, N'任务查看', N'', N'', N'Task/View.aspx', N'fa-circle-o', 5, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (52, 0, N'日程管理', N'', N'', N'#', N'fa-calendar', 9, 0, 0, 1, N'我的日历', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (53, 52, N'日程管理', N'', N'', N'Calendar/List.aspx', N'fa-circle-o', 1, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (54, 52, N'日程编辑', N'', N'', N'Calendar/Edit.aspx', N'fa-circle-o', 2, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (55, 52, N'我的日历', N'', N'', N'Calendar/Month.aspx', N'fa-circle-o', 3, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (56, 52, N'日历接收', N'', N'', N'Calendar/Receiver.aspx', N'fa-circle-o', 4, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (57, 52, N'日历查看', N'', N'', N'Calendar/View.aspx', N'fa-circle-o', 5, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (58, 0, N'工作计划', N'', N'', N'#', N'fa-calendar-check-o', 10, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (59, 58, N'计划管理', N'', N'', N'Plan/List.aspx', N'fa-circle-o', 1, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (60, 58, N'计划编辑', N'', N'', N'Plan/Edit.aspx', N'fa-circle-o', 2, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (61, 58, N'计划报表', N'', N'', N'Plan/Report.aspx', N'fa-circle-o', 3, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (62, 0, N'文件管理', N'', N'', N'#', N'fa-folder-open-o', 11, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (63, 62, N'文件管理', N'', N'', N'Document/List.aspx', N'fa-circle-o', 1, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (64, 62, N'文件编辑', N'', N'', N'Document/Edit.aspx', N'fa-circle-o', 2, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (65, 62, N'文件接收', N'', N'', N'Document/Receiver.aspx', N'fa-circle-o', 3, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (66, 0, N'笔记管理', N'', N'', N'#', N'fa-pencil-square-o', 12, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (67, 66, N'笔记管理', N'', N'', N'Note/List.aspx', N'fa-circle-o', 1, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (68, 66, N'笔记编辑', N'', N'', N'Note/Edit.aspx', N'fa-circle-o', 2, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (69, 66, N'笔记接收', N'', N'', N'Note/Receiver.aspx', N'fa-circle-o', 3, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (70, 66, N'笔记查看', N'', N'', N'Note/View.aspx', N'fa-circle-o', 4, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (71, 0, N'通讯录', N'', N'', N'#', N'fa-phone', 15, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (72, 71, N'通讯录', N'', N'', N'Address/List.aspx', N'fa-circle-o', 1, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (73, 71, N'通讯录编辑', N'', N'', N'Address/Edit.aspx', N'fa-circle-o', 2, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (74, 71, N'通讯录接收', N'', N'', N'Address/Receiver.aspx', N'fa-circle-o', 3, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (75, 71, N'通讯录查看', N'', N'', N'Address/View.aspx', N'fa-circle-o', 4, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (76, 0, N'讨论区', N'', N'', N'#', N'fa-comments-o', 16, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (77, 76, N'讨论区', N'', N'', N'Forum/List.aspx', N'fa-circle-o', 1, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (78, 76, N'讨论区编辑', N'', N'', N'Forum/Edit.aspx', N'fa-circle-o', 2, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (79, 76, N'讨论区接收', N'', N'', N'Forum/Receiver.aspx', N'fa-circle-o', 3, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (80, 76, N'讨论区查看', N'', N'', N'Forum/View.aspx', N'fa-circle-o', 4, 0, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (81, 12, N'在线用户', N'', N'', N'User/Online.aspx', N'fa-circle-o', 8, 0, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (82, 12, N'历史记录', N'', N'', N'User/History.aspx', N'fa-circle-o', 9, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (83, 29, N'流程通知', N'', N'', N'Process/Receiver.aspx', N'fa-circle-o', 5, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (84, 29, N'流程汇总', N'', N'', N'Process/ViewList.aspx', N'fa-circle-o', 6, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (85, 29, N'汇总查看', N'', N'', N'Process/View.aspx', N'fa-circle-o', 7, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (86, 0, N'车辆管理', N'', N'', N'#', N'fa-car', 13, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (87, 86, N'用车管理', N'', N'', N'Car/List.aspx', N'fa-circle-o', 1, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (88, 86, N'用车编辑', N'', N'', N'Car/Edit.aspx', N'fa-circle-o', 2, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (89, 86, N'用车审核', N'', N'', N'Car/CheckList.aspx', N'fa-circle-o', 3, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (90, 86, N'审核编辑', N'', N'', N'Car/Check.aspx', N'fa-circle-o', 4, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (91, 86, N'车辆管理', N'', N'', N'Car/SetList.aspx', N'fa-circle-o', 6, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (92, 86, N'车辆编辑', N'', N'', N'Car/Set.aspx', N'fa-circle-o', 7, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (93, 86, N'用车通知', N'', N'', N'Car/Receiver.aspx', N'fa-circle-o', 5, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (94, 0, N'财务管理', N'', N'', N'#', N'fa-cny', 14, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (95, 94, N'账号管理', N'', N'', N'Finance/Account.aspx', N'fa-circle-o', 1, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (96, 94, N'账号编辑', N'', N'', N'Finance/AccountEdit.aspx', N'fa-circle-o', 2, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (97, 94, N'财务管理', N'', N'', N'Finance/List.aspx', N'fa-circle-o', 3, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (98, 94, N'财务编辑', N'', N'', N'Finance/Edit.aspx', N'fa-circle-o', 4, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (99, 94, N'财务报表', N'', N'', N'Finance/Report.aspx', N'fa-circle-o', 5, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (100, 0, N'工作流管理', N'', N'', N'#', N'fa-sitemap', 0, 0, 0, 1, N'', N'', N'')
GO
print 'Processed 100 total records'
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (101, 100, N'模板管理', N'', N'', N'Workflow/Template.aspx', N'fa-circle-o', 1, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (102, 100, N'模板编辑', N'', N'', N'Workflow/TemplateEdit.aspx', N'fa-circle-o', 2, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (103, 100, N'步骤管理', N'', N'', N'Workflow/Step.aspx', N'fa-circle-o', 3, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (104, 100, N'步骤编辑', N'', N'', N'Workflow/StepEdit.aspx', N'fa-circle-o', 4, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (105, 100, N'签章管理', N'', N'', N'Workflow/Signature.aspx', N'fa-circle-o', 5, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (106, 100, N'签章编辑', N'', N'', N'Workflow/SignatureEdit.aspx', N'fa-circle-o', 6, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (107, 100, N'签章列表', N'', N'', N'Workflow/Sign.aspx', N'fa-circle-o', 7, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (108, 100, N'我的工作', N'', N'', N'Workflow/List.aspx', N'fa-circle-o', 8, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (109, 100, N'工作编辑', N'', N'', N'Workflow/Edit.aspx', N'fa-circle-o', 9, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (110, 100, N'我的审批', N'', N'', N'Workflow/CheckList.aspx', N'fa-circle-o', 10, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (111, 100, N'审批编辑', N'', N'', N'Workflow/Check.aspx', N'fa-circle-o', 11, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (112, 100, N'我的抄送', N'', N'', N'Workflow/ViewList.aspx', N'fa-circle-o', 12, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (113, 100, N'工作查看', N'', N'', N'Workflow/View.aspx', N'fa-circle-o', 13, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (114, 100, N'归档工作', N'', N'', N'Workflow/Complete.aspx', N'fa-circle-o', 14, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (115, 100, N'选择审批人', N'', N'', N'Workflow/Checker.aspx', N'fa-circle-o', 15, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (116, 100, N'审批人列表', N'', N'', N'Workflow/Receiver.aspx', N'fa-circle-o', 16, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (117, 100, N'抄送人列表', N'', N'', N'Workflow/Notify.aspx', N'fa-circle-o', 17, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (118, 100, N'上传附件', N'', N'', N'Workflow/Upload.aspx', N'fa-circle-o', 18, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (119, 100, N'修改记录', N'', N'', N'Workflow/Log.aspx', N'fa-circle-o', 19, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (120, 100, N'流程图', N'', N'', N'Workflow/FlowChart.aspx', N'fa-circle-o', 20, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (121, 0, N'客户管理', N'', N'', N'#', N'fa-user', 0, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (122, 121, N'客户管理', N'', N'', N'Customer/List.aspx', N'fa-circle-o', 1, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (123, 121, N'客户编辑', N'', N'', N'Customer/Edit.aspx', N'fa-circle-o', 2, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (124, 121, N'客户跟进', N'', N'', N'Customer/Follow.aspx', N'fa-circle-o', 3, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (125, 121, N'跟进编辑', N'', N'', N'Customer/FollowEdit.aspx', N'fa-circle-o', 4, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (126, 121, N'查找客户', N'', N'', N'Customer/Search.aspx', N'fa-circle-o', 5, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (127, 121, N'客户联系人', N'', N'', N'Customer/Contact.aspx', N'fa-circle-o', 6, 0, 0, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (128, 121, N'联系人编辑', N'', N'', N'Customer/ContactEdit.aspx', N'fa-circle-o', 7, 0, 1, 1, N'', N'', N'')
INSERT [dbo].[Sys_Menu] ([pk_Menu], [ParentID], [Name_CHS], [Name_CHT], [Name_EN], [Url], [Icon], [SortID], [LevelID], [TypeID], [Visible], [Tag_CHS], [Tag_CHT], [Tag_EN]) VALUES (129, 121, N'客户报表', N'', N'', N'Customer/Report.aspx', N'fa-circle-o', 8, 0, 0, 1, N'', N'', N'')
SET IDENTITY_INSERT [dbo].[Sys_Menu] OFF
/****** Object:  Table [dbo].[Sys_Express]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_Express](
	[pk_Express] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](100) NULL,
	[ShortName] [nvarchar](10) NULL,
	[ImagePath] [nvarchar](200) NULL,
	[Website] [nvarchar](100) NULL,
	[Url] [nvarchar](200) NULL,
	[Freight] [money] NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_Sys_Express] PRIMARY KEY CLUSTERED 
(
	[pk_Express] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Sys_Express] ON
INSERT [dbo].[Sys_Express] ([pk_Express], [FullName], [ShortName], [ImagePath], [Website], [Url], [Freight], [Visible]) VALUES (1, N'顺丰', N'', N'', N'http://www.sf-express.com/', N'http://www.kiees.cn/sf.php?wen=[TrackingNumber]', 30.0000, 1)
INSERT [dbo].[Sys_Express] ([pk_Express], [FullName], [ShortName], [ImagePath], [Website], [Url], [Freight], [Visible]) VALUES (2, N'圆通', N'', N'', N'http://www.yto.net.cn/', N'', 20.0000, 1)
INSERT [dbo].[Sys_Express] ([pk_Express], [FullName], [ShortName], [ImagePath], [Website], [Url], [Freight], [Visible]) VALUES (3, N'申通', N'', N'', N'http://www.sto.cn/', N'', 20.0000, 1)
INSERT [dbo].[Sys_Express] ([pk_Express], [FullName], [ShortName], [ImagePath], [Website], [Url], [Freight], [Visible]) VALUES (4, N'EMS', N'', N'', N'http://www.ems.com.cn/', N'', 20.0000, 1)
INSERT [dbo].[Sys_Express] ([pk_Express], [FullName], [ShortName], [ImagePath], [Website], [Url], [Freight], [Visible]) VALUES (5, N'韵达', N'', N'', N'http://www.yundaex.com/', N'', 20.0000, 1)
SET IDENTITY_INSERT [dbo].[Sys_Express] OFF
/****** Object:  Table [dbo].[Sys_Currency]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_Currency](
	[pk_Currency] [int] IDENTITY(1,1) NOT NULL,
	[C_ID] [int] NULL,
	[C_Sign] [nvarchar](5) NULL,
	[FullName] [nvarchar](100) NULL,
	[ShortName] [nvarchar](10) NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_Sys_Currency] PRIMARY KEY CLUSTERED 
(
	[pk_Currency] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Sys_Currency] ON
INSERT [dbo].[Sys_Currency] ([pk_Currency], [C_ID], [C_Sign], [FullName], [ShortName], [Visible]) VALUES (1, 0, N'￥', N'人民币', N'CNY', 1)
INSERT [dbo].[Sys_Currency] ([pk_Currency], [C_ID], [C_Sign], [FullName], [ShortName], [Visible]) VALUES (2, 1, N'$', N'US Dollar', N'USD', 1)
SET IDENTITY_INSERT [dbo].[Sys_Currency] OFF
/****** Object:  Table [dbo].[Sys_Country]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_Country](
	[pk_Country] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](100) NULL,
	[ShortName] [nvarchar](10) NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_Sys_Country] PRIMARY KEY CLUSTERED 
(
	[pk_Country] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Sys_Country] ON
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (1, N'Afghanistan', N'AF', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (2, N'Albania', N'AL', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (3, N'Armenia', N'AM', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (4, N'Cayman Islands', N'KY', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (5, N'Liberia', N'LR', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (6, N'Sri Lanka', N'LK', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (7, N'Central African Republic', N'CF', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (8, N'Libyan Arab Jamahiriya', N'LY', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (9, N'Sudan', N'SD', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (10, N'Chad', N'TD', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (11, N'Liechtenstein', N'LI', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (12, N'Suriname', N'SR', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (13, N'Chile', N'CL', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (14, N'Lithuania', N'LT', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (15, N'Svalbard and Jan Mayen', N'SJ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (16, N'China', N'CN', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (17, N'Luxembourg', N'LU', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (18, N'Swaziland', N'SZ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (19, N'Christmas Island', N'CX', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (20, N'Macao', N'MO', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (21, N'Sweden', N'SE', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (22, N'Cocos (Keeling) Islands', N'CC', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (23, N'Macedonia', N'MK', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (24, N'Switzerland', N'CH', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (25, N'Colombia', N'CO', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (26, N'Madagascar', N'MG', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (27, N'Syrian Arab Republic', N'SY', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (28, N'Comoros', N'KM', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (29, N'Malawi', N'MW', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (30, N'Taiwan', N'TW', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (31, N'Congo', N'CG', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (32, N'Malaysia', N'MY', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (33, N'Tajikistan', N'TJ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (34, N'Congo, the Democratic Republic of the', N'CD', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (35, N'Maldives', N'MV', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (36, N'Tanzania, United Republic of', N'TZ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (37, N'Cook Islands', N'CK', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (38, N'Mali', N'ML', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (39, N'Thailand', N'TH', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (40, N'Costa Rica', N'CR', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (41, N'Malta', N'MT', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (42, N'Timor-Leste', N'TL', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (43, N'Cote D’Ivoire', N'CI', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (44, N'Marshall Islands', N'MH', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (45, N'Togo', N'TG', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (46, N'Croatia', N'HR', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (47, N'Martinique', N'MQ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (48, N'Tokelau', N'TK', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (49, N'Cuba', N'CU', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (50, N'Mauritania', N'MR', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (51, N'Tonga', N'TO', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (52, N'Cyprus', N'CY', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (53, N'Mauritius', N'MU', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (54, N'Trinidad and Tobago', N'TT', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (55, N'Czech Republic', N'CZ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (56, N'Mayotte', N'YT', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (57, N'Tunisia', N'TN', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (58, N'Denmark', N'DK', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (59, N'Mexico', N'MX', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (60, N'Turkey', N'TR', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (61, N'Djibouti', N'DJ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (62, N'Micronesia, Federate States of', N'FM', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (63, N'Turkmenistan', N'TM', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (64, N'Dominica', N'DM', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (65, N'Moldova, Republic of', N'MD', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (66, N'Turks and Caicos Islands', N'TC', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (67, N'Dominican Republic', N'DO', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (68, N'Monaco', N'MC', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (69, N'Tuvalu', N'TV', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (70, N'Ecuador', N'EC', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (71, N'Mongolia', N'MN', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (72, N'Uganda', N'UG', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (73, N'Egypt', N'EG', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (74, N'Montserrat', N'MS', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (75, N'Ukraine', N'UA', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (76, N'El Salvador', N'SV', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (77, N'Morocco', N'MA', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (78, N'United Arab Emirates', N'AE.', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (79, N'Equatorial Guinea', N'GQ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (80, N'Mozambique', N'MZ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (81, N'United Kingdom', N'GB', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (82, N'Eritrea', N'ER', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (83, N'Myanmar', N'MM', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (84, N'United States', N'US', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (85, N'Estonia', N'EE', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (86, N'Namibia', N'NA', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (87, N'United States Minor Outlying Islands', N'UM', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (88, N'Ethiopia', N'ET', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (89, N'Nauru', N'NR', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (90, N'Uruguay', N'UY', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (91, N'Falkland Islands (Malvinas)', N'FK', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (92, N'Nepal', N'NP', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (93, N'Uzbekistan', N'UZ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (94, N'Faroe Islands', N'FO', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (95, N'Netherlands', N'NL', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (96, N'Vanuatu', N'VU', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (97, N'Fiji', N'FJ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (98, N'Netherlands Antilles', N'AN', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (99, N'Finland', N'FI', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (100, N'New Caledonia', N'NC', 1)
GO
print 'Processed 100 total records'
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (101, N'Venezuela', N'VE', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (102, N'France', N'FR', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (103, N'New Zealand', N'NZ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (104, N'Viet Nam', N'VN', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (105, N'French Guiana', N'GF', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (106, N'Nicaragua', N'NI', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (107, N'Virgin Islands, British', N'VG', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (108, N'French Polynesia', N'PF', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (109, N'Niger', N'NE', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (110, N'Wallis and Futuna', N'WF', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (111, N'French Southern Territories', N'TF', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (112, N'Nigeria', N'NG', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (113, N'Western Sahara', N'EH', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (114, N'Gabon', N'GA', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (115, N'Niue', N'NU', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (116, N'Yemen', N'YE', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (117, N'Gambia', N'GM', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (118, N'Norfolk Island', N'NF', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (119, N'Zambia', N'ZM', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (120, N'Georgia', N'GE', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (121, N'Northern Mariana Islands', N'MP', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (122, N'Zimbabwe', N'ZW', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (123, N'Germany', N'DE', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (124, N'Norway', N'NO', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (125, N'Aland Islands', N'AX', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (126, N'Ghana', N'GH', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (127, N'Oman', N'OM', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (128, N'Gibraltar', N'GI', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (129, N'Pakistan', N'PK', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (130, N'Algeria', N'DZ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (131, N'Greece', N'GR', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (132, N'Palau', N'PW', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (133, N'American Samoa', N'AS', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (134, N'Greenland', N'GL', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (135, N'Palestinian Territory, Occupied', N'PS', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (136, N'Andorra', N'AD', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (137, N'Grenada', N'GD', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (138, N'Panama', N'PA', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (139, N'Angola', N'AO', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (140, N'Guadeloupe', N'GP', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (141, N'Papua New Guinea', N'PG', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (142, N'Anguilla', N'AI', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (143, N'Guam', N'GU', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (144, N'Paraguay', N'PY', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (145, N'Antarctica', N'AQ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (146, N'Guatemala', N'GT', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (147, N'Peru', N'PE', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (148, N'Antigua and Barbuda', N'AG', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (149, N'Guinea', N'GN', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (150, N'Philippines', N'PH', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (151, N'Argentina', N'AR', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (152, N'Guinea-Bissau', N'GW', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (153, N'Pitcairn', N'PN', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (154, N'Guyana', N'GY', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (155, N'Poland', N'PL', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (156, N'Aruba', N'AW', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (157, N'Haiti', N'HT', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (158, N'Portugal', N'PT', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (159, N'Australia', N'AU', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (160, N'Heard Island and McDonald Islands', N'HM', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (161, N'Puerto Rico', N'PR', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (162, N'Austria', N'AT', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (163, N'Holy See (Vatican City State)', N'VA', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (164, N'Qatar', N'QA', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (165, N'Azerbaijan', N'AZ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (166, N'Honduras', N'HN', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (167, N'Reunion', N'RE', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (168, N'Bahamas', N'BS', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (169, N'Hong Kong', N'HK', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (170, N'Romania', N'RO', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (171, N'Bahrain', N'BH', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (172, N'Hungary', N'HU', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (173, N'Russian Federation', N'RU', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (174, N'Bangladesh', N'BD', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (175, N'Iceland', N'IS', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (176, N'Rwanda', N'RW', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (177, N'Barbados', N'BB', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (178, N'India', N'IN', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (179, N'Saint Helena', N'SH', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (180, N'Belarus', N'BY', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (181, N'Indonesia', N'ID', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (182, N'Saint Kitts and Nevis', N'KN', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (183, N'Belgium', N'BE', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (184, N'Iran, Islamic Republic of', N'IR', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (185, N'Saint Lucia', N'LC', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (186, N'Belize', N'BZ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (187, N'Iraq', N'IQ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (188, N'Saint Pierre and Miquelon', N'PM', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (189, N'Benin', N'BJ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (190, N'Ireland', N'IE', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (191, N'Saint Vincent and the Grenadines', N'VC', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (192, N'Bermuda', N'BM', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (193, N'Israel', N'IL', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (194, N'Samoa', N'WS', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (195, N'Bhutan', N'BT', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (196, N'Italy', N'IT', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (197, N'San Marino', N'SM', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (198, N'Bolivia', N'BO', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (199, N'Jamaica', N'JM', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (200, N'Sao Tome and Principe', N'ST', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (201, N'Bosnia and Herzegovina', N'BA', 1)
GO
print 'Processed 200 total records'
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (202, N'Japan', N'JP', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (203, N'Saudi Arabia', N'SA', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (204, N'Botswana', N'BW', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (205, N'Jordan', N'JO', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (206, N'Senegal', N'SN', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (207, N'Bouvet Island', N'BV', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (208, N'Kazakhstan', N'KZ', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (209, N'Serbia and Montenegro', N'CS', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (210, N'Brazil', N'BR', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (211, N'Kenya', N'KE', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (212, N'Seychelles', N'SC', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (213, N'British Indian Ocean Territory', N'IO', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (214, N'Kiribati', N'KI', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (215, N'Sierra Leone', N'SL', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (216, N'Brunei Darussalam', N'BN', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (217, N'Korea, Democratic People’s Republic of', N'KP', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (218, N'Singapore', N'SG', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (219, N'Bulgaria', N'BG', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (220, N'Korea, Republic of', N'KR', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (221, N'Slovakia', N'SK', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (222, N'Burkina Faso', N'BF', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (223, N'Kuwait', N'KW', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (224, N'Slovenia', N'SI', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (225, N'Cambodia', N'KH', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (226, N'Lao People’s Democratic Republic', N'LA', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (227, N'Somalia', N'SO', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (228, N'Cameroon', N'CM', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (229, N'Latvia', N'LV', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (230, N'South Africa', N'ZA', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (231, N'Canada', N'CA', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (232, N'Lebanon', N'LB', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (233, N'South Georgia and the South Sandwich Islands', N'GS', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (234, N'Cape Verde', N'CV', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (235, N'Lesotho', N'LS', 1)
INSERT [dbo].[Sys_Country] ([pk_Country], [FullName], [ShortName], [Visible]) VALUES (236, N'Spain', N'ES', 1)
SET IDENTITY_INSERT [dbo].[Sys_Country] OFF
/****** Object:  Table [dbo].[Sys_City]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_City](
	[pk_City] [int] IDENTITY(1,1) NOT NULL,
	[fk_Province] [int] NULL,
	[FullName] [nvarchar](100) NULL,
	[ShortName] [nvarchar](10) NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_Sys_City] PRIMARY KEY CLUSTERED 
(
	[pk_City] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role_Power]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role_Power](
	[pk_Power] [int] IDENTITY(1,1) NOT NULL,
	[ValueName_CHS] [nvarchar](50) NULL,
	[ValueName_CHT] [nvarchar](50) NULL,
	[ValueName_EN] [nvarchar](50) NULL,
	[ValueKey] [int] NULL,
	[SortID] [int] NULL,
 CONSTRAINT [PK_Role_Power] PRIMARY KEY CLUSTERED 
(
	[pk_Power] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role_Name]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role_Name](
	[pk_Name] [int] IDENTITY(1,1) NOT NULL,
	[RoleName_CHS] [nvarchar](50) NULL,
	[RoleName_CHT] [nvarchar](50) NULL,
	[RoleName_EN] [nvarchar](50) NULL,
	[PowerValue] [int] NULL,
	[fk_Company] [int] NULL,
 CONSTRAINT [PK_Role_Name] PRIMARY KEY CLUSTERED 
(
	[pk_Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Role_Name] ON
INSERT [dbo].[Role_Name] ([pk_Name], [RoleName_CHS], [RoleName_CHT], [RoleName_EN], [PowerValue], [fk_Company]) VALUES (1, N'超级管理员', N'', N'', 100, 0)
INSERT [dbo].[Role_Name] ([pk_Name], [RoleName_CHS], [RoleName_CHT], [RoleName_EN], [PowerValue], [fk_Company]) VALUES (2, N'总经理', N'', N'', 90, 0)
INSERT [dbo].[Role_Name] ([pk_Name], [RoleName_CHS], [RoleName_CHT], [RoleName_EN], [PowerValue], [fk_Company]) VALUES (3, N'部门经理', N'', N'', 80, 0)
INSERT [dbo].[Role_Name] ([pk_Name], [RoleName_CHS], [RoleName_CHT], [RoleName_EN], [PowerValue], [fk_Company]) VALUES (4, N'办公室主任', N'', N'', 60, 0)
INSERT [dbo].[Role_Name] ([pk_Name], [RoleName_CHS], [RoleName_CHT], [RoleName_EN], [PowerValue], [fk_Company]) VALUES (5, N'文员', N'', N'', 20, 0)
SET IDENTITY_INSERT [dbo].[Role_Name] OFF
/****** Object:  Table [dbo].[Role_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role_List](
	[pk_Role] [int] IDENTITY(1,1) NOT NULL,
	[fk_RoleName] [int] NULL,
	[fk_Menu] [int] NULL,
	[IsUse] [bit] NULL,
	[IsAdmin] [bit] NULL,
	[PowerList] [nvarchar](500) NULL,
	[fk_Company] [int] NULL,
 CONSTRAINT [PK_Role_List] PRIMARY KEY CLUSTERED 
(
	[pk_Role] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Role_List] ON
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (1, 1, 1, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (2, 1, 2, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (3, 1, 5, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (4, 1, 4, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (5, 1, 6, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (6, 1, 3, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (7, 1, 7, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (8, 1, 8, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (9, 1, 9, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (10, 1, 10, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (11, 1, 11, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (12, 1, 16, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (13, 1, 12, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (14, 1, 13, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (15, 1, 14, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (16, 1, 15, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (17, 1, 17, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (18, 1, 18, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (19, 1, 19, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (20, 1, 20, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (21, 1, 21, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (22, 1, 22, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (23, 1, 23, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (24, 1, 24, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (25, 1, 25, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (26, 1, 26, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (27, 1, 27, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (28, 1, 28, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (29, 1, 29, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (30, 1, 30, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (31, 1, 31, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (32, 1, 32, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (33, 1, 33, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (34, 1, 34, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (35, 1, 35, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (36, 1, 36, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (37, 1, 37, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (38, 1, 38, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (39, 2, 10, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (40, 2, 11, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (41, 2, 1, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (42, 2, 2, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (43, 2, 5, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (44, 2, 16, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (45, 2, 4, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (46, 2, 6, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (47, 2, 21, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (48, 2, 22, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (49, 2, 3, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (50, 2, 7, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (51, 2, 8, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (52, 2, 9, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (53, 2, 12, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (54, 2, 13, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (55, 2, 17, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (56, 2, 14, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (57, 2, 18, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (58, 2, 15, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (59, 2, 19, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (60, 2, 20, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (61, 2, 23, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (62, 2, 24, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (63, 2, 25, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (64, 2, 26, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (65, 2, 27, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (66, 2, 28, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (67, 2, 29, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (68, 2, 30, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (69, 2, 31, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (70, 2, 32, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (71, 2, 33, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (72, 2, 34, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (73, 2, 35, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (74, 2, 36, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (75, 2, 37, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (76, 2, 38, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (77, 4, 10, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (78, 4, 11, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (79, 4, 1, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (80, 4, 2, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (81, 4, 5, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (82, 4, 16, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (83, 4, 4, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (84, 4, 6, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (85, 4, 21, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (86, 4, 22, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (87, 4, 3, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (88, 4, 7, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (89, 4, 8, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (90, 4, 9, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (91, 4, 12, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (92, 4, 13, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (93, 4, 17, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (94, 4, 14, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (95, 4, 18, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (96, 4, 15, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (97, 4, 19, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (98, 4, 20, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (99, 4, 23, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (100, 4, 24, 1, 0, N'', 0)
GO
print 'Processed 100 total records'
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (101, 4, 25, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (102, 4, 26, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (103, 4, 27, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (104, 4, 28, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (105, 4, 29, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (106, 4, 30, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (107, 4, 31, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (108, 4, 32, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (109, 4, 33, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (110, 4, 34, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (111, 4, 35, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (112, 4, 36, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (113, 4, 37, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (114, 4, 38, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (115, 3, 10, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (116, 3, 11, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (117, 3, 1, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (118, 3, 2, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (119, 3, 5, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (120, 3, 16, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (121, 3, 4, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (122, 3, 6, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (123, 3, 21, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (124, 3, 22, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (125, 3, 3, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (126, 3, 7, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (127, 3, 8, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (128, 3, 9, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (129, 3, 12, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (130, 3, 13, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (131, 3, 17, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (132, 3, 14, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (133, 3, 18, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (134, 3, 15, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (135, 3, 19, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (136, 3, 20, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (137, 3, 23, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (138, 3, 24, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (139, 3, 25, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (140, 3, 26, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (141, 3, 27, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (142, 3, 28, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (143, 3, 29, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (144, 3, 30, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (145, 3, 31, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (146, 3, 32, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (147, 3, 33, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (148, 3, 34, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (149, 3, 35, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (150, 3, 36, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (151, 3, 37, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (152, 3, 38, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (153, 5, 10, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (154, 5, 11, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (155, 5, 1, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (156, 5, 2, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (157, 5, 5, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (158, 5, 16, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (159, 5, 4, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (160, 5, 6, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (161, 5, 21, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (162, 5, 22, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (163, 5, 3, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (164, 5, 7, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (165, 5, 8, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (166, 5, 9, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (167, 5, 12, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (168, 5, 13, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (169, 5, 17, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (170, 5, 14, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (171, 5, 18, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (172, 5, 15, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (173, 5, 19, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (174, 5, 20, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (175, 5, 23, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (176, 5, 24, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (177, 5, 25, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (178, 5, 26, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (179, 5, 27, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (180, 5, 28, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (181, 5, 29, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (182, 5, 30, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (183, 5, 31, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (184, 5, 32, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (185, 5, 33, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (186, 5, 34, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (187, 5, 35, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (188, 5, 36, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (189, 5, 37, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (190, 5, 38, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (191, 1, 39, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (192, 1, 40, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (193, 1, 41, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (194, 1, 42, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (195, 1, 43, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (196, 1, 44, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (197, 1, 45, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (198, 1, 46, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (199, 1, 47, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (200, 1, 48, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (201, 1, 49, 1, 0, N'', 0)
GO
print 'Processed 200 total records'
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (202, 1, 50, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (203, 1, 51, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (204, 1, 52, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (205, 1, 53, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (206, 1, 54, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (207, 4, 39, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (208, 4, 40, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (209, 4, 41, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (210, 4, 42, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (211, 4, 43, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (212, 4, 44, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (213, 4, 45, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (214, 4, 46, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (215, 4, 47, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (216, 4, 48, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (217, 4, 49, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (218, 4, 50, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (219, 4, 51, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (220, 4, 52, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (221, 4, 53, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (222, 4, 54, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (223, 1, 55, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (224, 1, 56, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (225, 1, 57, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (226, 1, 58, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (227, 1, 59, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (228, 1, 60, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (229, 1, 61, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (230, 1, 62, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (231, 1, 63, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (232, 1, 64, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (233, 1, 65, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (234, 1, 66, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (235, 1, 67, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (236, 1, 68, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (237, 1, 69, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (238, 1, 70, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (239, 1, 71, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (240, 1, 72, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (241, 1, 73, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (242, 1, 74, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (243, 1, 75, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (244, 1, 76, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (245, 1, 77, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (246, 1, 78, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (247, 1, 79, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (248, 1, 80, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (249, 1, 81, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (250, 2, 81, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (251, 2, 39, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (252, 2, 40, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (253, 2, 41, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (254, 2, 42, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (255, 2, 43, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (256, 2, 44, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (257, 2, 45, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (258, 2, 46, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (259, 2, 47, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (260, 2, 48, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (261, 2, 49, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (262, 2, 50, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (263, 2, 51, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (264, 2, 52, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (265, 2, 53, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (266, 2, 54, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (267, 2, 55, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (268, 2, 56, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (269, 2, 57, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (270, 2, 58, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (271, 2, 59, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (272, 2, 60, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (273, 2, 61, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (274, 2, 62, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (275, 2, 63, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (276, 2, 64, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (277, 2, 65, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (278, 2, 66, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (279, 2, 67, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (280, 2, 68, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (281, 2, 69, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (282, 2, 70, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (283, 2, 71, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (284, 2, 72, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (285, 2, 73, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (286, 2, 74, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (287, 2, 75, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (288, 2, 76, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (289, 2, 77, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (290, 2, 78, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (291, 2, 79, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (292, 2, 80, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (293, 3, 81, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (294, 3, 39, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (295, 3, 40, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (296, 3, 41, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (297, 3, 42, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (298, 3, 43, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (299, 3, 44, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (300, 3, 45, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (301, 3, 46, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (302, 3, 47, 0, 0, N'', 0)
GO
print 'Processed 300 total records'
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (303, 3, 48, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (304, 3, 49, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (305, 3, 50, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (306, 3, 51, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (307, 3, 52, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (308, 3, 53, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (309, 3, 54, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (310, 3, 55, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (311, 3, 56, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (312, 3, 57, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (313, 3, 58, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (314, 3, 59, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (315, 3, 60, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (316, 3, 61, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (317, 3, 62, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (318, 3, 63, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (319, 3, 64, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (320, 3, 65, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (321, 3, 66, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (322, 3, 67, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (323, 3, 68, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (324, 3, 69, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (325, 3, 70, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (326, 3, 71, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (327, 3, 72, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (328, 3, 73, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (329, 3, 74, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (330, 3, 75, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (331, 3, 76, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (332, 3, 77, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (333, 3, 78, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (334, 3, 79, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (335, 3, 80, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (336, 4, 81, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (337, 4, 55, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (338, 4, 56, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (339, 4, 57, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (340, 4, 58, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (341, 4, 59, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (342, 4, 60, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (343, 4, 61, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (344, 4, 62, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (345, 4, 63, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (346, 4, 64, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (347, 4, 65, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (348, 4, 66, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (349, 4, 67, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (350, 4, 68, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (351, 4, 69, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (352, 4, 70, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (353, 4, 71, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (354, 4, 72, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (355, 4, 73, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (356, 4, 74, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (357, 4, 75, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (358, 4, 76, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (359, 4, 77, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (360, 4, 78, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (361, 4, 79, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (362, 4, 80, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (363, 5, 81, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (364, 5, 39, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (365, 5, 40, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (366, 5, 41, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (367, 5, 42, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (368, 5, 43, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (369, 5, 44, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (370, 5, 45, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (371, 5, 46, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (372, 5, 47, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (373, 5, 48, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (374, 5, 49, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (375, 5, 50, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (376, 5, 51, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (377, 5, 52, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (378, 5, 53, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (379, 5, 54, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (380, 5, 55, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (381, 5, 56, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (382, 5, 57, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (383, 5, 58, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (384, 5, 59, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (385, 5, 60, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (386, 5, 61, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (387, 5, 62, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (388, 5, 63, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (389, 5, 64, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (390, 5, 65, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (391, 5, 66, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (392, 5, 67, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (393, 5, 68, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (394, 5, 69, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (395, 5, 70, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (396, 5, 71, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (397, 5, 72, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (398, 5, 73, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (399, 5, 74, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (400, 5, 75, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (401, 5, 76, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (402, 5, 77, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (403, 5, 78, 0, 0, N'', 0)
GO
print 'Processed 400 total records'
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (404, 5, 79, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (405, 5, 80, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (406, 1, 82, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (407, 2, 82, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (408, 3, 82, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (409, 4, 82, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (410, 5, 82, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (411, 1, 83, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (412, 2, 83, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (413, 3, 83, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (414, 4, 83, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (415, 5, 83, 1, 1, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (416, 1, 84, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (417, 1, 85, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (418, 1, 86, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (419, 1, 87, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (420, 1, 88, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (421, 1, 89, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (422, 1, 90, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (423, 1, 91, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (424, 1, 92, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (425, 2, 84, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (426, 2, 85, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (427, 2, 86, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (428, 2, 87, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (429, 2, 88, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (430, 2, 89, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (431, 2, 90, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (432, 2, 91, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (433, 2, 92, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (434, 3, 84, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (435, 3, 85, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (436, 3, 86, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (437, 3, 87, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (438, 3, 88, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (439, 3, 89, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (440, 3, 90, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (441, 3, 91, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (442, 3, 92, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (443, 4, 84, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (444, 4, 85, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (445, 4, 86, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (446, 4, 87, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (447, 4, 88, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (448, 4, 89, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (449, 4, 90, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (450, 4, 91, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (451, 4, 92, 0, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (452, 1, 93, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (453, 2, 93, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (454, 3, 93, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (455, 4, 93, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (456, 1, 94, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (457, 1, 95, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (458, 1, 96, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (459, 1, 97, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (460, 1, 98, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (461, 1, 99, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (462, 1, 100, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (463, 1, 101, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (464, 1, 102, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (465, 1, 103, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (466, 1, 104, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (467, 1, 105, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (468, 1, 106, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (469, 1, 107, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (470, 1, 108, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (471, 1, 109, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (472, 1, 110, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (473, 1, 111, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (474, 1, 112, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (475, 1, 113, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (476, 1, 114, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (477, 1, 115, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (478, 1, 116, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (479, 1, 117, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (480, 1, 118, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (481, 1, 119, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (482, 1, 120, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (483, 1, 121, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (484, 1, 122, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (485, 1, 123, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (486, 1, 124, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (487, 1, 125, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (488, 1, 126, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (489, 1, 127, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (490, 1, 128, 1, 0, N'', 0)
INSERT [dbo].[Role_List] ([pk_Role], [fk_RoleName], [fk_Menu], [IsUse], [IsAdmin], [PowerList], [fk_Company]) VALUES (491, 1, 129, 1, 0, N'', 0)
SET IDENTITY_INSERT [dbo].[Role_List] OFF
/****** Object:  Table [dbo].[Process_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Process_List](
	[pk_Process] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](1000) NULL,
	[Note] [nvarchar](1000) NULL,
	[ImagePath] [nvarchar](200) NULL,
	[FilePath] [nvarchar](200) NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[IsCheck] [bit] NULL,
	[CheckUser] [int] NULL,
	[CheckDate] [datetime] NULL,
	[CheckDepartment] [int] NULL,
	[AtUser] [nvarchar](500) NULL,
	[Tags] [nvarchar](200) NULL,
	[ParentID] [int] NULL,
 CONSTRAINT [PK_Process_List] PRIMARY KEY CLUSTERED 
(
	[pk_Process] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Process_List] ON
INSERT [dbo].[Process_List] ([pk_Process], [fk_User], [fk_Department], [TypeID], [StatusID], [Title], [Description], [Note], [ImagePath], [FilePath], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate], [IsCheck], [CheckUser], [CheckDate], [CheckDepartment], [AtUser], [Tags], [ParentID]) VALUES (1, 1, 1, 2, 1, N'购买5颗金士顿固态硬盘', N'商品描述：金士顿固态硬盘（128G）
商品价格：299/颗
商品数量：5
购买原因：升级公司电脑硬件，提高工作效率', N'同意，请尽快去办', N'', N'', 0, 1, CAST(0x0000A52D00ACE810 AS DateTime), 1, CAST(0x0000A52D01106CA0 AS DateTime), 1, 1, CAST(0x0000A52D01106CA0 AS DateTime), 1, N'', N'', 0)
INSERT [dbo].[Process_List] ([pk_Process], [fk_User], [fk_Department], [TypeID], [StatusID], [Title], [Description], [Note], [ImagePath], [FilePath], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate], [IsCheck], [CheckUser], [CheckDate], [CheckDepartment], [AtUser], [Tags], [ParentID]) VALUES (2, 1, 1, 2, 1, N'购买5颗金士顿固态硬盘', N'商品描述：金士顿固态硬盘（128G）', N'同意，请尽快去办', N'', N'', 0, 1, CAST(0x0000A52D00ACE810 AS DateTime), 1, CAST(0x0000A52D01106CA0 AS DateTime), 1, 1, CAST(0x0000A52D01106CA0 AS DateTime), 1, N'', N'', 1)
SET IDENTITY_INSERT [dbo].[Process_List] OFF
/****** Object:  Table [dbo].[Plan_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Plan_List](
	[pk_Plan] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL,
	[Note] [nvarchar](1000) NULL,
	[Feedback] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](200) NULL,
	[FilePath] [nvarchar](200) NULL,
	[UserList] [nvarchar](1000) NULL,
	[DepartmentList] [nvarchar](1000) NULL,
	[RoleList] [nvarchar](1000) NULL,
	[Url] [nvarchar](200) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Plan_List] PRIMARY KEY CLUSTERED 
(
	[pk_Plan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[pagination3]    Script Date: 05/16/2019 21:46:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[pagination3]  --新建存储过程用:CREATE PROCEDURE [dbo].[pagination3]
@tblName varchar(50),         --表名
@strGetFields varchar(5000) = '*',     --字段名(全部字段为*)
@fldName varchar(5000),         --排序字段(必须!支持多字段)
@strWhere varchar(5000) = Null,--条件语句(不用加where)
@pageSize int,                     --每页多少条记录
@pageIndex int = 1 ,             --指定当前为第几页
@OrderType bit=0,           -- 设置排序类型, 非 0 值则降序 
@doCount bit = 0 
as
begin
     Declare @sql nvarchar(4000)    
     --计算总记录数
     if @doCount != 0
		begin
			if (@strWhere='' or @strWhere=NULL)
				set @sql = 'select count(*) as Total from [' + @tblName + ']'
			else
				set @sql = 'select count(*) as Total from [' + @tblName + '] where '+@strWhere
		end      

	else
		begin   
			 if (@strWhere='' or @strWhere=NULL)
				if(@OrderType=1)
					set @sql = 'Select * FROM (select ROW_NUMBER() Over(order by ' + @fldName + ' desc) as rowId,' + @strGetFields + ' from ' + @tblName 
				else
					set @sql = 'Select * FROM (select ROW_NUMBER() Over(order by ' + @fldName + ' asc) as rowId,' + @strGetFields + ' from ' + @tblName 
			 else
				 if(@OrderType=1)
					set @sql = 'Select * FROM (select ROW_NUMBER() Over(order by ' + @fldName + ' desc) as rowId,' + @strGetFields + ' from ' + @tblName + ' where ' + @strWhere    
				else
					set @sql = 'Select * FROM (select ROW_NUMBER() Over(order by ' + @fldName + ' asc) as rowId,' + @strGetFields + ' from ' + @tblName + ' where ' + @strWhere   

			  --处理开始点和结束点
			 Declare @StartRecord int
			 Declare @EndRecord int 
		    
			 set @StartRecord = (@pageIndex-1)*@PageSize + 1
			 set @EndRecord = @StartRecord + @pageSize - 1

			 --继续合成sql语句
			if(@OrderType=1)
				set @Sql = @Sql + ') as ' + @tblName + ' where rowId between ' + Convert(varchar(50),@StartRecord) + ' and ' +   Convert(varchar(50),@EndRecord) + 'order  by '+@fldName+' desc'
			else
				set @Sql = @Sql + ') as ' + @tblName + ' where rowId between ' + Convert(varchar(50),@StartRecord) + ' and ' +   Convert(varchar(50),@EndRecord) + 'order  by '+@fldName+' asc'
		    
		end
end
Exec(@sql)
GO
/****** Object:  Table [dbo].[Notice_Receiver]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notice_Receiver](
	[pk_Receiver] [int] IDENTITY(1,1) NOT NULL,
	[fk_Notice] [int] NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[IsRead] [bit] NULL,
	[ReadDate] [datetime] NULL,
 CONSTRAINT [PK_Notice_Receiver] PRIMARY KEY CLUSTERED 
(
	[pk_Receiver] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Notice_Receiver] ON
INSERT [dbo].[Notice_Receiver] ([pk_Receiver], [fk_Notice], [fk_User], [fk_Department], [IsRead], [ReadDate]) VALUES (1, 1, 1, 1, 0, CAST(0x0000A538018A1B68 AS DateTime))
INSERT [dbo].[Notice_Receiver] ([pk_Receiver], [fk_Notice], [fk_User], [fk_Department], [IsRead], [ReadDate]) VALUES (2, 2, 1, 1, 1, CAST(0x0000A53A015F3951 AS DateTime))
INSERT [dbo].[Notice_Receiver] ([pk_Receiver], [fk_Notice], [fk_User], [fk_Department], [IsRead], [ReadDate]) VALUES (3, 3, 1, 1, 1, CAST(0x0000A55601668138 AS DateTime))
INSERT [dbo].[Notice_Receiver] ([pk_Receiver], [fk_Notice], [fk_User], [fk_Department], [IsRead], [ReadDate]) VALUES (4, 1, 2, 2, 0, CAST(0x0000A538018A1B68 AS DateTime))
INSERT [dbo].[Notice_Receiver] ([pk_Receiver], [fk_Notice], [fk_User], [fk_Department], [IsRead], [ReadDate]) VALUES (5, 2, 2, 2, 0, CAST(0x0000A53A015F3951 AS DateTime))
INSERT [dbo].[Notice_Receiver] ([pk_Receiver], [fk_Notice], [fk_User], [fk_Department], [IsRead], [ReadDate]) VALUES (6, 3, 2, 2, 0, CAST(0x0000A53A016CA538 AS DateTime))
INSERT [dbo].[Notice_Receiver] ([pk_Receiver], [fk_Notice], [fk_User], [fk_Department], [IsRead], [ReadDate]) VALUES (7, 1, 3, 3, 1, CAST(0x0000A53A01756D8D AS DateTime))
INSERT [dbo].[Notice_Receiver] ([pk_Receiver], [fk_Notice], [fk_User], [fk_Department], [IsRead], [ReadDate]) VALUES (8, 2, 3, 3, 1, CAST(0x0000A53A01756A9F AS DateTime))
INSERT [dbo].[Notice_Receiver] ([pk_Receiver], [fk_Notice], [fk_User], [fk_Department], [IsRead], [ReadDate]) VALUES (9, 3, 3, 3, 1, CAST(0x0000A53A01756758 AS DateTime))
INSERT [dbo].[Notice_Receiver] ([pk_Receiver], [fk_Notice], [fk_User], [fk_Department], [IsRead], [ReadDate]) VALUES (10, 4, 2, 2, 1, CAST(0x0000A8CA00F15DB7 AS DateTime))
SET IDENTITY_INSERT [dbo].[Notice_Receiver] OFF
/****** Object:  Table [dbo].[Notice_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notice_List](
	[pk_Notice] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL,
	[Note] [nvarchar](1000) NULL,
	[ImagePath] [nvarchar](200) NULL,
	[FilePath] [nvarchar](200) NULL,
	[UserList] [nvarchar](1000) NULL,
	[DepartmentList] [nvarchar](1000) NULL,
	[RoleList] [nvarchar](1000) NULL,
	[Url] [nvarchar](200) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[IsTop] [bit] NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Notice_List] PRIMARY KEY CLUSTERED 
(
	[pk_Notice] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Notice_List] ON
INSERT [dbo].[Notice_List] ([pk_Notice], [fk_User], [fk_Department], [TypeID], [StatusID], [Title], [Description], [Note], [ImagePath], [FilePath], [UserList], [DepartmentList], [RoleList], [Url], [StartDate], [EndDate], [IsTop], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (1, 1, 1, 0, 1, N'关于年度总结报告', N'请各部门经理做好年终总结报告，以便在年会上发表', N'', N'', N'', N'', N'', N'', N'', CAST(0x0000A53800000000 AS DateTime), CAST(0x0000A53F00000000 AS DateTime), 0, 0, 1, CAST(0x0000A538018A1B68 AS DateTime), 1, CAST(0x0000A53B008F86F8 AS DateTime))
INSERT [dbo].[Notice_List] ([pk_Notice], [fk_User], [fk_Department], [TypeID], [StatusID], [Title], [Description], [Note], [ImagePath], [FilePath], [UserList], [DepartmentList], [RoleList], [Url], [StartDate], [EndDate], [IsTop], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (2, 1, 1, 1, 0, N'元旦放假3天', N'请各部门做好节前卫生工作，祝大家元旦快乐', N'', N'', N'', N'', N'', N'', N'', CAST(0x0000A53900000000 AS DateTime), CAST(0x0000A54000000000 AS DateTime), 0, 0, 1, CAST(0x0000A539016C3E18 AS DateTime), 0, CAST(0x0000A539016C3E18 AS DateTime))
INSERT [dbo].[Notice_List] ([pk_Notice], [fk_User], [fk_Department], [TypeID], [StatusID], [Title], [Description], [Note], [ImagePath], [FilePath], [UserList], [DepartmentList], [RoleList], [Url], [StartDate], [EndDate], [IsTop], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (3, 1, 1, 1, 2, N'周一上午全体员工会议', N'请各部门同事务必9点到会议室开会，不得迟到！', N'', N'', N'', N'', N'', N'', N'', CAST(0x0000A53A00000000 AS DateTime), CAST(0x0000A54100000000 AS DateTime), 0, 0, 1, CAST(0x0000A53A016CA538 AS DateTime), 0, CAST(0x0000A53A016CA538 AS DateTime))
INSERT [dbo].[Notice_List] ([pk_Notice], [fk_User], [fk_Department], [TypeID], [StatusID], [Title], [Description], [Note], [ImagePath], [FilePath], [UserList], [DepartmentList], [RoleList], [Url], [StartDate], [EndDate], [IsTop], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (4, 2, 2, 1, 0, N'有1个用车需要您审核', N'请到用车审核查看', N'', N'', N'', N'', N'', N'', N'../Car/CheckList.aspx?active=86,89', CAST(0x0000A8CA00DE6660 AS DateTime), CAST(0x0000A8CA00EEE120 AS DateTime), 0, 0, 1, CAST(0x0000A8CA00DE6660 AS DateTime), 0, CAST(0x0000A8CA00DE6660 AS DateTime))
SET IDENTITY_INSERT [dbo].[Notice_List] OFF
/****** Object:  Table [dbo].[Note_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Note_List](
	[pk_Note] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL,
	[Note] [nvarchar](1000) NULL,
	[Feedback] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](200) NULL,
	[FilePath] [nvarchar](200) NULL,
	[UserList] [nvarchar](1000) NULL,
	[DepartmentList] [nvarchar](1000) NULL,
	[RoleList] [nvarchar](1000) NULL,
	[Url] [nvarchar](200) NULL,
	[IsFolder] [bit] NULL,
	[IsStar] [bit] NULL,
	[ParentID] [int] NULL,
	[Tags] [nvarchar](200) NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Note_List] PRIMARY KEY CLUSTERED 
(
	[pk_Note] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Memo_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Memo_List](
	[pk_Memo] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](200) NULL,
	[FilePath] [nvarchar](200) NULL,
	[UserList] [nvarchar](1000) NULL,
	[DepartmentList] [nvarchar](1000) NULL,
	[RoleList] [nvarchar](1000) NULL,
	[Url] [nvarchar](200) NULL,
	[IsStar] [bit] NULL,
	[Tags] [nvarchar](200) NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Memo_List] PRIMARY KEY CLUSTERED 
(
	[pk_Memo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mail_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mail_List](
	[pk_Mail] [int] IDENTITY(1,1) NOT NULL,
	[fk_Account] [int] NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[ParentID] [int] NULL,
	[MailType] [int] NULL,
	[MailFrom] [nvarchar](100) NULL,
	[MailTo] [nvarchar](2000) NULL,
	[MailCc] [nvarchar](2000) NULL,
	[MailBcc] [nvarchar](2000) NULL,
	[Subject] [nvarchar](200) NULL,
	[Body] [nvarchar](max) NULL,
	[IsBodyHtml] [bit] NULL,
	[IsInternal] [bit] NULL,
	[IsSent] [bit] NULL,
	[SentDate] [datetime] NULL,
	[IsStar] [bit] NULL,
	[IsAttachment] [bit] NULL,
	[AttachmentList] [nvarchar](500) NULL,
	[IsForward] [bit] NULL,
	[IsRead] [bit] NULL,
	[ReadDate] [datetime] NULL,
	[Priority] [int] NULL,
	[IsReply] [bit] NULL,
	[ReplyDate] [datetime] NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Mail_List] PRIMARY KEY CLUSTERED 
(
	[pk_Mail] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mail_Attachment]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mail_Attachment](
	[pk_Attachment] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [nvarchar](100) NULL,
	[FilePath] [nvarchar](200) NULL,
	[FileType] [nvarchar](10) NULL,
	[FileSize] [int] NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_Mail_Attachment] PRIMARY KEY CLUSTERED 
(
	[pk_Attachment] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mail_Account]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mail_Account](
	[pk_Account] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[AccountName] [nvarchar](100) NULL,
	[DisplayName] [nvarchar](50) NULL,
	[PopHost] [varchar](100) NULL,
	[Port] [int] NULL,
	[UseSSL] [bit] NULL,
	[SmtpHost] [varchar](100) NULL,
	[SmtpPort] [int] NULL,
	[LoginName] [varchar](100) NULL,
	[Password] [varchar](100) NULL,
	[SmtpPwd] [varchar](100) NULL,
	[SmtpUser] [varchar](100) NULL,
	[SmtpUseSSL] [bit] NULL,
	[Signature] [nvarchar](1000) NULL,
	[Note] [nvarchar](500) NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Mail_Account] PRIMARY KEY CLUSTERED 
(
	[pk_Account] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Mail_Account] ON
INSERT [dbo].[Mail_Account] ([pk_Account], [fk_User], [fk_Department], [TypeID], [StatusID], [AccountName], [DisplayName], [PopHost], [Port], [UseSSL], [SmtpHost], [SmtpPort], [LoginName], [Password], [SmtpPwd], [SmtpUser], [SmtpUseSSL], [Signature], [Note], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (1, 1, 1, 0, 0, N'客户询问信', N'魔方动力', N'pop3.126.com', 110, 0, N'smtp.163.com', 25, N'mojocube@163.com', N'XiRAJSMhKiYxMjM0NTY=', N'XiRAJSMhKiYxMjM0NTY=', N'mojocube@163.com', 0, N'', N'', 0, 1, CAST(0x0000A53C0004905C AS DateTime), 1, CAST(0x0000A7CA00E10A8C AS DateTime))
SET IDENTITY_INSERT [dbo].[Mail_Account] OFF
/****** Object:  Table [dbo].[Forum_Vote]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Forum_Vote](
	[pk_Vote] [int] IDENTITY(1,1) NOT NULL,
	[fk_Forum] [int] NULL,
	[Title] [nvarchar](200) NULL,
	[SortID] [int] NULL,
	[Visual] [varchar](10) NULL,
	[UserList] [nvarchar](max) NULL,
	[Votes] [int] NULL,
 CONSTRAINT [PK_Forum_Vote] PRIMARY KEY CLUSTERED 
(
	[pk_Vote] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Forum_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Forum_List](
	[pk_Forum] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL,
	[Note] [nvarchar](1000) NULL,
	[ImagePath] [nvarchar](200) NULL,
	[FilePath] [nvarchar](200) NULL,
	[UserList] [nvarchar](1000) NULL,
	[DepartmentList] [nvarchar](1000) NULL,
	[RoleList] [nvarchar](1000) NULL,
	[Url] [nvarchar](200) NULL,
	[IsStar] [bit] NULL,
	[IsTop] [bit] NULL,
	[IsVote] [bit] NULL,
	[ParentID] [int] NULL,
	[SortID] [int] NULL,
	[IsReply] [bit] NULL,
	[LastReplyUser] [int] NULL,
	[LastReply] [datetime] NULL,
	[Replies] [int] NULL,
	[Clicks] [int] NULL,
	[Likes] [int] NULL,
	[Tags] [nvarchar](200) NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Forum_List] PRIMARY KEY CLUSTERED 
(
	[pk_Forum] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Finance_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Finance_List](
	[pk_Finance] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[fk_Account] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](1000) NULL,
	[Note] [nvarchar](1000) NULL,
	[HandleName] [nvarchar](50) NULL,
	[HandleDate] [datetime] NULL,
	[Amount] [decimal](18, 4) NULL,
	[ImagePath] [nvarchar](200) NULL,
	[FilePath] [nvarchar](200) NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Finance_List] PRIMARY KEY CLUSTERED 
(
	[pk_Finance] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Finance_List] ON
INSERT [dbo].[Finance_List] ([pk_Finance], [fk_User], [fk_Department], [fk_Account], [TypeID], [StatusID], [Title], [Description], [Note], [HandleName], [HandleDate], [Amount], [ImagePath], [FilePath], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (1, 1, 3, 1, 0, 1, N'强盛电子货款', N'材料、运费', N'已付清', N'张三', CAST(0x0000A8CD00D8B490 AS DateTime), CAST(500000.0000 AS Decimal(18, 4)), N'', N'', 1, CAST(0x0000A8CD00D95E7C AS DateTime), 1, CAST(0x0000A8CD01100904 AS DateTime))
INSERT [dbo].[Finance_List] ([pk_Finance], [fk_User], [fk_Department], [fk_Account], [TypeID], [StatusID], [Title], [Description], [Note], [HandleName], [HandleDate], [Amount], [ImagePath], [FilePath], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (2, 1, 3, 1, 1, 1, N'购买公务车', N'', N'', N'李四', CAST(0x0000A8CD00DA1420 AS DateTime), CAST(-200000.0000 AS Decimal(18, 4)), N'', N'', 1, CAST(0x0000A8CD00DA7EC4 AS DateTime), 0, CAST(0x0000A8CD00DA7EC4 AS DateTime))
SET IDENTITY_INSERT [dbo].[Finance_List] OFF
/****** Object:  Table [dbo].[Finance_Account]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Finance_Account](
	[pk_Account] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[AccountName] [nvarchar](100) NULL,
	[Owner] [nvarchar](50) NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](500) NULL,
	[Note] [nvarchar](500) NULL,
	[ImagePath] [nvarchar](200) NULL,
	[FilePath] [nvarchar](200) NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Finance_Account] PRIMARY KEY CLUSTERED 
(
	[pk_Account] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Finance_Account] ON
INSERT [dbo].[Finance_Account] ([pk_Account], [fk_User], [fk_Department], [TypeID], [StatusID], [AccountName], [Owner], [Title], [Description], [Note], [ImagePath], [FilePath], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (1, 1, 1, 0, 0, N'62269988890163', N'张三', N'建行对公账号', N'开户地址：建行珠江新城支行', N'', N'', N'', 1, CAST(0x0000A8CD00BDDA58 AS DateTime), 1, CAST(0x0000A8CD00BEE330 AS DateTime))
INSERT [dbo].[Finance_Account] ([pk_Account], [fk_User], [fk_Department], [TypeID], [StatusID], [AccountName], [Owner], [Title], [Description], [Note], [ImagePath], [FilePath], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (2, 1, 1, 1, 0, N'13800138000', N'李四', N'公司财务微信账号', N'密码：123456', N'', N'', N'', 1, CAST(0x0000A8CD00C1749C AS DateTime), 1, CAST(0x0000A8CD00C1D70C AS DateTime))
INSERT [dbo].[Finance_Account] ([pk_Account], [fk_User], [fk_Department], [TypeID], [StatusID], [AccountName], [Owner], [Title], [Description], [Note], [ImagePath], [FilePath], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (3, 1, 1, 2, 0, N'mojocube', N'王五', N'财务支付宝账号', N'密码：123456', N'', N'', N'', 1, CAST(0x0000A8CD00C1A250 AS DateTime), 1, CAST(0x0000A8CD00C1DA90 AS DateTime))
SET IDENTITY_INSERT [dbo].[Finance_Account] OFF
/****** Object:  Table [dbo].[Document_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Document_List](
	[pk_Document] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL,
	[Note] [nvarchar](1000) NULL,
	[Feedback] [nvarchar](max) NULL,
	[DocumentType] [int] NULL,
	[FileName] [nvarchar](100) NULL,
	[FilePath] [nvarchar](200) NULL,
	[FileType] [nvarchar](10) NULL,
	[FileSize] [int] NULL,
	[UserList] [nvarchar](1000) NULL,
	[DepartmentList] [nvarchar](1000) NULL,
	[RoleList] [nvarchar](1000) NULL,
	[Url] [nvarchar](200) NULL,
	[IsFolder] [bit] NULL,
	[ParentID] [int] NULL,
	[Tags] [nvarchar](200) NULL,
	[Downloads] [int] NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Document_List] PRIMARY KEY CLUSTERED 
(
	[pk_Document] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_List](
	[pk_Customer] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[FullName] [nvarchar](100) NULL,
	[ShortName] [nvarchar](10) NULL,
	[CustomerType] [nvarchar](50) NULL,
	[CustomerLevel] [nvarchar](10) NULL,
	[CustomerProperty] [nvarchar](50) NULL,
	[CustomerState] [nvarchar](50) NULL,
	[CustomerSource] [nvarchar](50) NULL,
	[Industry] [nvarchar](50) NULL,
	[Nature] [nvarchar](50) NULL,
	[EmployeNum] [nvarchar](50) NULL,
	[License] [nvarchar](100) NULL,
	[Tags] [nvarchar](200) NULL,
	[Contact] [nvarchar](50) NULL,
	[Website] [nvarchar](100) NULL,
	[Email] [nvarchar](100) NULL,
	[TEL] [nvarchar](50) NULL,
	[Mobile] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[Wechat] [nvarchar](50) NULL,
	[QQ] [nvarchar](50) NULL,
	[Wangwang] [nvarchar](50) NULL,
	[Weibo] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[Province] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[County] [nvarchar](50) NULL,
	[Address] [nvarchar](500) NULL,
	[Remark] [nvarchar](500) NULL,
	[Description] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](200) NULL,
	[FilePath] [nvarchar](200) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[IsStar] [bit] NULL,
	[IsAlert] [bit] NULL,
	[AlertDate] [datetime] NULL,
	[AlertInfo] [nvarchar](500) NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Customer_List] PRIMARY KEY CLUSTERED 
(
	[pk_Customer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Customer_List] ON
INSERT [dbo].[Customer_List] ([pk_Customer], [fk_User], [fk_Department], [TypeID], [StatusID], [FullName], [ShortName], [CustomerType], [CustomerLevel], [CustomerProperty], [CustomerState], [CustomerSource], [Industry], [Nature], [EmployeNum], [License], [Tags], [Contact], [Website], [Email], [TEL], [Mobile], [Fax], [Wechat], [QQ], [Wangwang], [Weibo], [ZipCode], [Country], [Province], [City], [County], [Address], [Remark], [Description], [ImagePath], [FilePath], [StartDate], [EndDate], [IsStar], [IsAlert], [AlertDate], [AlertInfo], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (1, 1, 1, 0, 0, N'魔方动力软件有限公司', N'MFDL', N'合作伙伴', N'5星', N'合作客户', N'已成交', N'客户介绍', N'科技软件', N'民营企业', N'50人', N'', N'软件', N'马云', N'http://www.mojocube.com/', N'4100@qq.com', N'89729668', N'18600186000', N'', N'', N'4100', N'', N'', N'516000', N'中国', N'广东省', N'惠州市', N'惠城区', N'惠州风尚国际', N'', N'', N'', N'', CAST(0x0000AA4F0154C968 AS DateTime), CAST(0x0000ABBD00000000 AS DateTime), 0, 0, CAST(0x0000ABBD00000000 AS DateTime), N'', 1, CAST(0x0000AA4F0154C968 AS DateTime), 1, CAST(0x0000AA4F01559A00 AS DateTime))
SET IDENTITY_INSERT [dbo].[Customer_List] OFF
/****** Object:  Table [dbo].[Customer_Follow]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Follow](
	[pk_Follow] [int] IDENTITY(1,1) NOT NULL,
	[fk_Customer] [int] NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL,
	[FollowDate] [datetime] NULL,
	[NextFollow] [datetime] NULL,
	[Remark] [nvarchar](500) NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Customer_Follow] PRIMARY KEY CLUSTERED 
(
	[pk_Follow] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Customer_Follow] ON
INSERT [dbo].[Customer_Follow] ([pk_Follow], [fk_Customer], [fk_User], [fk_Department], [TypeID], [StatusID], [Title], [Description], [FollowDate], [NextFollow], [Remark], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (1, 1, 1, 1, 0, 0, N'跟客户洽谈OA建设问题', N'', CAST(0x0000AA4F00000000 AS DateTime), CAST(0x0000AA5200000000 AS DateTime), N'', 1, CAST(0x0000AA4F0155D114 AS DateTime), 0, CAST(0x0000AA4F0155D114 AS DateTime))
SET IDENTITY_INSERT [dbo].[Customer_Follow] OFF
/****** Object:  Table [dbo].[Car_Set]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Car_Set](
	[pk_Set] [int] IDENTITY(1,1) NOT NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](1000) NULL,
	[Note] [nvarchar](1000) NULL,
	[ImagePath] [nvarchar](200) NULL,
	[FilePath] [nvarchar](200) NULL,
	[BuyDate] [datetime] NULL,
	[IsUse] [bit] NULL,
	[Mileage] [varchar](20) NULL,
	[LastMaintenance] [datetime] NULL,
 CONSTRAINT [PK_Car_Set] PRIMARY KEY CLUSTERED 
(
	[pk_Set] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Car_Set] ON
INSERT [dbo].[Car_Set] ([pk_Set], [TypeID], [StatusID], [Title], [Description], [Note], [ImagePath], [FilePath], [BuyDate], [IsUse], [Mileage], [LastMaintenance]) VALUES (1, 0, 0, N'奔驰S级S 320【粤A 88888】', N'接待专用车', N'加98号汽油', N'Car/2018042212535888941470.jpg', N'', CAST(0x0000A79800000000 AS DateTime), 1, N'5000公里', CAST(0x0000A8B500000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[Car_Set] OFF
/****** Object:  Table [dbo].[Car_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Car_List](
	[pk_Car] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[fk_Set] [int] NULL,
	[NumberID] [varchar](20) NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](1000) NULL,
	[Note] [nvarchar](1000) NULL,
	[ImagePath] [nvarchar](200) NULL,
	[FilePath] [nvarchar](200) NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[IsCheck] [bit] NULL,
	[CheckUser] [int] NULL,
	[CheckDate] [datetime] NULL,
	[CheckDepartment] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Driver] [nvarchar](10) NULL,
	[Passengers] [nvarchar](100) NULL,
	[Destination] [nvarchar](100) NULL,
	[Mileage] [varchar](20) NULL,
	[IsBack] [bit] NULL,
	[BackDate] [datetime] NULL,
 CONSTRAINT [PK_Car_List] PRIMARY KEY CLUSTERED 
(
	[pk_Car] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Car_List] ON
INSERT [dbo].[Car_List] ([pk_Car], [fk_User], [fk_Department], [fk_Set], [NumberID], [TypeID], [StatusID], [Title], [Description], [Note], [ImagePath], [FilePath], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate], [IsCheck], [CheckUser], [CheckDate], [CheckDepartment], [StartDate], [EndDate], [Driver], [Passengers], [Destination], [Mileage], [IsBack], [BackDate]) VALUES (1, 1, 1, 1, N'20180422132944543815', 2, 1, N'接待陈副市长', N'接到陈副市长到我司进行考察', N'同意', N'', N'', 1, CAST(0x0000A8CA00DE6660 AS DateTime), 2, CAST(0x0000A8CA00F3AAE8 AS DateTime), 1, 2, CAST(0x0000A8CA00F3AAE8 AS DateTime), 2, CAST(0x0000A8CA00000000 AS DateTime), CAST(0x0000A8CA018B3BB0 AS DateTime), N'张三', N'', N'深圳', N'5000公里', 1, CAST(0x0000A8CA00F4E4F8 AS DateTime))
SET IDENTITY_INSERT [dbo].[Car_List] OFF
/****** Object:  Table [dbo].[Calendar_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Calendar_List](
	[pk_Calendar] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL,
	[Note] [nvarchar](1000) NULL,
	[ImagePath] [nvarchar](200) NULL,
	[FilePath] [nvarchar](200) NULL,
	[UserList] [nvarchar](1000) NULL,
	[DepartmentList] [nvarchar](1000) NULL,
	[RoleList] [nvarchar](1000) NULL,
	[Url] [nvarchar](200) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[IsRemind] [bit] NULL,
	[AdvancedTime] [int] NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Calendar_List] PRIMARY KEY CLUSTERED 
(
	[pk_Calendar] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Calendar_List] ON
INSERT [dbo].[Calendar_List] ([pk_Calendar], [fk_User], [fk_Department], [TypeID], [StatusID], [Title], [Description], [Note], [ImagePath], [FilePath], [UserList], [DepartmentList], [RoleList], [Url], [StartDate], [EndDate], [IsRemind], [AdvancedTime], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (1, 1, 1, 0, 0, N'平安夜倒数晚会', N'各部门负责人请安排一个节目，到时有抽奖环节！', N'', N'', N'', N'3|2', N'', N'', N'', CAST(0x0000A578016BEC10 AS DateTime), CAST(0x0000A578017C66D0 AS DateTime), 0, 0, 0, 1, CAST(0x0000A562016C65F0 AS DateTime), 0, CAST(0x0000A562016C65F0 AS DateTime))
SET IDENTITY_INSERT [dbo].[Calendar_List] OFF
/****** Object:  Table [dbo].[Attendance_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Attendance_List](
	[pk_Attendance] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[Description] [nvarchar](500) NULL,
	[Note] [nvarchar](500) NULL,
	[IPAddress] [nvarchar](50) NULL,
	[Browser] [nvarchar](50) NULL,
	[SessionID] [nvarchar](50) NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Attendance_List] PRIMARY KEY CLUSTERED 
(
	[pk_Attendance] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Attendance_List] ON
INSERT [dbo].[Attendance_List] ([pk_Attendance], [fk_User], [fk_Department], [TypeID], [StatusID], [Description], [Note], [IPAddress], [Browser], [SessionID], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (1, 1, 1, 0, 1, N'路上塞车，故晚到', N'', N'127.0.0.1', N'Chrome 31.0', N'ike11qcike2rhl4dxc4ksln0', 0, 1, CAST(0x0000A52900B107EC AS DateTime), 1, CAST(0x0000A52900B49D80 AS DateTime))
INSERT [dbo].[Attendance_List] ([pk_Attendance], [fk_User], [fk_Department], [TypeID], [StatusID], [Description], [Note], [IPAddress], [Browser], [SessionID], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (2, 1, 1, 1, 0, N'', N'', N'127.0.0.1', N'Chrome 31.0', N'zewryztzpwqvcg0upmyn100f', 0, 1, CAST(0x0000A529017DF1BC AS DateTime), 0, CAST(0x0000A529017DF1BC AS DateTime))
SET IDENTITY_INSERT [dbo].[Attendance_List] OFF
/****** Object:  Table [dbo].[Address_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address_List](
	[pk_Address] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[NickName] [nvarchar](50) NULL,
	[FullName] [nvarchar](50) NULL,
	[FirstName] [nvarchar](50) NULL,
	[MiddleName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Phone1] [nvarchar](50) NULL,
	[Phone2] [nvarchar](50) NULL,
	[Email1] [nvarchar](100) NULL,
	[Email2] [nvarchar](100) NULL,
	[Fax] [nvarchar](50) NULL,
	[Line] [nvarchar](50) NULL,
	[Wechat] [nvarchar](50) NULL,
	[QQ] [nvarchar](50) NULL,
	[Facebook] [nvarchar](50) NULL,
	[Twitter] [nvarchar](50) NULL,
	[Linkedin] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](50) NULL,
	[Place] [nvarchar](50) NULL,
	[Address1] [nvarchar](500) NULL,
	[Address2] [nvarchar](500) NULL,
	[Sex] [int] NULL,
	[Birthday] [datetime] NULL,
	[Note] [nvarchar](1000) NULL,
	[Feedback] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](200) NULL,
	[FilePath] [nvarchar](200) NULL,
	[UserList] [nvarchar](1000) NULL,
	[DepartmentList] [nvarchar](1000) NULL,
	[RoleList] [nvarchar](1000) NULL,
	[Url] [nvarchar](200) NULL,
	[IsFolder] [bit] NULL,
	[IsStar] [bit] NULL,
	[ParentID] [int] NULL,
	[Tags] [nvarchar](200) NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Address_List] PRIMARY KEY CLUSTERED 
(
	[pk_Address] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Address_List] ON
INSERT [dbo].[Address_List] ([pk_Address], [fk_User], [fk_Department], [TypeID], [StatusID], [NickName], [FullName], [FirstName], [MiddleName], [LastName], [Phone1], [Phone2], [Email1], [Email2], [Fax], [Line], [Wechat], [QQ], [Facebook], [Twitter], [Linkedin], [ZipCode], [Place], [Address1], [Address2], [Sex], [Birthday], [Note], [Feedback], [ImagePath], [FilePath], [UserList], [DepartmentList], [RoleList], [Url], [IsFolder], [IsStar], [ParentID], [Tags], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (1, 1, 1, 0, 0, N'', N'雷军', N'', N'', N'', N'13800138000', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', 0, CAST(0x0000878A00000000 AS DateTime), N'魔方动力软件有限公司', N'', N'', N'', N'', N'', N'', N'', 0, 0, 0, N'', 1, 1, CAST(0x0000AA4F0155F0B8 AS DateTime), 0, CAST(0x0000AA4F0155F0B8 AS DateTime))
SET IDENTITY_INSERT [dbo].[Address_List] OFF
/****** Object:  Table [dbo].[Workflow_Template]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workflow_Template](
	[pk_Template] [int] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](100) NULL,
	[UserList] [nvarchar](1000) NULL,
	[DepartmentList] [nvarchar](1000) NULL,
	[RoleList] [nvarchar](1000) NULL,
	[Description] [nvarchar](max) NULL,
	[ParentID] [int] NULL,
	[SortID] [int] NULL,
	[Visible] [bit] NULL,
	[fk_Company] [int] NULL,
	[IsStepFree] [bit] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Workflow_Template] PRIMARY KEY CLUSTERED 
(
	[pk_Template] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Workflow_Template] ON
INSERT [dbo].[Workflow_Template] ([pk_Template], [TemplateName], [UserList], [DepartmentList], [RoleList], [Description], [ParentID], [SortID], [Visible], [fk_Company], [IsStepFree], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (1, N'公文', N'', N'', N'', N'', 0, 0, 1, 0, 0, 1, CAST(0x0000AA0A0189B448 AS DateTime), 0, CAST(0x0000AA0A0189B448 AS DateTime))
INSERT [dbo].[Workflow_Template] ([pk_Template], [TemplateName], [UserList], [DepartmentList], [RoleList], [Description], [ParentID], [SortID], [Visible], [fk_Company], [IsStepFree], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (2, N'财务', N'', N'', N'', N'', 0, 0, 1, 0, 0, 1, CAST(0x0000AA0A0189C5DC AS DateTime), 0, CAST(0x0000AA0A0189C5DC AS DateTime))
INSERT [dbo].[Workflow_Template] ([pk_Template], [TemplateName], [UserList], [DepartmentList], [RoleList], [Description], [ParentID], [SortID], [Visible], [fk_Company], [IsStepFree], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (3, N'人事', N'', N'', N'', N'', 0, 0, 1, 0, 0, 1, CAST(0x0000AA0A0189D194 AS DateTime), 0, CAST(0x0000AA0A0189D194 AS DateTime))
INSERT [dbo].[Workflow_Template] ([pk_Template], [TemplateName], [UserList], [DepartmentList], [RoleList], [Description], [ParentID], [SortID], [Visible], [fk_Company], [IsStepFree], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (4, N'收文', N'', N'', N'', N'<div style="text-align:center;">
	<h2>
		公文收文示例
	</h2>
	<table align="center" style="width:600px;text-align:center;">
		<tbody>
			<tr>
				<td style="height:50px;text-align:left;">
					魔方动力（2019）8 号
				</td>
				<td style="text-align:right;width:50%;">
					签发人：张三
				</td>
			</tr>
		</tbody>
	</table>
	<div style="width:600px;border-top:solid 3px #ff0000;height:3px;margin:0 auto;">
	</div>
	<div style="width:600px;margin:0 auto;font-size:16pt;padding:20px 10px 10px;">
		关于魔方动力OA办公管理系统
	</div>
	<div style="width:600px;padding:10px;line-height:1.8em;text-align:left;margin:0 auto;">
		魔方动力（MojoCube）基于网络，专注于网页设计和网站程序开发。已经开发一系列网站管理程序，如McCMS内容管理系统、McCart购物车系统。系统界面清新， 兼容性强，可扩展性十分强大，可以开发出目前所见的绝大部分网站，适合企业建站自用或者开发者二次开发。目前已支持Access和Mssql数据库，最新版的内容管理系统和购物车系统均支持响应式和HTML5。 我们是一支年轻富有活力的团队，拥有多年网络开发经验。我们了解当前最新最流行的开发模式，对主流的网页设计、系统设计有较深入的了解。 我们是创业团队，热衷帮助中小企业实现网络信息化。相对于同行大型软件公司，我们经营成本相对较低，所以我们拥有较高的性价比优势，更适合中小企业。 魔方动力产品包括内容管理系统（McCMS）、购物车系统（McCart）、办公管理系统等等，我们并不是软件的代理商或者二次开发商，我们具备完整的开发能力。
            魔方动力是什么？
            魔方动力（MojoCube）从事网站系统开发逾10年，积累了很多宝贵的经验，自主研发了一套简单而强大的系统。产品包括内容管理系统（CMS）、购物车系统（eCommerce）、办公管理系统（OA）。
	</div>
	<table align="center" style="width:600px;text-align:center;">
		<tbody>
			<tr>
				<td style="height:60px;text-align:right;">
					盖章：
				</td>
				<td style="text-align:center;width:50%;">
				</td>
			</tr>
		</tbody>
	</table>
</div>', 1, 0, 1, 0, 0, 1, CAST(0x0000AA0A0189D194 AS DateTime), 0, CAST(0x0000AA0A0189D194 AS DateTime))
INSERT [dbo].[Workflow_Template] ([pk_Template], [TemplateName], [UserList], [DepartmentList], [RoleList], [Description], [ParentID], [SortID], [Visible], [fk_Company], [IsStepFree], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (5, N'发文', N'', N'', N'', N'<div style="text-align:center;">
	<h2>
		公文发文示例
	</h2>
	<table align="center" style="width:600px;text-align:center;">
		<tbody>
			<tr>
				<td style="height:50px;text-align:left;">
					魔方动力（2019）8 号
				</td>
				<td style="text-align:right;width:50%;">
					签发人：张三
				</td>
			</tr>
		</tbody>
	</table>
	<div style="width:600px;border-top:solid 3px #ff0000;height:3px;margin:0 auto;">
	</div>
	<div style="width:600px;margin:0 auto;font-size:16pt;padding:20px 10px 10px;">
		关于魔方动力OA办公管理系统
	</div>
	<div style="width:600px;padding:10px;line-height:1.8em;text-align:left;margin:0 auto;">
		魔方动力（MojoCube）基于网络，专注于网页设计和网站程序开发。已经开发一系列网站管理程序，如McCMS内容管理系统、McCart购物车系统。系统界面清新， 兼容性强，可扩展性十分强大，可以开发出目前所见的绝大部分网站，适合企业建站自用或者开发者二次开发。目前已支持Access和Mssql数据库，最新版的内容管理系统和购物车系统均支持响应式和HTML5。 我们是一支年轻富有活力的团队，拥有多年网络开发经验。我们了解当前最新最流行的开发模式，对主流的网页设计、系统设计有较深入的了解。 我们是创业团队，热衷帮助中小企业实现网络信息化。相对于同行大型软件公司，我们经营成本相对较低，所以我们拥有较高的性价比优势，更适合中小企业。 魔方动力产品包括内容管理系统（McCMS）、购物车系统（McCart）、办公管理系统等等，我们并不是软件的代理商或者二次开发商，我们具备完整的开发能力。
            魔方动力是什么？
            魔方动力（MojoCube）从事网站系统开发逾10年，积累了很多宝贵的经验，自主研发了一套简单而强大的系统。产品包括内容管理系统（CMS）、购物车系统（eCommerce）、办公管理系统（OA）。
	</div>
	<table align="center" style="width:600px;text-align:center;">
		<tbody>
			<tr>
				<td style="height:60px;text-align:right;">
					盖章：
				</td>
				<td style="text-align:center;width:50%;">
				</td>
			</tr>
		</tbody>
	</table>
</div>', 1, 0, 1, 0, 0, 1, CAST(0x0000AA0A0189D194 AS DateTime), 0, CAST(0x0000AA0A0189D194 AS DateTime))
INSERT [dbo].[Workflow_Template] ([pk_Template], [TemplateName], [UserList], [DepartmentList], [RoleList], [Description], [ParentID], [SortID], [Visible], [fk_Company], [IsStepFree], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (6, N'请款', N'', N'', N'', N'<div style="text-align:center;">
	<h2>
		请款申请单示例
	</h2>
	<table align="center" cellpadding="0" cellspacing="0" style="width:600px;text-align:center;border:solid 1px #000;">
		<tbody>
			<tr>
				<td style="height:35px;text-align:center;width:120px;border:solid 1px #000;">
					申请人
				</td>
				<td style="text-align:center;border:solid 1px #000;width:180px;">
				</td>
				<td style="height:35px;text-align:center;width:120px;border:solid 1px #000;">
					申请日期
				</td>
				<td style="text-align:center;border:solid 1px #000;width:180px;">
				</td>
			</tr>
			<tr>
				<td style="height:35px;text-align:center;width:120px;border:solid 1px #000;">
					部门
				</td>
				<td style="text-align:center;border:solid 1px #000;width:180px;">
				</td>
				<td style="height:35px;text-align:center;width:120px;border:solid 1px #000;">
					金额
				</td>
				<td style="text-align:center;border:solid 1px #000;width:180px;">
				</td>
			</tr>
			<tr>
				<td style="height:80px;text-align:center;width:120px;border:solid 1px #000;">
					申请理由
				</td>
				<td style="text-align:center;border:solid 1px #000;" colspan="3">
				</td>
			</tr>
			<tr>
				<td style="height:80px;text-align:center;width:120px;border:solid 1px #000;">
					部门审批
				</td>
				<td style="text-align:center;border:solid 1px #000;" colspan="3">
				</td>
			</tr>
			<tr>
				<td style="height:80px;text-align:center;width:120px;border:solid 1px #000;">
					财务审批
				</td>
				<td style="text-align:center;border:solid 1px #000;" colspan="3">
				</td>
			</tr>
			<tr>
				<td style="height:80px;text-align:center;width:120px;border:solid 1px #000;">
					总监审批
				</td>
				<td style="text-align:center;border:solid 1px #000;" colspan="3">
				</td>
			</tr>
		</tbody>
	</table>
</div>', 2, 0, 1, 0, 0, 1, CAST(0x0000AA0A0189D194 AS DateTime), 0, CAST(0x0000AA0A0189D194 AS DateTime))
INSERT [dbo].[Workflow_Template] ([pk_Template], [TemplateName], [UserList], [DepartmentList], [RoleList], [Description], [ParentID], [SortID], [Visible], [fk_Company], [IsStepFree], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (7, N'请假', N'', N'', N'', N'<div style="text-align:center;">
	<h2>
		请假申请单示例
	</h2>
	<table align="center" cellpadding="0" cellspacing="0" style="width:600px;text-align:center;border:solid 1px #000;">
		<tbody>
			<tr>
				<td style="height:35px;text-align:center;width:120px;border:solid 1px #000;">
					申请人
				</td>
				<td style="text-align:center;border:solid 1px #000;width:180px;">
				</td>
				<td style="height:35px;text-align:center;width:120px;border:solid 1px #000;">
					申请日期
				</td>
				<td style="text-align:center;border:solid 1px #000;width:180px;">
				</td>
			</tr>
			<tr>
				<td style="height:35px;text-align:center;width:120px;border:solid 1px #000;">
					部门
				</td>
				<td style="text-align:center;border:solid 1px #000;width:180px;">
				</td>
				<td style="height:35px;text-align:center;width:120px;border:solid 1px #000;">
					请假时间
				</td>
				<td style="text-align:center;border:solid 1px #000;width:180px;">
				</td>
			</tr>
			<tr>
				<td style="height:80px;text-align:center;width:120px;border:solid 1px #000;">
					申请理由
				</td>
				<td style="text-align:center;border:solid 1px #000;" colspan="3">
				</td>
			</tr>
			<tr>
				<td style="height:80px;text-align:center;width:120px;border:solid 1px #000;">
					部门审批
				</td>
				<td style="text-align:center;border:solid 1px #000;" colspan="3">
				</td>
			</tr>
			<tr>
				<td style="height:80px;text-align:center;width:120px;border:solid 1px #000;">
					人事审批
				</td>
				<td style="text-align:center;border:solid 1px #000;" colspan="3">
				</td>
			</tr>
			<tr>
				<td style="height:80px;text-align:center;width:120px;border:solid 1px #000;">
					总监审批
				</td>
				<td style="text-align:center;border:solid 1px #000;" colspan="3">
				</td>
			</tr>
		</tbody>
	</table>
</div>', 3, 0, 1, 0, 0, 1, CAST(0x0000AA0A0189D194 AS DateTime), 0, CAST(0x0000AA0A0189D194 AS DateTime))
SET IDENTITY_INSERT [dbo].[Workflow_Template] OFF
/****** Object:  Table [dbo].[Workflow_Step]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workflow_Step](
	[pk_Step] [int] IDENTITY(1,1) NOT NULL,
	[fk_Template] [int] NULL,
	[UserList] [nvarchar](1000) NULL,
	[DepartmentList] [nvarchar](1000) NULL,
	[RoleList] [nvarchar](1000) NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](500) NULL,
	[SortID] [int] NULL,
	[IsCounterSign] [bit] NULL,
	[fk_Company] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Workflow_Step] PRIMARY KEY CLUSTERED 
(
	[pk_Step] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Workflow_Step] ON
INSERT [dbo].[Workflow_Step] ([pk_Step], [fk_Template], [UserList], [DepartmentList], [RoleList], [Title], [Description], [SortID], [IsCounterSign], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (1, 4, N'3|2|1', N'', N'', N'部门审批', N'', 1, 0, 0, 1, CAST(0x0000AA0B0002E248 AS DateTime), 0, CAST(0x0000AA0B0002E248 AS DateTime))
INSERT [dbo].[Workflow_Step] ([pk_Step], [fk_Template], [UserList], [DepartmentList], [RoleList], [Title], [Description], [SortID], [IsCounterSign], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (2, 4, N'1', N'', N'', N'人事审批', N'', 2, 0, 0, 1, CAST(0x0000AA0B00031704 AS DateTime), 0, CAST(0x0000AA0B00031704 AS DateTime))
INSERT [dbo].[Workflow_Step] ([pk_Step], [fk_Template], [UserList], [DepartmentList], [RoleList], [Title], [Description], [SortID], [IsCounterSign], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (3, 4, N'1', N'', N'', N'总监审批', N'', 3, 0, 0, 1, CAST(0x0000AA0B00032E74 AS DateTime), 0, CAST(0x0000AA0B00032E74 AS DateTime))
INSERT [dbo].[Workflow_Step] ([pk_Step], [fk_Template], [UserList], [DepartmentList], [RoleList], [Title], [Description], [SortID], [IsCounterSign], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (4, 5, N'3|2|1', N'', N'', N'部门审批', N'', 1, 0, 0, 1, CAST(0x0000AA0B0002E248 AS DateTime), 0, CAST(0x0000AA0B0002E248 AS DateTime))
INSERT [dbo].[Workflow_Step] ([pk_Step], [fk_Template], [UserList], [DepartmentList], [RoleList], [Title], [Description], [SortID], [IsCounterSign], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (5, 5, N'1', N'', N'', N'人事审批', N'', 2, 0, 0, 1, CAST(0x0000AA0B00031704 AS DateTime), 0, CAST(0x0000AA0B00031704 AS DateTime))
INSERT [dbo].[Workflow_Step] ([pk_Step], [fk_Template], [UserList], [DepartmentList], [RoleList], [Title], [Description], [SortID], [IsCounterSign], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (6, 5, N'1', N'', N'', N'总监审批', N'', 3, 0, 0, 1, CAST(0x0000AA0B00032E74 AS DateTime), 0, CAST(0x0000AA0B00032E74 AS DateTime))
INSERT [dbo].[Workflow_Step] ([pk_Step], [fk_Template], [UserList], [DepartmentList], [RoleList], [Title], [Description], [SortID], [IsCounterSign], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (7, 6, N'3|2|1', N'', N'', N'部门审批', N'', 1, 0, 0, 1, CAST(0x0000AA0B0002E248 AS DateTime), 0, CAST(0x0000AA0B0002E248 AS DateTime))
INSERT [dbo].[Workflow_Step] ([pk_Step], [fk_Template], [UserList], [DepartmentList], [RoleList], [Title], [Description], [SortID], [IsCounterSign], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (8, 6, N'1', N'', N'', N'财务审批', N'', 2, 0, 0, 1, CAST(0x0000AA0B00031704 AS DateTime), 1, CAST(0x0000AA0B00041A00 AS DateTime))
INSERT [dbo].[Workflow_Step] ([pk_Step], [fk_Template], [UserList], [DepartmentList], [RoleList], [Title], [Description], [SortID], [IsCounterSign], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (9, 6, N'1', N'', N'', N'总监审批', N'', 3, 0, 0, 1, CAST(0x0000AA0B00032E74 AS DateTime), 0, CAST(0x0000AA0B00032E74 AS DateTime))
INSERT [dbo].[Workflow_Step] ([pk_Step], [fk_Template], [UserList], [DepartmentList], [RoleList], [Title], [Description], [SortID], [IsCounterSign], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (10, 7, N'3|2|1', N'', N'', N'部门审批', N'', 1, 0, 0, 1, CAST(0x0000AA0B0002E248 AS DateTime), 0, CAST(0x0000AA0B0002E248 AS DateTime))
INSERT [dbo].[Workflow_Step] ([pk_Step], [fk_Template], [UserList], [DepartmentList], [RoleList], [Title], [Description], [SortID], [IsCounterSign], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (11, 7, N'1', N'', N'', N'人事审批', N'', 2, 0, 0, 1, CAST(0x0000AA0B00031704 AS DateTime), 0, CAST(0x0000AA0B00031704 AS DateTime))
INSERT [dbo].[Workflow_Step] ([pk_Step], [fk_Template], [UserList], [DepartmentList], [RoleList], [Title], [Description], [SortID], [IsCounterSign], [fk_Company], [CreateUser], [CreateDate], [ModifyUser], [ModifyDate]) VALUES (12, 7, N'1', N'', N'', N'总监审批', N'', 3, 0, 0, 1, CAST(0x0000AA0B00032E74 AS DateTime), 0, CAST(0x0000AA0B00032E74 AS DateTime))
SET IDENTITY_INSERT [dbo].[Workflow_Step] OFF
/****** Object:  Table [dbo].[Workflow_Signature]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workflow_Signature](
	[pk_Signature] [int] IDENTITY(1,1) NOT NULL,
	[UserList] [nvarchar](1000) NULL,
	[DepartmentList] [nvarchar](1000) NULL,
	[RoleList] [nvarchar](1000) NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](500) NULL,
	[Password] [nvarchar](50) NULL,
	[FileName] [nvarchar](100) NULL,
	[FilePath] [nvarchar](200) NULL,
	[FileType] [nvarchar](10) NULL,
	[FileSize] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_Workflow_Signature] PRIMARY KEY CLUSTERED 
(
	[pk_Signature] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Workflow_Receiver]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workflow_Receiver](
	[pk_Receiver] [int] IDENTITY(1,1) NOT NULL,
	[fk_Workflow] [int] NULL,
	[fk_Template] [int] NULL,
	[fk_Step] [int] NULL,
	[fk_Signature] [int] NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[IsCheck] [bit] NULL,
	[CheckDate] [datetime] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[Note] [nvarchar](1000) NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_Workflow_Receiver] PRIMARY KEY CLUSTERED 
(
	[pk_Receiver] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Workflow_Log]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workflow_Log](
	[pk_Log] [int] IDENTITY(1,1) NOT NULL,
	[fk_Workflow] [int] NULL,
	[fk_Step] [int] NULL,
	[OldDescription] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_Workflow_Log] PRIMARY KEY CLUSTERED 
(
	[pk_Log] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Workflow_List]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workflow_List](
	[pk_Workflow] [int] IDENTITY(1,1) NOT NULL,
	[fk_User] [int] NULL,
	[fk_Department] [int] NULL,
	[fk_Template] [int] NULL,
	[TypeID] [int] NULL,
	[StatusID] [int] NULL,
	[CurrentStepID] [int] NULL,
	[NextStepID] [int] NULL,
	[Number] [nvarchar](50) NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL,
	[fk_Company] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[NotifyUser] [nvarchar](1000) NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Workflow_List] PRIMARY KEY CLUSTERED 
(
	[pk_Workflow] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Workflow_Attachment]    Script Date: 05/16/2019 21:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workflow_Attachment](
	[pk_Attachment] [int] IDENTITY(1,1) NOT NULL,
	[fk_Workflow] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](500) NULL,
	[FileName] [nvarchar](100) NULL,
	[FilePath] [nvarchar](200) NULL,
	[FileType] [nvarchar](10) NULL,
	[FileSize] [int] NULL,
	[CreateUser] [int] NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_Workflow_Attachment] PRIMARY KEY CLUSTERED 
(
	[pk_Attachment] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[View_User_List]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_User_List]
AS
SELECT     dbo.User_List.pk_User, dbo.User_List.UserName, dbo.User_List.Password, dbo.User_List.TypeID, dbo.User_List.fk_Department, 
                      dbo.User_List.RoleValue, dbo.User_List.RoleList, dbo.User_List.Position, dbo.User_List.Number, dbo.User_List.Skin, dbo.User_List.Language, 
                      dbo.User_List.IsLock, dbo.User_List.LastLoginIP, dbo.User_List.LastLoginTime, dbo.User_List.NickName, dbo.User_List.FullName, 
                      dbo.User_List.FirstName, dbo.User_List.MiddleName, dbo.User_List.LastName, dbo.User_List.Phone1, dbo.User_List.Phone2, dbo.User_List.Email1, 
                      dbo.User_List.Email2, dbo.User_List.Fax, dbo.User_List.Line, dbo.User_List.Wechat, dbo.User_List.QQ, dbo.User_List.Facebook, 
                      dbo.User_List.Twitter, dbo.User_List.Linkedin, dbo.User_List.ZipCode, dbo.User_List.Place, dbo.User_List.Address1, dbo.User_List.Address2, 
                      dbo.User_List.Province, dbo.User_List.City, dbo.User_List.County, dbo.User_List.Zone, dbo.User_List.Sex, dbo.User_List.Height, 
                      dbo.User_List.Weight, dbo.User_List.Birthday, dbo.User_List.Education, dbo.User_List.School, dbo.User_List.ImagePath1, dbo.User_List.ImagePath2, 
                      dbo.User_List.IDCardPath, dbo.User_List.ResumePath, dbo.User_List.Wages, dbo.User_List.BankAccount, dbo.User_List.IDNumber, 
                      dbo.User_List.Source, dbo.User_List.Note, dbo.User_List.Remark, dbo.User_List.EntryTime, dbo.User_List.IsQuit, dbo.User_List.QuitTime, 
                      dbo.User_List.fk_Company, dbo.User_List.CreateUser, dbo.User_List.CreateDate, dbo.User_List.ModifyUser, dbo.User_List.ModifyDate, 
                      dbo.User_Department.DepartmentName, dbo.Role_Name.RoleName_CHS
FROM         dbo.User_List LEFT OUTER JOIN
                      dbo.Role_Name ON dbo.User_List.RoleValue = dbo.Role_Name.pk_Name LEFT OUTER JOIN
                      dbo.User_Department ON dbo.User_List.fk_Department = dbo.User_Department.pk_Department
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "User_List"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 177
               Right = 191
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Role_Name"
            Begin Extent = 
               Top = 110
               Left = 215
               Bottom = 225
               Right = 370
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "User_Department"
            Begin Extent = 
               Top = 6
               Left = 229
               Bottom = 156
               Right = 394
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_User_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_User_List'
GO
/****** Object:  View [dbo].[View_Workflow_Receiver]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Workflow_Receiver]
AS
SELECT     dbo.Workflow_Receiver.pk_Receiver, dbo.Workflow_Receiver.fk_Workflow, dbo.Workflow_Receiver.fk_Template, dbo.Workflow_Receiver.fk_Step, dbo.Workflow_Receiver.fk_Signature, 
                      dbo.Workflow_Receiver.fk_User, dbo.Workflow_Receiver.fk_Department, dbo.Workflow_Receiver.IsCheck, dbo.Workflow_Receiver.CheckDate, dbo.Workflow_Receiver.TypeID, 
                      dbo.Workflow_Receiver.StatusID, dbo.Workflow_Receiver.Note, dbo.Workflow_Receiver.CreateUser, dbo.Workflow_Receiver.CreateDate, dbo.Workflow_List.Number, dbo.Workflow_List.Title, 
                      dbo.Workflow_Step.Title AS StepName, dbo.User_List.FullName, dbo.Workflow_List.StatusID AS WorkflowStatus, dbo.Workflow_List.EndDate
FROM         dbo.User_List RIGHT OUTER JOIN
                      dbo.Workflow_List ON dbo.User_List.pk_User = dbo.Workflow_List.fk_User RIGHT OUTER JOIN
                      dbo.Workflow_Receiver LEFT OUTER JOIN
                      dbo.Workflow_Step ON dbo.Workflow_Receiver.fk_Step = dbo.Workflow_Step.pk_Step ON dbo.Workflow_List.pk_Workflow = dbo.Workflow_Receiver.fk_Workflow
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "User_List"
            Begin Extent = 
               Top = 42
               Left = 558
               Bottom = 241
               Right = 719
            End
            DisplayFlags = 280
            TopColumn = 9
         End
         Begin Table = "Workflow_List"
            Begin Extent = 
               Top = 6
               Left = 237
               Bottom = 210
               Right = 398
            End
            DisplayFlags = 280
            TopColumn = 10
         End
         Begin Table = "Workflow_Receiver"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 251
               Right = 199
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Workflow_Step"
            Begin Extent = 
               Top = 110
               Left = 220
               Bottom = 288
               Right = 382
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 18
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Workflow_Receiver'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Workflow_Receiver'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Workflow_Receiver'
GO
/****** Object:  View [dbo].[View_Workflow_List]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Workflow_List]
AS
SELECT     dbo.Workflow_List.pk_Workflow, dbo.Workflow_List.fk_User, dbo.Workflow_List.fk_Department, dbo.Workflow_List.fk_Template, dbo.Workflow_List.TypeID, dbo.Workflow_List.StatusID, 
                      dbo.Workflow_List.CurrentStepID, dbo.Workflow_List.NextStepID, dbo.Workflow_List.Number, dbo.Workflow_List.Title, dbo.Workflow_List.Description, dbo.Workflow_List.fk_Company, 
                      dbo.Workflow_List.StartDate, dbo.Workflow_List.EndDate, dbo.Workflow_List.NotifyUser, dbo.Workflow_List.CreateUser, dbo.Workflow_List.CreateDate, dbo.Workflow_List.ModifyUser, 
                      dbo.Workflow_List.ModifyDate, dbo.Workflow_Step.Title AS StepName, '|' + dbo.Workflow_List.NotifyUser + '|' AS UserList, dbo.User_List.FullName
FROM         dbo.Workflow_List LEFT OUTER JOIN
                      dbo.User_List ON dbo.Workflow_List.fk_User = dbo.User_List.pk_User LEFT OUTER JOIN
                      dbo.Workflow_Step ON dbo.Workflow_List.NextStepID = dbo.Workflow_Step.pk_Step
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Workflow_List"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 263
               Right = 199
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Workflow_Step"
            Begin Extent = 
               Top = 6
               Left = 237
               Bottom = 200
               Right = 399
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "User_List"
            Begin Extent = 
               Top = 38
               Left = 805
               Bottom = 292
               Right = 966
            End
            DisplayFlags = 280
            TopColumn = 4
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Workflow_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Workflow_List'
GO
/****** Object:  View [dbo].[View_MyHistory]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_MyHistory]
AS
SELECT     Url, fk_User, TypeID, Title, fk_Company, MAX(LogTime) AS CreateDate
FROM         dbo.User_Log
GROUP BY Url, fk_User, TypeID, Title, fk_Company
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "User_Log"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 190
               Right = 179
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_MyHistory'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_MyHistory'
GO
/****** Object:  View [dbo].[View_Menu]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Menu]
AS
SELECT     dbo.Role_List.pk_Role, dbo.Role_List.fk_RoleName, dbo.Role_List.fk_Menu, dbo.Role_List.IsUse, dbo.Role_List.IsAdmin, dbo.Role_List.PowerList, 
                      dbo.Role_List.fk_Company, dbo.Sys_Menu.pk_Menu, dbo.Sys_Menu.ParentID, dbo.Sys_Menu.Name_CHS, dbo.Sys_Menu.Name_CHT, 
                      dbo.Sys_Menu.Name_EN, dbo.Sys_Menu.Url, dbo.Sys_Menu.Icon, dbo.Sys_Menu.SortID, dbo.Sys_Menu.LevelID, dbo.Sys_Menu.TypeID, 
                      dbo.Sys_Menu.Visible, dbo.Sys_Menu.Tag_CHS, dbo.Sys_Menu.Tag_CHT, dbo.Sys_Menu.Tag_EN
FROM         dbo.Role_List LEFT OUTER JOIN
                      dbo.Sys_Menu ON dbo.Role_List.fk_Menu = dbo.Sys_Menu.pk_Menu
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Role_List"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 175
               Right = 182
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Sys_Menu"
            Begin Extent = 
               Top = 6
               Left = 220
               Bottom = 188
               Right = 354
            End
            DisplayFlags = 280
            TopColumn = 6
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 22
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Menu'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Menu'
GO
/****** Object:  View [dbo].[View_Forum_Vote]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Forum_Vote]
AS
SELECT     pk_Vote, fk_Forum, Title, SortID, Visual, UserList, Votes, '|' + UserList + '|' AS UserList2
FROM         dbo.Forum_Vote
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Forum_Vote"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 182
               Right = 172
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Forum_Vote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Forum_Vote'
GO
/****** Object:  View [dbo].[View_Customer_List]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Customer_List]
AS
SELECT     dbo.Customer_List.pk_Customer, dbo.Customer_List.fk_User, dbo.Customer_List.fk_Department, dbo.Customer_List.TypeID, dbo.Customer_List.StatusID, dbo.Customer_List.FullName, 
                      dbo.Customer_List.ShortName, dbo.Customer_List.CustomerType, dbo.Customer_List.CustomerLevel, dbo.Customer_List.CustomerProperty, dbo.Customer_List.CustomerState, 
                      dbo.Customer_List.CustomerSource, dbo.Customer_List.Industry, dbo.Customer_List.Nature, dbo.Customer_List.EmployeNum, dbo.Customer_List.License, dbo.Customer_List.Tags, 
                      dbo.Customer_List.Contact, dbo.Customer_List.Website, dbo.Customer_List.Email, dbo.Customer_List.TEL, dbo.Customer_List.Mobile, dbo.Customer_List.Fax, dbo.Customer_List.Wechat, 
                      dbo.Customer_List.QQ, dbo.Customer_List.Wangwang, dbo.Customer_List.Weibo, dbo.Customer_List.ZipCode, dbo.Customer_List.Country, dbo.Customer_List.Province, dbo.Customer_List.City, 
                      dbo.Customer_List.County, dbo.Customer_List.Address, dbo.Customer_List.Remark, dbo.Customer_List.Description, dbo.Customer_List.ImagePath, dbo.Customer_List.FilePath, 
                      dbo.Customer_List.StartDate, dbo.Customer_List.EndDate, dbo.Customer_List.IsStar, dbo.Customer_List.IsAlert, dbo.Customer_List.AlertDate, dbo.Customer_List.AlertInfo, 
                      dbo.Customer_List.CreateUser, dbo.Customer_List.CreateDate, dbo.Customer_List.ModifyUser, dbo.Customer_List.ModifyDate, dbo.User_List.FullName AS UserFullName
FROM         dbo.Customer_List LEFT OUTER JOIN
                      dbo.User_List ON dbo.Customer_List.fk_User = dbo.User_List.pk_User
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Customer_List"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 215
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "User_List"
            Begin Extent = 
               Top = 6
               Left = 253
               Bottom = 126
               Right = 414
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Customer_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Customer_List'
GO
/****** Object:  View [dbo].[View_Customer_Follow]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Customer_Follow]
AS
SELECT     dbo.Customer_Follow.pk_Follow, dbo.Customer_Follow.fk_Customer, dbo.Customer_Follow.fk_User, dbo.Customer_Follow.fk_Department, dbo.Customer_Follow.TypeID, 
                      dbo.Customer_Follow.StatusID, dbo.Customer_Follow.Title, dbo.Customer_Follow.Description, dbo.Customer_Follow.FollowDate, dbo.Customer_Follow.NextFollow, dbo.Customer_Follow.Remark, 
                      dbo.Customer_Follow.CreateUser, dbo.Customer_Follow.CreateDate, dbo.Customer_Follow.ModifyUser, dbo.Customer_Follow.ModifyDate, dbo.Customer_List.FullName, 
                      dbo.User_List.FullName AS UserFullName, dbo.Customer_List.Contact, dbo.Customer_List.Mobile
FROM         dbo.Customer_Follow LEFT OUTER JOIN
                      dbo.User_List ON dbo.Customer_Follow.fk_User = dbo.User_List.pk_User LEFT OUTER JOIN
                      dbo.Customer_List ON dbo.Customer_Follow.fk_Customer = dbo.Customer_List.pk_Customer
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Customer_Follow"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 199
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "User_List"
            Begin Extent = 
               Top = 6
               Left = 237
               Bottom = 126
               Right = 398
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Customer_List"
            Begin Extent = 
               Top = 6
               Left = 436
               Bottom = 126
               Right = 613
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Customer_Follow'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Customer_Follow'
GO
/****** Object:  View [dbo].[View_Calendar_Check]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Calendar_Check]
AS
SELECT     pk_Calendar, fk_User, fk_Department, TypeID, StatusID, Title, Description, Note, ImagePath, FilePath, UserList, DepartmentList, RoleList, Url, 
                      StartDate, EndDate, IsRemind, AdvancedTime, fk_Company, CreateUser, CreateDate, ModifyUser, ModifyDate, DATEDIFF(HOUR, GETDATE(), StartDate) 
                      AS TimeDiff
FROM         dbo.Calendar_List
WHERE     (IsRemind = 1)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Calendar_List"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 192
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Calendar_Check'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Calendar_Check'
GO
/****** Object:  View [dbo].[View_Finance_List]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Finance_List]
AS
SELECT     dbo.Finance_List.*, dbo.User_Department.DepartmentName, dbo.Finance_Account.Title AS AccountTitle
FROM         dbo.Finance_List LEFT OUTER JOIN
                      dbo.Finance_Account ON dbo.Finance_List.fk_Account = dbo.Finance_Account.pk_Account LEFT OUTER JOIN
                      dbo.User_Department ON dbo.Finance_List.fk_Department = dbo.User_Department.pk_Department
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Finance_List"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 293
               Right = 199
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "User_Department"
            Begin Extent = 
               Top = 6
               Left = 237
               Bottom = 175
               Right = 410
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Finance_Account"
            Begin Extent = 
               Top = 108
               Left = 223
               Bottom = 307
               Right = 384
            End
            DisplayFlags = 280
            TopColumn = 3
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Finance_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Finance_List'
GO
/****** Object:  View [dbo].[View_Notice_List]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Notice_List]
AS
SELECT     dbo.Notice_List.pk_Notice, dbo.Notice_List.fk_User, dbo.Notice_List.fk_Department, dbo.Notice_List.TypeID, dbo.Notice_List.StatusID, 
                      dbo.Notice_List.Title, dbo.Notice_List.Description, dbo.Notice_List.Note, dbo.Notice_List.ImagePath, dbo.Notice_List.FilePath, 
                      dbo.Notice_List.UserList, dbo.Notice_List.DepartmentList, dbo.Notice_List.RoleList, dbo.Notice_List.Url, dbo.Notice_List.StartDate, 
                      dbo.Notice_List.EndDate, dbo.Notice_List.IsTop, dbo.Notice_List.fk_Company, dbo.Notice_List.CreateUser, dbo.Notice_List.CreateDate, 
                      dbo.Notice_List.ModifyUser, dbo.Notice_List.ModifyDate, dbo.View_User_List.FullName, dbo.View_User_List.DepartmentName
FROM         dbo.Notice_List LEFT OUTER JOIN
                      dbo.View_User_List ON dbo.Notice_List.fk_User = dbo.View_User_List.pk_User
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Notice_List"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 191
               Right = 192
            End
            DisplayFlags = 280
            TopColumn = 14
         End
         Begin Table = "View_User_List"
            Begin Extent = 
               Top = 6
               Left = 230
               Bottom = 121
               Right = 395
            End
            DisplayFlags = 280
            TopColumn = 60
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 25
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 2700
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Notice_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Notice_List'
GO
/****** Object:  View [dbo].[View_Note_List]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Note_List]
AS
SELECT     dbo.Note_List.pk_Note, dbo.Note_List.fk_User, dbo.Note_List.fk_Department, dbo.Note_List.TypeID, dbo.Note_List.StatusID, dbo.Note_List.Title, 
                      dbo.Note_List.Description, dbo.Note_List.Note, dbo.Note_List.Feedback, dbo.Note_List.ImagePath, dbo.Note_List.FilePath, dbo.Note_List.UserList, 
                      dbo.Note_List.DepartmentList, dbo.Note_List.RoleList, dbo.Note_List.Url, dbo.Note_List.IsFolder, dbo.Note_List.IsStar, dbo.Note_List.ParentID, 
                      dbo.Note_List.Tags, dbo.Note_List.fk_Company, dbo.Note_List.CreateUser, dbo.Note_List.CreateDate, dbo.Note_List.ModifyUser, 
                      dbo.Note_List.ModifyDate, dbo.View_User_List.FullName, dbo.View_User_List.DepartmentName, '|' + dbo.Note_List.UserList + '|' AS UserList2, 
                      Note_List_1.Title AS ParentName
FROM         dbo.Note_List LEFT OUTER JOIN
                      dbo.Note_List AS Note_List_1 ON dbo.Note_List.ParentID = Note_List_1.pk_Note LEFT OUTER JOIN
                      dbo.View_User_List ON dbo.Note_List.fk_User = dbo.View_User_List.pk_User
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Note_List"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 192
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Note_List_1"
            Begin Extent = 
               Top = 6
               Left = 230
               Bottom = 121
               Right = 384
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "View_User_List"
            Begin Extent = 
               Top = 6
               Left = 422
               Bottom = 121
               Right = 587
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Note_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Note_List'
GO
/****** Object:  View [dbo].[View_Document_List]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Document_List]
AS
SELECT     dbo.Document_List.pk_Document, dbo.Document_List.fk_User, dbo.Document_List.fk_Department, dbo.Document_List.TypeID, 
                      dbo.Document_List.StatusID, dbo.Document_List.Title, dbo.Document_List.Description, dbo.Document_List.Note, dbo.Document_List.Feedback, 
                      dbo.Document_List.DocumentType, dbo.Document_List.FileName, dbo.Document_List.FilePath, dbo.Document_List.FileType, 
                      dbo.Document_List.FileSize, dbo.Document_List.UserList, dbo.Document_List.DepartmentList, dbo.Document_List.RoleList, dbo.Document_List.Url, 
                      dbo.Document_List.IsFolder, dbo.Document_List.ParentID, dbo.Document_List.Tags, dbo.Document_List.Downloads, dbo.Document_List.fk_Company, 
                      dbo.Document_List.CreateUser, dbo.Document_List.CreateDate, dbo.Document_List.ModifyUser, dbo.Document_List.ModifyDate, 
                      dbo.View_User_List.FullName, dbo.View_User_List.DepartmentName, '|' + dbo.Document_List.UserList + '|' AS UserList2
FROM         dbo.Document_List LEFT OUTER JOIN
                      dbo.View_User_List ON dbo.Document_List.fk_User = dbo.View_User_List.pk_User
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Document_List"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 168
               Right = 192
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "View_User_List"
            Begin Extent = 
               Top = 6
               Left = 230
               Bottom = 187
               Right = 395
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 31
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Document_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Document_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Document_List'
GO
/****** Object:  View [dbo].[View_Address_List]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Address_List]
AS
SELECT     dbo.Address_List.pk_Address, dbo.Address_List.fk_User, dbo.Address_List.fk_Department, dbo.Address_List.TypeID, dbo.Address_List.StatusID, dbo.Address_List.NickName, 
                      dbo.Address_List.FullName AS Contact, dbo.Address_List.FirstName, dbo.Address_List.MiddleName, dbo.Address_List.LastName, dbo.Address_List.Phone1, dbo.Address_List.Phone2, 
                      dbo.Address_List.Email1, dbo.Address_List.Email2, dbo.Address_List.Fax, dbo.Address_List.Line, dbo.Address_List.Wechat, dbo.Address_List.QQ, dbo.Address_List.Facebook, 
                      dbo.Address_List.Twitter, dbo.Address_List.Linkedin, dbo.Address_List.ZipCode, dbo.Address_List.Place, dbo.Address_List.Address1, dbo.Address_List.Address2, dbo.Address_List.Sex, 
                      dbo.Address_List.Birthday, dbo.Address_List.Note, dbo.Address_List.Feedback, dbo.Address_List.ImagePath, dbo.Address_List.FilePath, dbo.Address_List.UserList, 
                      dbo.Address_List.DepartmentList, dbo.Address_List.RoleList, dbo.Address_List.Url, dbo.Address_List.IsFolder, dbo.Address_List.IsStar, dbo.Address_List.ParentID, dbo.Address_List.Tags, 
                      dbo.Address_List.fk_Company, dbo.Address_List.CreateUser, dbo.Address_List.CreateDate, dbo.Address_List.ModifyUser, dbo.Address_List.ModifyDate, dbo.View_User_List.FullName, 
                      dbo.View_User_List.DepartmentName, '|' + dbo.Address_List.UserList + '|' AS UserList2, Address_List_1.FullName AS ParentName, dbo.Customer_List.FullName AS Customer
FROM         dbo.Address_List LEFT OUTER JOIN
                      dbo.Customer_List ON dbo.Address_List.fk_Company = dbo.Customer_List.pk_Customer LEFT OUTER JOIN
                      dbo.Address_List AS Address_List_1 ON dbo.Address_List.ParentID = Address_List_1.pk_Address LEFT OUTER JOIN
                      dbo.View_User_List ON dbo.Address_List.fk_User = dbo.View_User_List.pk_User
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Address_List"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 192
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Address_List_1"
            Begin Extent = 
               Top = 6
               Left = 433
               Bottom = 121
               Right = 587
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "View_User_List"
            Begin Extent = 
               Top = 6
               Left = 230
               Bottom = 121
               Right = 395
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Customer_List"
            Begin Extent = 
               Top = 6
               Left = 625
               Bottom = 126
               Right = 802
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Address_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Address_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Address_List'
GO
/****** Object:  View [dbo].[View_Car_List]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Car_List]
AS
SELECT     dbo.Car_List.*, dbo.View_User_List.FullName, dbo.View_User_List.DepartmentName, View_User_List_1.FullName AS CheckFullName, dbo.Car_Set.Title AS CarTitle
FROM         dbo.Car_List LEFT OUTER JOIN
                      dbo.Car_Set ON dbo.Car_List.fk_Set = dbo.Car_Set.pk_Set LEFT OUTER JOIN
                      dbo.View_User_List AS View_User_List_1 ON dbo.Car_List.CheckUser = View_User_List_1.pk_User LEFT OUTER JOIN
                      dbo.View_User_List ON dbo.Car_List.fk_User = dbo.View_User_List.pk_User
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Car_List"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 225
               Right = 213
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "View_User_List"
            Begin Extent = 
               Top = 6
               Left = 251
               Bottom = 225
               Right = 424
            End
            DisplayFlags = 280
            TopColumn = 54
         End
         Begin Table = "View_User_List_1"
            Begin Extent = 
               Top = 78
               Left = 230
               Bottom = 264
               Right = 403
            End
            DisplayFlags = 280
            TopColumn = 9
         End
         Begin Table = "Car_Set"
            Begin Extent = 
               Top = 113
               Left = 806
               Bottom = 233
               Right = 976
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 33
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Car_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'   Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Car_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Car_List'
GO
/****** Object:  View [dbo].[View_Calendar_List]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Calendar_List]
AS
SELECT     dbo.Calendar_List.pk_Calendar, dbo.Calendar_List.fk_User, dbo.Calendar_List.fk_Department, dbo.Calendar_List.TypeID, dbo.Calendar_List.StatusID, 
                      dbo.Calendar_List.Title, dbo.Calendar_List.Description, dbo.Calendar_List.Note, dbo.Calendar_List.ImagePath, dbo.Calendar_List.FilePath, 
                      dbo.Calendar_List.UserList, dbo.Calendar_List.DepartmentList, dbo.Calendar_List.RoleList, dbo.Calendar_List.Url, dbo.Calendar_List.StartDate, 
                      dbo.Calendar_List.EndDate, dbo.Calendar_List.IsRemind, dbo.Calendar_List.AdvancedTime, dbo.Calendar_List.fk_Company, 
                      dbo.Calendar_List.CreateUser, dbo.Calendar_List.CreateDate, dbo.Calendar_List.ModifyUser, dbo.Calendar_List.ModifyDate, 
                      dbo.View_User_List.FullName, dbo.View_User_List.DepartmentName, '|' + dbo.Calendar_List.UserList + '|' AS UserList2
FROM         dbo.Calendar_List LEFT OUTER JOIN
                      dbo.View_User_List ON dbo.Calendar_List.fk_User = dbo.View_User_List.pk_User
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Calendar_List"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 194
               Right = 192
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "View_User_List"
            Begin Extent = 
               Top = 6
               Left = 230
               Bottom = 173
               Right = 395
            End
            DisplayFlags = 280
            TopColumn = 57
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Calendar_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Calendar_List'
GO
/****** Object:  View [dbo].[View_Forum_List]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Forum_List]
AS
SELECT     dbo.Forum_List.pk_Forum, dbo.Forum_List.fk_User, dbo.Forum_List.fk_Department, dbo.Forum_List.TypeID, dbo.Forum_List.StatusID, 
                      dbo.Forum_List.Title, dbo.Forum_List.Description, dbo.Forum_List.Note, dbo.Forum_List.ImagePath, dbo.Forum_List.FilePath, 
                      dbo.Forum_List.UserList, dbo.Forum_List.DepartmentList, dbo.Forum_List.RoleList, dbo.Forum_List.Url, dbo.Forum_List.IsStar, dbo.Forum_List.IsTop, 
                      dbo.Forum_List.IsVote, dbo.Forum_List.ParentID, dbo.Forum_List.SortID, dbo.Forum_List.IsReply, dbo.Forum_List.LastReplyUser, 
                      dbo.Forum_List.LastReply, dbo.Forum_List.Replies, dbo.Forum_List.Clicks, dbo.Forum_List.Likes, dbo.Forum_List.Tags, dbo.Forum_List.fk_Company, 
                      dbo.Forum_List.CreateUser, dbo.Forum_List.CreateDate, dbo.Forum_List.ModifyUser, dbo.Forum_List.ModifyDate, dbo.View_User_List.FullName, 
                      dbo.View_User_List.DepartmentName, '|' + dbo.Forum_List.UserList + '|' AS UserList2, dbo.View_User_List.ImagePath1
FROM         dbo.Forum_List LEFT OUTER JOIN
                      dbo.View_User_List ON dbo.Forum_List.fk_User = dbo.View_User_List.pk_User
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Forum_List"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 192
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "View_User_List"
            Begin Extent = 
               Top = 6
               Left = 230
               Bottom = 121
               Right = 395
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Forum_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Forum_List'
GO
/****** Object:  View [dbo].[View_Task_List]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Task_List]
AS
SELECT     dbo.Task_List.*, dbo.View_User_List.FullName, dbo.View_User_List.DepartmentName
FROM         dbo.Task_List LEFT OUTER JOIN
                      dbo.View_User_List ON dbo.Task_List.fk_User = dbo.View_User_List.pk_User
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Task_List"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 194
               Right = 192
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "View_User_List"
            Begin Extent = 
               Top = 6
               Left = 230
               Bottom = 172
               Right = 395
            End
            DisplayFlags = 280
            TopColumn = 57
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 30
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 1620
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Task_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Task_List'
GO
/****** Object:  View [dbo].[View_Process_List]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Process_List]
AS
SELECT     dbo.Process_List.pk_Process, dbo.Process_List.fk_User, dbo.Process_List.fk_Department, dbo.Process_List.TypeID, dbo.Process_List.StatusID, 
                      dbo.Process_List.Title, dbo.Process_List.Description, dbo.Process_List.Note, dbo.Process_List.ImagePath, dbo.Process_List.FilePath, 
                      dbo.Process_List.fk_Company, dbo.Process_List.CreateUser, dbo.Process_List.CreateDate, dbo.Process_List.ModifyUser, 
                      dbo.Process_List.ModifyDate, dbo.Process_List.IsCheck, dbo.Process_List.CheckUser, dbo.Process_List.CheckDate, 
                      dbo.Process_List.CheckDepartment, dbo.Process_List.AtUser, dbo.Process_List.Tags, dbo.Process_List.ParentID, dbo.View_User_List.FullName, 
                      dbo.View_User_List.DepartmentName, dbo.View_User_List.ImagePath1, View_User_List_1.FullName AS CheckFullName, 
                      View_User_List_1.DepartmentName AS CheckDepartmentName, View_User_List_1.ImagePath1 AS ImagePath2
FROM         dbo.Process_List LEFT OUTER JOIN
                      dbo.View_User_List AS View_User_List_1 ON dbo.Process_List.CheckUser = View_User_List_1.pk_User LEFT OUTER JOIN
                      dbo.View_User_List ON dbo.Process_List.fk_User = dbo.View_User_List.pk_User
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Process_List"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 205
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "View_User_List_1"
            Begin Extent = 
               Top = 6
               Left = 243
               Bottom = 121
               Right = 408
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "View_User_List"
            Begin Extent = 
               Top = 6
               Left = 446
               Bottom = 121
               Right = 611
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 24
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Process_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Process_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Process_List'
GO
/****** Object:  View [dbo].[View_Plan_List]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Plan_List]
AS
SELECT     dbo.Plan_List.pk_Plan, dbo.Plan_List.fk_User, dbo.Plan_List.fk_Department, dbo.Plan_List.TypeID, dbo.Plan_List.StatusID, dbo.Plan_List.Title, 
                      dbo.Plan_List.Description, dbo.Plan_List.Note, dbo.Plan_List.Feedback, dbo.Plan_List.ImagePath, dbo.Plan_List.FilePath, dbo.Plan_List.UserList, 
                      dbo.Plan_List.DepartmentList, dbo.Plan_List.RoleList, dbo.Plan_List.Url, dbo.Plan_List.StartDate, dbo.Plan_List.EndDate, dbo.Plan_List.fk_Company, 
                      dbo.Plan_List.CreateUser, dbo.Plan_List.CreateDate, dbo.Plan_List.ModifyUser, dbo.Plan_List.ModifyDate, dbo.View_User_List.FullName, 
                      dbo.View_User_List.DepartmentName
FROM         dbo.Plan_List LEFT OUTER JOIN
                      dbo.View_User_List ON dbo.Plan_List.fk_User = dbo.View_User_List.pk_User
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Plan_List"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 195
               Right = 192
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "View_User_List"
            Begin Extent = 
               Top = 6
               Left = 230
               Bottom = 172
               Right = 395
            End
            DisplayFlags = 280
            TopColumn = 57
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 25
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Plan_List'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Plan_List'
GO
/****** Object:  View [dbo].[View_User_Online]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_User_Online]
AS
SELECT     dbo.User_Online.pk_Online, dbo.User_Online.fk_User, dbo.User_Online.SessionID, dbo.User_Online.IPAddress, dbo.User_Online.Browser, 
                      dbo.User_Online.TypeID, dbo.User_Online.LoginTime, dbo.User_Online.fk_Company, dbo.View_User_List.UserName, dbo.View_User_List.FullName, 
                      dbo.View_User_List.DepartmentName
FROM         dbo.User_Online LEFT OUTER JOIN
                      dbo.View_User_List ON dbo.User_Online.fk_User = dbo.View_User_List.pk_User
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "User_Online"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 179
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "View_User_List"
            Begin Extent = 
               Top = 6
               Left = 217
               Bottom = 121
               Right = 382
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_User_Online'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_User_Online'
GO
/****** Object:  View [dbo].[View_Task_Receiver]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Task_Receiver]
AS
SELECT     dbo.Task_Receiver.pk_Receiver, dbo.Task_Receiver.fk_Task, dbo.Task_Receiver.fk_User, dbo.Task_Receiver.fk_Department, dbo.Task_Receiver.IsRead, dbo.Task_Receiver.ReadDate, 
                      dbo.Task_Receiver.IsReceive, dbo.Task_Receiver.ReceiveDate, dbo.View_Task_List.TypeID, dbo.View_Task_List.StatusID, dbo.View_Task_List.Title, dbo.View_Task_List.Description, 
                      dbo.View_Task_List.IsTop, dbo.View_Task_List.CreateDate, dbo.View_Task_List.FullName, dbo.View_Task_List.DepartmentName, dbo.View_Task_List.IsComplete, dbo.View_Task_List.IsCancel, 
                      dbo.View_Task_List.CreateUser, dbo.View_Task_List.FilePath, dbo.User_List.FullName AS Receiver
FROM         dbo.Task_Receiver LEFT OUTER JOIN
                      dbo.User_List ON dbo.Task_Receiver.fk_User = dbo.User_List.pk_User LEFT OUTER JOIN
                      dbo.View_Task_List ON dbo.Task_Receiver.fk_Task = dbo.View_Task_List.pk_Task
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Task_Receiver"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 201
               Right = 191
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "View_Task_List"
            Begin Extent = 
               Top = 6
               Left = 229
               Bottom = 174
               Right = 394
            End
            DisplayFlags = 280
            TopColumn = 10
         End
         Begin Table = "User_List"
            Begin Extent = 
               Top = 6
               Left = 432
               Bottom = 126
               Right = 593
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 17
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Task_Receiver'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Task_Receiver'
GO
/****** Object:  View [dbo].[View_Notice_Receiver]    Script Date: 05/16/2019 21:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Notice_Receiver]
AS
SELECT     dbo.Notice_Receiver.pk_Receiver, dbo.Notice_Receiver.fk_Notice, dbo.Notice_Receiver.fk_User, dbo.Notice_Receiver.fk_Department, dbo.Notice_Receiver.IsRead, dbo.Notice_Receiver.ReadDate, 
                      dbo.View_Notice_List.TypeID, dbo.View_Notice_List.StatusID, dbo.View_Notice_List.Title, dbo.View_Notice_List.Description, dbo.View_Notice_List.FullName, dbo.View_Notice_List.DepartmentName, 
                      dbo.View_Notice_List.CreateDate, dbo.View_Notice_List.IsTop, dbo.View_Notice_List.Url, dbo.View_Notice_List.FilePath
FROM         dbo.Notice_Receiver LEFT OUTER JOIN
                      dbo.View_Notice_List ON dbo.Notice_Receiver.fk_Notice = dbo.View_Notice_List.pk_Notice
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Notice_Receiver"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 181
               Right = 191
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "View_Notice_List"
            Begin Extent = 
               Top = 6
               Left = 229
               Bottom = 180
               Right = 394
            End
            DisplayFlags = 280
            TopColumn = 9
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 16
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Notice_Receiver'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Notice_Receiver'
GO
/****** Object:  Default [DF_Address_List_IsFolder]    Script Date: 05/16/2019 21:47:01 ******/
ALTER TABLE [dbo].[Address_List] ADD  CONSTRAINT [DF_Address_List_IsFolder]  DEFAULT ((0)) FOR [IsFolder]
GO
/****** Object:  Default [DF_Calendar_List_IsTop]    Script Date: 05/16/2019 21:47:01 ******/
ALTER TABLE [dbo].[Calendar_List] ADD  CONSTRAINT [DF_Calendar_List_IsTop]  DEFAULT ((0)) FOR [IsRemind]
GO
/****** Object:  Default [DF_Document_List_IsTop]    Script Date: 05/16/2019 21:47:01 ******/
ALTER TABLE [dbo].[Document_List] ADD  CONSTRAINT [DF_Document_List_IsTop]  DEFAULT ((0)) FOR [IsFolder]
GO
/****** Object:  Default [DF_Forum_List_IsFolder]    Script Date: 05/16/2019 21:47:01 ******/
ALTER TABLE [dbo].[Forum_List] ADD  CONSTRAINT [DF_Forum_List_IsFolder]  DEFAULT ((0)) FOR [IsTop]
GO
/****** Object:  Default [DF_Note_List_IsFolder]    Script Date: 05/16/2019 21:47:01 ******/
ALTER TABLE [dbo].[Note_List] ADD  CONSTRAINT [DF_Note_List_IsFolder]  DEFAULT ((0)) FOR [IsFolder]
GO
/****** Object:  Default [DF_Notice_List_IsTop]    Script Date: 05/16/2019 21:47:01 ******/
ALTER TABLE [dbo].[Notice_List] ADD  CONSTRAINT [DF_Notice_List_IsTop]  DEFAULT ((0)) FOR [IsTop]
GO
/****** Object:  Default [DF_Sys_Menu_ParentID]    Script Date: 05/16/2019 21:47:01 ******/
ALTER TABLE [dbo].[Sys_Menu] ADD  CONSTRAINT [DF_Sys_Menu_ParentID]  DEFAULT ((0)) FOR [ParentID]
GO
/****** Object:  Default [DF_Sys_Menu_SortID]    Script Date: 05/16/2019 21:47:01 ******/
ALTER TABLE [dbo].[Sys_Menu] ADD  CONSTRAINT [DF_Sys_Menu_SortID]  DEFAULT ((0)) FOR [SortID]
GO
/****** Object:  Default [DF_Sys_Menu_LevelID]    Script Date: 05/16/2019 21:47:01 ******/
ALTER TABLE [dbo].[Sys_Menu] ADD  CONSTRAINT [DF_Sys_Menu_LevelID]  DEFAULT ((0)) FOR [LevelID]
GO
/****** Object:  Default [DF_Sys_Menu_Visible]    Script Date: 05/16/2019 21:47:01 ******/
ALTER TABLE [dbo].[Sys_Menu] ADD  CONSTRAINT [DF_Sys_Menu_Visible]  DEFAULT ((1)) FOR [Visible]
GO
/****** Object:  Default [DF_Task_List_IsTop]    Script Date: 05/16/2019 21:47:01 ******/
ALTER TABLE [dbo].[Task_List] ADD  CONSTRAINT [DF_Task_List_IsTop]  DEFAULT ((0)) FOR [IsTop]
GO
/****** Object:  Default [DF_User_List_ShowHistory]    Script Date: 05/16/2019 21:47:01 ******/
ALTER TABLE [dbo].[User_List] ADD  CONSTRAINT [DF_User_List_ShowHistory]  DEFAULT ((10)) FOR [ShowHistory]
GO
/****** Object:  Default [DF_User_Online_TypeID]    Script Date: 05/16/2019 21:47:01 ******/
ALTER TABLE [dbo].[User_Online] ADD  CONSTRAINT [DF_User_Online_TypeID]  DEFAULT ((0)) FOR [TypeID]
GO
