﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Document_List, App_Web_ru30snpe" %>

<%@ Register Src="Nav.ascx" TagName="Nav" TagPrefix="MojoCube" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">

        <section class="content-header">
          <h1>
            文件管理
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">文件管理</li>
          </ol>
        </section>

        <section class="content">
          <div class="row">
          
            <MojoCube:Nav id="Nav" runat="server" />
    
            <div class="col-md-9">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"><asp:Label ID="lblTitle" runat="server"></asp:Label></h3>
                  <div class="box-tools">
                    <div class="input-group" style="width: 150px;">
                      <asp:TextBox ID="txtKeyword" runat="server" CssClass="form-control input-sm pull-right" placeholder="查找..."></asp:TextBox>
                      <div class="input-group-btn">
                        <asp:LinkButton ID="lnbSearch" runat="server" CssClass="btn btn-sm btn-default" onclick="lnbSearch_Click"><i class="fa fa-search"></i></asp:LinkButton>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-body no-padding">
                  <div class="mailbox-controls">
                    <asp:LinkButton ID="lnbSelectAll" runat="server" CssClass="btn btn-default btn-sm checkbox-toggle" onclick="lnbSelectAll_Click" ToolTip="全选/反选"><i class="fa fa-square-o"></i></asp:LinkButton>
                    <div class="btn-group">
                        <asp:LinkButton ID="lnbDelete" runat="server" CssClass="btn btn-default btn-sm" onclick="lnbDelete_Click" OnClientClick="{return confirm('确定删除吗？');}" ToolTip="删除文件"><i class="fa fa-trash-o"></i></asp:LinkButton>
                        <a href="#FolderDiv" class="fancybox btn btn-default btn-sm" title="新建文件夹"><i class="fa fa-folder-open"></i></a>
                    </div>
                    <asp:LinkButton ID="lnbRefresh" runat="server" CssClass="btn btn-default btn-sm" onclick="lnbRefresh_Click" ToolTip="刷新"><i class="fa fa-refresh"></i></asp:LinkButton>
                    <asp:HyperLink ID="hlBack" runat="server" CssClass="btn btn-default btn-sm" ToolTip="返回" Visible="false"><i class="fa fa-level-up"></i></asp:HyperLink>
                  </div>
                  <div class="table-responsive mailbox-messages">

                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" GridLines="None" BorderWidth="0px" CssClass="table table-hover table-striped" AllowSorting="True" OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" OnRowCreated="GridView1_RowCreated">
                        <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("pk_Document") %>'></asp:Label>
                                    <asp:CheckBox ID="cbFolder" runat="server" Checked='<%# Bind("IsFolder") %>' />
                                    <asp:Label ID="lblFilePath" runat="server" Text='<%# Bind("FilePath") %>'></asp:Label>
                                    <asp:Label ID="lblCreateUser" runat="server" Text='<%# Bind("CreateUser") %>'></asp:Label>
                                    <asp:Label ID="lblFullName" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="id" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="选择">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbSelect" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Label ID="lblThumbnail" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="mailbox-star" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="类型" SortExpression="TypeID">
                                <ItemTemplate>
                                    <asp:Label ID="lblType" runat="server" Text='<%# Bind("TypeID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="标题">
                                <ItemTemplate>
                                    <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="mailbox-subject" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="格式">
                                <ItemTemplate>
                                    <asp:Label ID="lblFileType" runat="server" Text='<%# Bind("FileType") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="mailbox-attachment" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="大小">
                                <ItemTemplate>
                                    <asp:Label ID="lblFileSize" runat="server" Text='<%# Bind("FileSize") %>' style="color:#999;"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="mailbox-attachment" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="更新">
                                <ItemTemplate>
                                    <asp:Label ID="lblModifyDate" runat="server" Text='<%# Bind("ModifyDate") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="mailbox-date" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="状态" SortExpression="StatusID">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("StatusID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="操作">
                                <ItemTemplate>
                                    <asp:HyperLink ID="gvEdit" runat="server" ToolTip="修改"><span class="label label-primary"><i class="fa fa-edit"></i> 修改</span></asp:HyperLink>
                                    <asp:HyperLink ID="gvDownload" runat="server" ToolTip="下载"><span class="label label-success"><i class="fa fa-download"></i> 下载</span></asp:HyperLink>
                                    <asp:LinkButton ID="gvDelete" runat="server" ToolTip="删除" CommandName="_delete"><span class="label label-danger"><i class="fa fa-remove"></i> 删除</span></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <div id="EmptyDiv" runat="server" visible="false" style="height:60px; text-align:center; padding:10px; overflow:auto; border-top:solid 1px #F4F4F4;">暂无信息</div>
                    
                  </div>
                </div>
                <div class="box-footer no-padding" style="margin-top:-20px;">
                  <div class="mailbox-controls">
                  
                    <div id="pager" style="background:#fff; border:0px; margin-top:0px; padding:2px;">
                       <webdiyer:AspNetPager ID="ListPager" runat="server" OnPageChanged="ListPager_PageChanged"></webdiyer:AspNetPager>
                    </div>
        
                  </div>
                </div>
              </div>
            </div>

          </div>

        </section>

      </div>
      
      <div id="FolderDiv" style="display:none; width:300px;">
            
          <div class="box-body">

              <div class="form-group">
                  <asp:TextBox ID="txtFolderName" runat="server" CssClass="form-control" Text="新建文件夹"></asp:TextBox>
              </div>
                  
              <div class="pull-right">
                  <asp:LinkButton ID="lnbFolder" runat="server" CssClass="btn btn-primary" onclick="lnbFolder_Click">确定</asp:LinkButton>
              </div>

          </div>

      </div>

</asp:Content>


