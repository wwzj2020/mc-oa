﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="User_List, App_Web_3aszqovu" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">

        <section class="content-header">
          <h1>
            用户管理
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">用户管理</li>
          </ol>
        </section>

        <section class="content">

          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">
                      <asp:HyperLink ID="hlAdd" runat="server"><span class="label label-success"><i class="fa fa-plus"></i> 新增</span></asp:HyperLink>
                      <asp:LinkButton ID="lnbBirthday" runat="server" onclick="lnbBirthday_Click"><span class="label label-primary"><i class="fa fa-birthday-cake"></i> 生日</span></asp:LinkButton>
                      <asp:HyperLink ID="hlSearch" runat="server" NavigateUrl="#SearchDiv" CssClass="fancybox" ToolTip="高级搜索"><span class="label label-back"><i class="fa fa-search"></i> 高级搜索</span></asp:HyperLink>
                      <asp:LinkButton ID="lnbExcel" runat="server" OnClick="lnbExcel_Click"><span class="label label-success"><i class="fa fa-file-excel-o"></i> 导出</span></asp:LinkButton>
                  </h3>
                  <div class="box-tools">
                    <div class="input-group" style="width: 150px;">
                      <asp:TextBox ID="txtKeyword" runat="server" CssClass="form-control input-sm pull-right" placeholder="查找..."></asp:TextBox>
                      <div class="input-group-btn">
                        <asp:LinkButton ID="lnbSearch" runat="server" CssClass="btn btn-sm btn-default" onclick="lnbSearch_Click"><i class="fa fa-search"></i></asp:LinkButton>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-body table-responsive no-padding">

                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" GridLines="None" BorderWidth="0px" CssClass="table table-hover" AllowSorting="True" OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" OnRowCreated="GridView1_RowCreated">
                        <Columns>
                            <asp:TemplateField HeaderText="ID" SortExpression="pk_User">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("pk_User") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="id" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Label ID="lblThumbnail" runat="server" Text='<%# Bind("ImagePath1") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="部门" SortExpression="DepartmentName">
                                <ItemTemplate>
                                    <asp:Label ID="lblDepartmentName" runat="server" Text='<%# Bind("DepartmentName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="真实姓名" SortExpression="FullName">
                                <ItemTemplate>
                                    <asp:Label ID="lblFullName" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="用户名" SortExpression="UserName">
                                <ItemTemplate>
                                    <asp:Label ID="lblUserName" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="角色" SortExpression="RoleName_CHS">
                                <ItemTemplate>
                                    <asp:Label ID="lblRoleName" runat="server" Text='<%# Bind("RoleName_CHS") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="电话" SortExpression="Phone1">
                                <ItemTemplate>
                                    <asp:Label ID="lblPhone1" runat="server" Text='<%# Bind("Phone1") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="工资" SortExpression="Wages">
                                <ItemTemplate>
                                    <asp:Label ID="lblWages" runat="server" Text='<%# Bind("Wages") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="禁用">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbLock" runat="server" Checked='<%# Bind("IsLock") %>' Enabled="false" />
                                </ItemTemplate>
                                <ItemStyle Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="操作">
                                <ItemTemplate>
                                    <asp:HyperLink ID="gvEdit" runat="server" ToolTip="修改"><span class="label label-primary"><i class="fa fa-edit"></i> 修改</span></asp:HyperLink>
                                    <asp:LinkButton ID="gvDelete" runat="server" ToolTip="删除" CommandName="_delete"><span class="label label-danger"><i class="fa fa-remove"></i> 删除</span></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    
                    <div id="pager">
                       <webdiyer:AspNetPager ID="ListPager" runat="server" OnPageChanged="ListPager_PageChanged"></webdiyer:AspNetPager>
                    </div>
        
                </div>
                
              </div>
            </div>
          </div>

        </section>

      </div>
    
      <div id="SearchDiv" style="display:none; width:500px;">
            
          <div class="box-body">
          
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label1" runat="server" Text="部门"></asp:Label></label>
                <asp:DropDownList ID="ddlSearchDepartment" runat="server" CssClass="form-control select2"></asp:DropDownList>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label5" runat="server" Text="角色"></asp:Label></label>
                <asp:DropDownList ID="ddlSearchRole" runat="server" CssClass="form-control select2"></asp:DropDownList>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label2" runat="server" Text="真实姓名"></asp:Label></label>
                <asp:TextBox ID="txtSearchFullName" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label3" runat="server" Text="用户名"></asp:Label></label>
                <asp:TextBox ID="txtSearchUserName" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label7" runat="server" Text="联系电话"></asp:Label></label>
                <asp:TextBox ID="txtSearchPhone" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label8" runat="server" Text="身份证号"></asp:Label></label>
                <asp:TextBox ID="txtSearchIDNumber" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label4" runat="server" Text="生日起"></asp:Label></label>
                <asp:TextBox ID="txtSearchStart" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label6" runat="server" Text="生日止"></asp:Label></label>
                <asp:TextBox ID="txtSearchEnd" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
              </div>
                  
              <div class="pull-right">
                  <asp:LinkButton ID="lnbSearchMore" runat="server" CssClass="btn btn-primary" onclick="lnbSearchMore_Click">搜索</asp:LinkButton>
                  <asp:LinkButton ID="lnbSearchCancel" runat="server" CssClass="btn btn-default" onclick="lnbSearchCancel_Click">重置</asp:LinkButton>
              </div>

          </div>

      </div>

</asp:Content>


