﻿<%@ page language="C#" masterpagefile="~/Commons/Simple.master" autoeventwireup="true" inherits="Calendar_View, App_Web_q4hx13e2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSimple" Runat="Server">

      <div class="content-wrapper-simple">

        <section class="content">

          <div id="AlertDiv" runat="server"></div>

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">
                  <asp:HyperLink ID="hlPrint" runat="server" NavigateUrl="javascript:McPrintHide('.box-title,.box-footer');"><span class="label label-primary"><i class="fa fa-print"></i> 打印</span></asp:HyperLink>
              </h3>
            </div>

            <div class="box-body">
              <div class="row">
              
                <div class="box-body">
                  <div class="mailbox-read-info">
                    <h3><asp:Label ID="lblTitle" runat="server"></asp:Label></h3>
                    <h5><asp:Label ID="lblFrom" runat="server" CssClass="mailbox-read-time"></asp:Label><asp:Label ID="lblDate" runat="server" CssClass="mailbox-read-time pull-right"></asp:Label></h5>
                  </div>
                  <div class="mailbox-read-message">
                      <asp:Label ID="lblDescription" runat="server"></asp:Label>
                  </div>
                </div>

              </div>
            </div>
            
            <div class="box-footer">
                <asp:Button ID="btnEdit" runat="server" Text="修改" CssClass="btn btn-primary" Visible="false"></asp:Button>
            </div>

          </div>

        </section>

      </div>

</asp:Content>


