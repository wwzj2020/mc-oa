﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Process_ViewList, App_Web_bxgxqor5" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">

        <section class="content-header">
          <h1>
            流程汇总
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">流程汇总</li>
          </ol>
        </section>

        <section class="content">

          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">
                      <asp:HyperLink ID="hlPrint" runat="server" NavigateUrl="javascript:McPrint();"><span class="label label-primary"><i class="fa fa-print"></i> 打印</span></asp:HyperLink>
                      <asp:HyperLink ID="hlSearch" runat="server" NavigateUrl="#SearchDiv" CssClass="fancybox" ToolTip="高级搜索"><span class="label label-back"><i class="fa fa-search"></i> 高级搜索</span></asp:HyperLink>
                      <asp:HyperLink ID="hlAddType" runat="server"><span class="label label-success"><i class="fa fa-plus"></i> 新增类型</span></asp:HyperLink>
                  </h3>
                  <div class="box-tools">
                    <div class="input-group" style="width: 150px;">
                      <asp:TextBox ID="txtKeyword" runat="server" CssClass="form-control input-sm pull-right" placeholder="查找..."></asp:TextBox>
                      <div class="input-group-btn">
                        <asp:LinkButton ID="lnbSearch" runat="server" CssClass="btn btn-sm btn-default" onclick="lnbSearch_Click"><i class="fa fa-search"></i></asp:LinkButton>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-body table-responsive no-padding">

                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" GridLines="None" BorderWidth="0px" CssClass="table table-hover" AllowSorting="True" OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" OnRowCreated="GridView1_RowCreated">
                        <Columns>
                            <asp:TemplateField HeaderText="ID" SortExpression="pk_Process" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("pk_Process") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="id" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="类型" SortExpression="TypeID">
                                <ItemTemplate>
                                    <asp:Label ID="lblType" runat="server" Text='<%# Bind("TypeID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="标题">
                                <ItemTemplate>
                                    <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="申请人">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateUser" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="申请时间">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="状态" SortExpression="StatusID">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("StatusID") %>'></asp:Label>
                                    <asp:Label ID="lblNote" runat="server" Text='<%# Bind("Note") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="附件">
                                <ItemTemplate>
                                    <asp:Label ID="lblAttachment" runat="server" Text='<%# Bind("FilePath") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="mailbox-attachment" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="操作">
                                <ItemTemplate>
                                    <asp:HyperLink ID="gvEdit" runat="server" ToolTip="查看"><span class="label label-primary"><i class="fa fa-search"></i> 查看</span></asp:HyperLink>
                                    <asp:LinkButton ID="gvDelete" runat="server" ToolTip="删除" CommandName="_delete"><span class="label label-danger"><i class="fa fa-remove"></i> 删除</span></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    
                    <div id="pager">
                       <webdiyer:AspNetPager ID="ListPager" runat="server" OnPageChanged="ListPager_PageChanged"></webdiyer:AspNetPager>
                    </div>
        
                </div>
                
              </div>
            </div>
          </div>

        </section>

      </div>
    
      <div id="SearchDiv" style="display:none; width:500px;">
            
          <div class="box-body">
          
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label1" runat="server" Text="类型"></asp:Label></label>
                <asp:DropDownList ID="ddlSearchType" runat="server" CssClass="form-control select2"></asp:DropDownList>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label5" runat="server" Text="状态"></asp:Label></label>
                <asp:DropDownList ID="ddlSearchStatus" runat="server" CssClass="form-control select2"></asp:DropDownList>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label2" runat="server" Text="标题"></asp:Label></label>
                <asp:TextBox ID="txtSearchTitle" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label3" runat="server" Text="申请人"></asp:Label></label>
                <asp:TextBox ID="txtSearchCreated" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label4" runat="server" Text="时间起"></asp:Label></label>
                <asp:TextBox ID="txtSearchStart" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label6" runat="server" Text="时间止"></asp:Label></label>
                <asp:TextBox ID="txtSearchEnd" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
              </div>
                  
              <div class="pull-right">
                  <asp:LinkButton ID="lnbSearchMore" runat="server" CssClass="btn btn-primary" onclick="lnbSearchMore_Click">搜索</asp:LinkButton>
                  <asp:LinkButton ID="lnbSearchCancel" runat="server" CssClass="btn btn-default" onclick="lnbSearchCancel_Click">重置</asp:LinkButton>
              </div>

          </div>

      </div>

</asp:Content>