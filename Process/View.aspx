﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Process_View, App_Web_bxgxqor5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            流程汇总
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">流程汇总</li>
          </ol>
        </section>

        <section class="content">

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">
                  <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
                  <asp:Label ID="lblType" runat="server"></asp:Label>
              </h3>
            </div>

            <div class="box-body">

              <div class="row">
                  
                <div class="col-md-12 form-group">
                    
                    <div class="box-body chat" id="chat-box">

                        <div id="InfoDiv" runat="server"></div>

                    </div>

                </div>

              </div>
              
            </div>
            
          </div>

        </section>

      </div>

</asp:Content>