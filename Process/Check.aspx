﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Process_Check, App_Web_bxgxqor5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

    <script type="text/javascript">

        function removeAtt() {
            document.getElementById("ctl00_cphMain_txtFilePath").value = "";
        }

    </script>

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            流程审核
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">流程审核</li>
          </ol>
        </section>

        <section class="content">

          <div id="AlertDiv" runat="server"></div>

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">
                  <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
                  <asp:Label ID="lblType" runat="server"></asp:Label>
              </h3>
            </div>

            <div class="box-body">

              <div class="row">
                  
                <div class="col-md-12 form-group">
                    
                    <div class="box-body chat" id="chat-box">

                        <div id="InfoDiv" runat="server"></div>

                    </div>

                </div>

              </div>
              
              <hr />
              
              <div class="row">
              
                  <div class="col-md-12 form-group">
                    <label><asp:Label ID="Label6" runat="server" Text="审核描述"></asp:Label></label>
                    <asp:TextBox ID="txtNote" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3" placeholder="说说您的审核理由..."></asp:TextBox>
                  </div>
                  
              </div>
              
              <div class="row">
                  
                <hr />
                  
                <div class="col-md-6 form-group" style="position:relative;">
                    <asp:TextBox ID="txtFilePath" runat="server" CssClass="form-control" onfocus="this.blur()"></asp:TextBox>
                    <div style="position:absolute; top:5px; right:20px;">
                        <a href="javascript:void();" onclick="removeAtt();"><span class="label label-danger"><i class="fa fa-remove"></i> 移除</span></a>
                    </div>
                </div>

                <div class="col-md-6 form-group">
                <div class="btn btn-default btn-file">
                    <i class="fa fa-paperclip"></i> 增加附件
                    <asp:FileUpload ID="fuAttachment" runat="server" onchange="ChkUpload(this);"></asp:FileUpload>
                    <span id="filepath"></span>
                </div>
                <p class="help-block">5MB以内</p>
                </div>

              </div>

            </div>
            
            <div class="box-footer">
                <asp:Button ID="btnSave1" runat="server" Text="同意" CssClass="btn btn-success" onclick="btnSave1_Click" OnClientClick="{return confirm('审核后不能修改，确定同意吗？');}"></asp:Button>
                <asp:Button ID="btnSave2" runat="server" Text="驳回" CssClass="btn btn-danger" onclick="btnSave2_Click" OnClientClick="{return confirm('审核后不能修改，确定驳回吗？');}"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default" onclick="btnCancel_Click"></asp:Button>
            </div>

          </div>

        </section>

      </div>

</asp:Content>


