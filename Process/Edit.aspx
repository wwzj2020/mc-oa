﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Process_Edit, App_Web_bxgxqor5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

    <script type="text/javascript">

        function removeAtt() {
            document.getElementById("ctl00_cphMain_txtFilePath").value = "";
        }

    </script>

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            流程管理
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">流程管理</li>
          </ol>
        </section>

        <section class="content">

          <div id="AlertDiv" runat="server"></div>

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">
                  <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
                  <asp:Label ID="lblType" runat="server"></asp:Label>
              </h3>
            </div>

            <div class="box-body">

              <div id="Div1" runat="server" class="row">

                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label1" runat="server" Text="类型"></asp:Label></label>
                    <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control select2" AutoPostBack="True" onselectedindexchanged="ddlType_SelectedIndexChanged"></asp:DropDownList>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label2" runat="server" Text="标题"></asp:Label></label>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label3" runat="server" Text="描述"></asp:Label></label>
                    <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group hidden">
                    <label><asp:Label ID="Label7" runat="server" Text="审核人"></asp:Label></label>
                    <asp:TextBox ID="txtReceiverID" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
              </div>
              
              <div id="Div2" runat="server" class="row">
                  
                <hr />
                  
                <div class="col-md-6 form-group" style="position:relative;">
                    <asp:TextBox ID="txtFilePath" runat="server" CssClass="form-control" onfocus="this.blur()"></asp:TextBox>
                    <div style="position:absolute; top:5px; right:20px;">
                        <a href="javascript:void();" onclick="removeAtt();"><span class="label label-danger"><i class="fa fa-remove"></i> 移除</span></a>
                    </div>
                </div>

                <div class="col-md-6 form-group">
                <div class="btn btn-default btn-file">
                    <i class="fa fa-paperclip"></i> 增加附件
                    <asp:FileUpload ID="fuAttachment" runat="server" onchange="ChkUpload(this);"></asp:FileUpload>
                    <span id="filepath"></span>
                </div>
                <p class="help-block">5MB以内</p>
                </div>

              </div>
              
              <div id="Div3" runat="server" visible="false" class="row">
                  
                <div class="col-md-12 form-group">
                    
                    <div class="box-body chat" id="chat-box">

                        <div id="InfoDiv" runat="server"></div>

                    </div>

                </div>

              </div>
              
            </div>
            
            <div id="ButtonDiv" runat="server" class="box-footer">
                <asp:Button ID="btnSave" runat="server" Text="新增流程" CssClass="btn btn-primary" onclick="btnSave_Click" OnClientClick="{return confirm('新增流程后不能修改，确定新增吗？');}"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default" onclick="btnCancel_Click"></asp:Button>
            </div>

          </div>

        </section>

      </div>

</asp:Content>


