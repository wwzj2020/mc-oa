﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Workflow_CheckList, App_Web_mll1udoh" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">

        <section class="content-header">
          <h1>
            我的审批
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">我的审批</li>
          </ol>
        </section>

        <section class="content">

          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">
                      <asp:Label ID="lblType" runat="server"></asp:Label>
                  </h3>
                  <div class="box-tools">
                    <div class="input-group" style="width: 150px;">
                      <asp:TextBox ID="txtKeyword" runat="server" CssClass="form-control input-sm pull-right" placeholder="查找..."></asp:TextBox>
                      <div class="input-group-btn">
                        <asp:LinkButton ID="lnbSearch" runat="server" CssClass="btn btn-sm btn-default" onclick="lnbSearch_Click"><i class="fa fa-search"></i></asp:LinkButton>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-body table-responsive no-padding">

                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" GridLines="None" BorderWidth="0px" CssClass="table table-hover" AllowSorting="True" OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" OnRowCreated="GridView1_RowCreated" OnRowCommand="GridView1_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="编号" SortExpression="Number">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("pk_Receiver") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblWorkflowID" runat="server" Text='<%# Bind("fk_Workflow") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblCheck" runat="server" Text='<%# Bind("IsCheck") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblWorkflowStatus" runat="server" Text='<%# Bind("WorkflowStatus") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%# Bind("EndDate") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblNumber" runat="server" Text='<%# Bind("Number") %>' style="font-size:9pt; color:#999;"></asp:Label>
                                    <asp:Label ID="lblNextReceiver" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblStepID" runat="server" Text='<%# Bind("fk_Step") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="id" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="程度" SortExpression="TypeID">
                                <ItemTemplate>
                                    <asp:Label ID="lblType" runat="server" Text='<%# Bind("TypeID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="标题">
                                <ItemTemplate>
                                    <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="节点名称">
                                <ItemTemplate>
                                    <asp:Label ID="lblStepName" runat="server" Text='<%# Bind("StepName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="180px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="发起人">
                                <ItemTemplate>
                                    <asp:Label ID="lblFullName" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="发起时间">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="150px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="审批时间">
                                <ItemTemplate>
                                    <asp:Label ID="lblCheckDate" runat="server" Text='<%# Bind("CheckDate") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="150px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="状态" SortExpression="StatusID">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("StatusID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="100px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="操作">
                                <ItemTemplate>
                                    <asp:HyperLink ID="gvView" runat="server" ToolTip="查看"><span class="label label-primary"><i class="fa fa-search"></i> 查看</span></asp:HyperLink>
                                    <asp:HyperLink ID="gvEdit" runat="server" ToolTip="审批"><span class="label label-primary"><i class="fa fa-edit"></i> 审批</span></asp:HyperLink>
                                    <asp:LinkButton ID="gvUndo" runat="server" ToolTip="撤回" CommandName="_undo" Visible="false" OnClientClick="{return confirm('确定撤回吗？');}"><span class="label label-danger"><i class="fa fa-share"></i> 撤回</span></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    
                    <div id="pager">
                       <webdiyer:AspNetPager ID="ListPager" runat="server" OnPageChanged="ListPager_PageChanged"></webdiyer:AspNetPager>
                    </div>
        
                </div>
                
              </div>
            </div>
          </div>

        </section>

      </div>
      
</asp:Content>