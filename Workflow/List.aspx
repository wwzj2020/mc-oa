﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Workflow_List, App_Web_mll1udoh" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">

        <section class="content-header">
          <h1>
            我的工作
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">我的工作</li>
          </ol>
        </section>

        <section class="content">

          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">
                      <asp:HyperLink ID="hlAdd" runat="server" NavigateUrl="#AddDiv" CssClass="fancybox" ToolTip="新建工作"><span class="label label-success"><i class="fa fa-plus"></i> 新增</span></asp:HyperLink>
                  </h3>
                  <div class="box-tools">
                    <div class="input-group" style="width: 150px;">
                      <asp:TextBox ID="txtKeyword" runat="server" CssClass="form-control input-sm pull-right" placeholder="查找..."></asp:TextBox>
                      <div class="input-group-btn">
                        <asp:LinkButton ID="lnbSearch" runat="server" CssClass="btn btn-sm btn-default" onclick="lnbSearch_Click"><i class="fa fa-search"></i></asp:LinkButton>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-body table-responsive no-padding">

                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" GridLines="None" BorderWidth="0px" CssClass="table table-hover" AllowSorting="True" OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" OnRowCreated="GridView1_RowCreated">
                        <Columns>
                            <asp:TemplateField HeaderText="编号" SortExpression="Number">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("pk_Workflow") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblCreateUser" runat="server" Text='<%# Bind("CreateUser") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblCurrentStepID" runat="server" Text='<%# Bind("CurrentStepID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblNextStepID" runat="server" Text='<%# Bind("NextStepID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblNumber" runat="server" Text='<%# Bind("Number") %>' style="font-size:9pt; color:#999;"></asp:Label>
                                    <asp:Label ID="lblNextReceiver" runat="server" Visible="false"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="id" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="程度" SortExpression="TypeID">
                                <ItemTemplate>
                                    <asp:Label ID="lblType" runat="server" Text='<%# Bind("TypeID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="标题">
                                <ItemTemplate>
                                    <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="当前节点">
                                <ItemTemplate>
                                    <asp:Label ID="lblStepName" runat="server" Text='<%# Bind("StepName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="180px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="发起时间">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="150px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="到期时间">
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%# Bind("EndDate") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="150px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="状态" SortExpression="StatusID">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("StatusID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="100px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="操作">
                                <ItemTemplate>
                                    <asp:HyperLink ID="gvView" runat="server" ToolTip="查看"><span class="label label-primary"><i class="fa fa-search"></i> 查看</span></asp:HyperLink>
                                    <asp:HyperLink ID="gvEdit" runat="server" ToolTip="修改" Visible="false"><span class="label label-primary"><i class="fa fa-edit"></i> 修改</span></asp:HyperLink>
                                    <asp:LinkButton ID="gvUndo" runat="server" ToolTip="撤回" CommandName="_undo" Visible="false" OnClientClick="{return confirm('确定撤回吗？');}"><span class="label label-danger"><i class="fa fa-share"></i> 撤回</span></asp:LinkButton>
                                    <asp:LinkButton ID="gvDelete" runat="server" ToolTip="删除" CommandName="_delete" Visible="false"><span class="label label-danger"><i class="fa fa-remove"></i> 删除</span></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    
                    <div id="pager">
                       <webdiyer:AspNetPager ID="ListPager" runat="server" OnPageChanged="ListPager_PageChanged"></webdiyer:AspNetPager>
                    </div>
        
                </div>
                
              </div>
            </div>
          </div>

        </section>

      </div>
      
      <div id="AddDiv" style="display:none; width:500px;">
            
          <div class="box-body">
          
              <div class="form-group">
                <label><asp:Label ID="Label1" runat="server" Text="模板"></asp:Label></label>
                <asp:DropDownList ID="ddlTemplate" runat="server" CssClass="form-control select2"></asp:DropDownList>
              </div>
                  
              <div class="form-group">
                <label><asp:Label ID="Label2" runat="server" Text="标题"></asp:Label></label>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="pull-right">
                  <asp:LinkButton ID="lnbAdd" runat="server" CssClass="btn btn-primary" onclick="lnbAdd_Click">下一步</asp:LinkButton>
                  <asp:LinkButton ID="lnbReset" runat="server" CssClass="btn btn-default" onclick="lnbReset_Click">重置</asp:LinkButton>
              </div>

          </div>

      </div>

</asp:Content>