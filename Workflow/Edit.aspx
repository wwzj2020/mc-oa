﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Workflow_Edit, App_Web_mll1udoh" %>

<%@ Register Src="~/Controls/KindEditor.ascx" TagName="KindEditor" TagPrefix="MojoCube" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

    <script type="text/javascript">

        function ddlChange() {
            var ddl = document.getElementById("ctl00_cphMain_ddlStep");
            document.getElementById("ctl00_cphMain_hlReceiver").href = "Receiver.aspx?stepId=" + ddl.value;
        }

    </script>

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            工作编辑
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">工作编辑</li>
          </ol>
        </section>

        <section class="content">
        
          <div id="AlertDiv" runat="server"></div>

          <div class="row">
            <div class="col-xs-12">
              <div class="nav-tabs-custom">

                <div class="box-header with-border">
                  <h3 class="box-title">
                      <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
                  </h3>
                </div>

                <ul class="nav nav-tabs">
                  <li class="active"><a href="#info" data-toggle="tab">基本信息</a></li>
                  <li><a href="#file" data-toggle="tab">附件管理</a></li>
                </ul>
                <div class="tab-content">
                  
                  <div class="active tab-pane" id="info">
                  
                    <div class="box-body">

                        <div class="form-group">
                            <MojoCube:KindEditor id="txtContent" runat="server" Height="500" />
                        </div>
                        
                        <div class="row">

                            <div class="col-md-6 form-group">
                                <label><asp:Label ID="Label3" runat="server" Text="标题"></asp:Label></label>
                                <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                    
                            <div class="col-md-6 form-group">
                                <label><asp:Label ID="Label5" runat="server" Text="到期"></asp:Label></label>
                                <asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})"></asp:TextBox>
                            </div>
                            
                            <div class="col-md-6 form-group">
                                <label><asp:Label ID="Label4" runat="server" Text="下一步"></asp:Label></label>
                                <asp:DropDownList ID="ddlStep" runat="server" CssClass="form-control select2" onchange="ddlChange();"></asp:DropDownList>
                            </div>
                    
                            <div class="col-md-6 form-group">
                                <label><asp:Label ID="Label6" runat="server" Text="紧急程度"></asp:Label></label>
                                <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control select2"></asp:DropDownList>
                            </div>
                    
                            <div class="col-md-6 form-group" style="position:relative;">
                                <label><asp:Label ID="Label1" runat="server" Text="审批人"></asp:Label></label>
                                <asp:TextBox ID="txtReceiver" runat="server" CssClass="form-control" onfocus="this.blur()"></asp:TextBox>
                                <asp:TextBox ID="txtReceiverID" runat="server" style="display:none;"></asp:TextBox>
                                <div style="position:absolute; top:31px; right:20px;">
                                    <asp:HyperLink ID="hlReceiver" runat="server" CssClass="fancybox fancybox.iframe"><span class="label label-success"><i class="fa fa-plus"></i> 审批人</span></asp:HyperLink>
                                </div>
                            </div>
                    
                            <div class="col-md-6 form-group" style="position:relative;">
                                <label><asp:Label ID="Label2" runat="server" Text="抄送人"></asp:Label></label>
                                <asp:TextBox ID="txtNotify" runat="server" CssClass="form-control" onfocus="this.blur()"></asp:TextBox>
                                <asp:TextBox ID="txtNotifyID" runat="server" style="display:none;"></asp:TextBox>
                                <div style="position:absolute; top:31px; right:20px;">
                                    <asp:HyperLink ID="hlNotify" runat="server" CssClass="fancybox fancybox.iframe"><span class="label label-success"><i class="fa fa-plus"></i> 抄送人</span></asp:HyperLink>
                                </div>
                            </div>
                    
                        </div>
                    
                    </div>
                    
                  </div>

                  <div class="tab-pane" id="file">

                    <div class="box-body">

                      <div class="row">
                          <iframe id="ifUpload" runat="server" style="width:100%;" frameborder="0" scrolling="auto" class="iframe-css"></iframe>
                      </div>

                    </div>
            
                  </div>
                  
                  <div class="box-footer">
                      <asp:Button ID="btnSave" runat="server" Text="提交" CssClass="btn btn-primary" onclick="btnSave_Click"></asp:Button>
                      <asp:Button ID="btnUndo" runat="server" Text="撤回" CssClass="btn btn-warning" onclick="btnUndo_Click" OnClientClick="{return confirm('确定撤回吗？');}" Visible="false"></asp:Button>
                      <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default" onclick="btnCancel_Click"></asp:Button>
                  </div>

                </div>
              </div>
            </div>
          </div>

        </section>

      </div>
      
</asp:Content>