﻿<%@ page language="C#" autoeventwireup="true" inherits="Workflow_Log, App_Web_mll1udoh" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>修改记录</title>
    <link rel="shortcut icon" href="../Images/favicon.ico" type="image/x-icon" />
    
    <style>
        body{background:#eee; padding:10px; font-size:11pt; font-family:'Microsoft YaHei'}
        .main{background:#fff; border:solid 1px #ddd; margin-bottom:20px;}
        .item{padding:10px;}
        .item-title{font-weight:700; padding:10px; float:left}
        .item-info{color:#999; padding:10px; width:20%; float:right; text-align:right; font-style:italic}
        .item-content{ padding:10px;}
        .item-content-tb{width:100%; border-collapse: collapse; border-right: 1px dashed #ddd; border-bottom: 1px dashed #ddd;}
        .item-content-td{width:50%; border-left: 1px dashed #ddd; border-top: 1px dashed #ddd;}
        .item-content-td-div{ padding:10px;}
        .td1{border-left:solid 5px #F39C12}
        .td2{border-left:solid 5px #008D4C}
        .none{ text-align:center; padding:50px;}
    </style>

</head>
<body>
    <form id="form1" runat="server">
    
        <div id="ContentDiv" runat="server"></div>

    </form>
</body>
</html>
