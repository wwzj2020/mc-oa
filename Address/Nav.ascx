﻿<%@ control language="C#" autoeventwireup="true" inherits="Address_Nav, App_Web_0xvofpvl" %>

<div class="col-md-3">
    <asp:HyperLink ID="hlAdd" runat="server" CssClass="btn btn-primary btn-block margin-bottom"><i class="fa fa-pencil"></i> 新建联系人</asp:HyperLink>
    <div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">通讯录</h3>
        <div class="box-tools">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div id="NavDiv" runat="server" class="box-body no-padding">

    </div>
    </div>
    <div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">分类</h3>
    </div>
    <div class="box-body">
        <div class="input-group">
            <asp:TextBox ID="txtFullName" runat="server" CssClass="form-control" placeholder="通讯录分类"></asp:TextBox>
            <div class="input-group-btn">
                <asp:Button ID="btnAdd" runat="server" Text="新增" CssClass="btn btn-primary btn-flat" onclick="btnAdd_Click" />
            </div>
        </div>
    </div>
    </div>
    <div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">类型</h3>
        <div class="box-tools">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div id="TypeDiv" runat="server" class="box-body no-padding">

    </div>
    </div>
</div>
