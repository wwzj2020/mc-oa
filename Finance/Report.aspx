﻿<%@ page language="C#" masterpagefile="../Commons/Main.master" autoeventwireup="true" inherits="Finance_Report, App_Web_hwcdw4ds" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            财务报表
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">财务报表</li>
          </ol>
        </section>

        <section class="content">

        <div class="row">

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-battery-full"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">收入</span>
                  <asp:Label ID="Label1" runat="server" CssClass="info-box-number"></asp:Label>
                </div>
              </div>
            </div>
            
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-battery-empty"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">支出</span>
                  <asp:Label ID="Label2" runat="server" CssClass="info-box-number"></asp:Label>
                </div>
              </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-cny"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">盈利</span>
                  <asp:Label ID="Label3" runat="server" CssClass="info-box-number"></asp:Label>
                </div>
              </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-clock-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">待定</span>
                  <asp:Label ID="Label4" runat="server" CssClass="info-box-number"></asp:Label>
                </div>
              </div>
            </div>

        </div>

        <div class="row">
        
            <div class="col-md-12">

                <!-- 统计报表 -->
                <div class="box">
                    
                    <div class="box-header">
                      <h3 class="box-title">本年度盈利报告</h3>
                    </div>

                    <div class="box-body" style="text-align:center; border-top:solid 1px #eee; overflow:auto;">
                        <div id="chart1"></div>
                    </div>

                </div>

             </div>

        </div>
        
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        详细收入支出报告
                    </h3>
                </div>
                <div class="box-body table-responsive no-padding">
                
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" GridLines="None" BorderWidth="0px" CssClass="table table-hover report-tb">
                    </asp:GridView>

                </div>
                
                </div>
            </div>
        </div>

    </section>

    </div>

</asp:Content>