﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Customer_List, App_Web_fsjepgjt" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">

        <section class="content-header">
          <h1>
            <asp:Label ID="lblTitle" runat="server" Text="客户管理"></asp:Label>
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">客户管理</li>
          </ol>
        </section>

        <section class="content">
            
          <div id="AlertDiv" runat="server"></div>

          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">
                      <asp:HyperLink ID="hlAdd" runat="server"><span class="label label-success"><i class="fa fa-plus"></i> 新增</span></asp:HyperLink>
                      <asp:HyperLink ID="hlChange" runat="server" NavigateUrl="#ChangeDiv" CssClass="fancybox" ToolTip="转交"><span class="label label-primary"><i class="fa fa-exchange"></i> 转交</span></asp:HyperLink>
                      <asp:HyperLink ID="hlSearch" runat="server" NavigateUrl="#SearchDiv" CssClass="fancybox" ToolTip="高级搜索"><span class="label label-back"><i class="fa fa-search"></i> 高级搜索</span></asp:HyperLink>
                      <asp:HyperLink ID="hlExpire" runat="server" ToolTip="到期客户"><span class="label label-back"><i class="fa fa-filter"></i> 到期客户</span></asp:HyperLink>
                      <asp:LinkButton ID="lnbExcel" runat="server" OnClick="lnbExcel_Click"><span class="label label-success"><i class="fa fa-file-excel-o"></i> 导出</span></asp:LinkButton>
                  </h3>
                  <div class="box-tools">
                    <div class="input-group" style="width: 150px;">
                      <asp:TextBox ID="txtKeyword" runat="server" CssClass="form-control input-sm pull-right" placeholder="查找..."></asp:TextBox>
                      <div class="input-group-btn">
                        <asp:LinkButton ID="lnbSearch" runat="server" CssClass="btn btn-sm btn-default" onclick="lnbSearch_Click"><i class="fa fa-search"></i></asp:LinkButton>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-body table-responsive no-padding">

                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" GridLines="None" BorderWidth="0px" CssClass="table table-hover" AllowSorting="True" OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" OnRowCreated="GridView1_RowCreated">
                        <Columns>
                            <asp:TemplateField HeaderText="ID" SortExpression="pk_Customer" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("pk_Customer") %>'></asp:Label>
                                    <asp:Label ID="lblUser" runat="server" Text='<%# Bind("fk_User") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="id" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="选择">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbSelect" runat="server" />
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="类型" SortExpression="TypeID">
                                <ItemTemplate>
                                    <asp:Label ID="lblType" runat="server" Text='<%# Bind("TypeID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="客户名称">
                                <ItemTemplate>
                                    <asp:Label ID="lblFullName" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                    <asp:Label ID="lblWebsite" runat="server" Text='<%# Bind("Website") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="220px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="级别">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerLevel" runat="server" Text='<%# Bind("CustomerLevel") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="客户类型">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerType" runat="server" Text='<%# Bind("CustomerType") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="客户状态">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerState" runat="server" Text='<%# Bind("CustomerState") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="员工数量">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmployeNum" runat="server" Text='<%# Bind("EmployeNum") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="省市">
                                <ItemTemplate>
                                    <asp:Label ID="lblProvince" runat="server" Text='<%# Bind("Province") %>'></asp:Label>
                                    <asp:Label ID="lblCity" runat="server" Text='<%# Bind("City") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="120px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="详细地址">
                                <ItemTemplate>
                                    <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="联系人">
                                <ItemTemplate>
                                    <asp:Label ID="lblContact" runat="server" Text='<%# Bind("Contact") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="负责人">
                                <ItemTemplate>
                                    <asp:Label ID="lblUserFullName" runat="server" Text='<%# Bind("UserFullName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="创建时间" SortExpression="CreateDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="150px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="到期日期" SortExpression="EndDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%# Bind("EndDate") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="100px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="提醒日期" SortExpression="AlertDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblAlertDate" runat="server" Text='<%# Bind("AlertDate") %>' Visible="False"></asp:Label>
                                    <asp:Label ID="lblAlertInfo" runat="server" Text='<%# Bind("AlertInfo") %>' Visible="False"></asp:Label>
                                    <asp:CheckBox ID="cbAlert" runat="server" Checked='<%# Bind("IsAlert") %>' Visible="False" />
                                </ItemTemplate>
                                <ItemStyle Width="100px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="跟进">
                                <ItemTemplate>
                                    <asp:Label ID="lblFollow" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="联系">
                                <ItemTemplate>
                                    <asp:Label ID="lblContactLink" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="状态" SortExpression="StatusID">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("StatusID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="操作">
                                <ItemTemplate>
                                    <asp:HyperLink ID="gvEdit" runat="server" ToolTip="修改"><span class="label label-primary"><i class="fa fa-edit"></i> 修改</span></asp:HyperLink>
                                    <asp:HyperLink ID="gvView" runat="server" ToolTip="查看" Visible="false"><span class="label label-primary"><i class="fa fa-search"></i> 查看</span></asp:HyperLink>
                                    <asp:LinkButton ID="gvDelete" runat="server" ToolTip="删除" CommandName="_delete"><span class="label label-danger"><i class="fa fa-remove"></i> 删除</span></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    
                    <div id="pager">
                       <webdiyer:AspNetPager ID="ListPager" runat="server" OnPageChanged="ListPager_PageChanged"></webdiyer:AspNetPager>
                    </div>
        
                </div>
                
              </div>
            </div>
          </div>

        </section>

      </div>
    
      <div id="ChangeDiv" style="display:none; width:500px;">
            
        <div class="box-body">
            
            <asp:ScriptManager id="ScriptManager1"  runat="server"></asp:ScriptManager>
    
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
        
                    <div class="form-group">
                      <label><asp:Label ID="Label7" runat="server" Text="部门"></asp:Label></label>
                      <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control select2" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                  
                    <div class="form-group">
                      <label><asp:Label ID="Label8" runat="server" Text="人员"></asp:Label></label>
                      <asp:DropDownList ID="ddlUser" runat="server" CssClass="form-control select2"></asp:DropDownList>
                    </div>
                  
                </ContentTemplate>
            </asp:UpdatePanel>
            
            <div class="pull-right">
                <asp:LinkButton ID="lnbChange" runat="server" CssClass="btn btn-primary" onclick="lnbChange_Click" OnClientClick="{return confirm('此操作将转交客户、客户跟进、客户联系人，确定转交吗？');}">转交</asp:LinkButton>
            </div>

        </div>

      </div>
    
      <div id="SearchDiv" style="display:none; width:500px;">
            
          <div class="box-body">
          
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label1" runat="server" Text="类型"></asp:Label></label>
                <asp:DropDownList ID="ddlSearchType" runat="server" CssClass="form-control select2"></asp:DropDownList>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label5" runat="server" Text="状态"></asp:Label></label>
                <asp:DropDownList ID="ddlSearchStatus" runat="server" CssClass="form-control select2"></asp:DropDownList>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label2" runat="server" Text="客户"></asp:Label></label>
                <asp:TextBox ID="txtSearchTitle" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label3" runat="server" Text="负责人"></asp:Label></label>
                <asp:TextBox ID="txtSearchCreated" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label4" runat="server" Text="时间起"></asp:Label></label>
                <asp:TextBox ID="txtSearchStart" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" AutoComplete="off"></asp:TextBox>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label6" runat="server" Text="时间止"></asp:Label></label>
                <asp:TextBox ID="txtSearchEnd" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" AutoComplete="off"></asp:TextBox>
              </div>
                  
              <div class="pull-right">
                  <asp:LinkButton ID="lnbSearchMore" runat="server" CssClass="btn btn-primary" onclick="lnbSearchMore_Click">搜索</asp:LinkButton>
                  <asp:LinkButton ID="lnbSearchCancel" runat="server" CssClass="btn btn-default" onclick="lnbSearchCancel_Click">重置</asp:LinkButton>
              </div>

          </div>

      </div>

</asp:Content>